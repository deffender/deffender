ALTER TABLE `account_donatepoints` MODIFY COLUMN `last_changed` BIGINT NOT NULL DEFAULT 0;
ALTER TABLE `account_votepoints` MODIFY COLUMN `last_changed` BIGINT NOT NULL DEFAULT 0;
ALTER TABLE `account_donatepoints` ADD INDEX `account_donatepoints_lastchanged` (`last_changed`);
ALTER TABLE `account_votepoints` ADD INDEX `account_votepoints_lastchanged` (`last_changed`);
