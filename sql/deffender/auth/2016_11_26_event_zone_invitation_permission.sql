DELETE FROM `fun_permissions` WHERE `id` = 1023;
INSERT INTO `fun_permissions` (`id`, `name`) VALUES (1023, 'can send event zone invitations');
DELETE FROM `fun_linked_permissions` WHERE `id` = 159 and `linkedId` = 1023;
INSERT INTO `fun_linked_permissions` (`id`, `linkedId`) VALUES (159, 1023);

