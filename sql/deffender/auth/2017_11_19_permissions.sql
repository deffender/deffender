DELETE FROM `fun_permissions` WHERE `id` IN (1027, 1028, 1029, 1030, 2418, 2419);
INSERT INTO `fun_permissions` (`id`, `name`) VALUES
(1027, 'Command: em buff/debuff/unbuff/unall'),
(1028, 'Command: em reward'),
(1029, 'Command: em duel'),
(1030, 'Command: em other'),
(2418, 'Command: currency change donate'),
(2419, 'Command: currency change vote');
