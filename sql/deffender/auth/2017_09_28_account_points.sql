CREATE TABLE `account_donate` (
	`account_id` INT UNSIGNED NOT NULL,
	`points` INT NOT NULL,
	`last_changed` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`account_id`)
);

CREATE TABLE `account_votepoints` (
	`account_id` INT UNSIGNED NOT NULL,
	`points` INT NOT NULL,
	`last_changed` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`account_id`)
);
