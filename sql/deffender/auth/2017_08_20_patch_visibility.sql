ALTER TABLE `realmlist` ADD COLUMN `currentPatch` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `gamebuild`;

DELETE FROM `fun_permissions` WHERE `id` = 2413;
INSERT INTO `fun_permissions` VALUES
(2413, 'Command: npc set patch');

DELETE FROM `fun_linked_permissions` WHERE `id` = 163 AND `linkedId` = 2413;
INSERT INTO `fun_linked_permissions` VALUES
(163, 2413);
