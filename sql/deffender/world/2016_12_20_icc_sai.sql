UPDATE `creature_template` SET `mechanic_immune_mask` = 42679295 WHERE entry IN (37007, 38031);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (36724, 37012) AND `action_type` = 11 AND `action_param1` = 70980;

UPDATE `creature_template` SET `AIName` = 'SmartAI' WHERE `entry` = 37098;

DELETE FROM `smart_scripts` WHERE (source_type = 0 AND entryorguid = 37098);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(37098, 0, 0, 0, 0, 0, 100, 1, 3000, 5000, 0, 0, 11, 71906, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Val\'Kyr Herald - Cast Severed Essence');

UPDATE `creature_template` SET `ScriptName` = 'npc_icc_severed_essence' WHERE `entry` = 38410;

DELETE FROM spell_script_names WHERE spell_id = 71907;
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES
(71907, 'spell_icc_severed_essence');

UPDATE spell_dbc SET Effect1 = 77, EffectImplicitTargetA1 = 1 WHERE Id = 71907;
