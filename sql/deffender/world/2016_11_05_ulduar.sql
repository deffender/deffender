DELETE FROM `gameobject` WHERE `id` IN (194200, 194201);
INSERT INTO `gameobject` (`id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `VerifiedBuild`) VALUES
(194200, 603, 1, 1, 2036.1767, -201.3969, 432.688, 3.328515, 0, 0, 0, 0, 300, 0, 1, 0),
(194201, 603, 2, 1, 2036.1767, -201.3969, 432.688, 3.328515, 0, 0, 0, 0, 300, 0, 1, 0);
