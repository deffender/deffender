DELETE FROM `spell_script_names` WHERE `spell_id` IN (62275, 62439, 62450, 62653, 62688);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (62439, 'spell_freya_iron_roots_cast'), (62275, 'spell_freya_iron_roots_cast');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (62450, 'spell_freya_unstable_sun_beam');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (62653, 'spell_freya_tidal_wave');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (62688, 'spell_freya_summon_lashers');
