DELETE FROM `spell_script_names` WHERE `spell_id` = 20375;
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (20375, 'spell_pal_seal_of_command');
UPDATE spell_proc SET SchoolMask = 3, SpellTypeMask = 1, Cooldown = 0 WHERE SpellId = 20375;
