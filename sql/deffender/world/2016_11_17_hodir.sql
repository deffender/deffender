UPDATE `gameobject` SET `spawntimesecs` = 300, `animprogress` = 0 WHERE `id` IN (194201, 194200);
UPDATE `creature_template` SET `unit_flags` = `unit_flags` & (~0x200) WHERE `entry` = 32900;

UPDATE `creature_template` SET `unit_flags` = 393220, `InhabitType` = 11, `mechanic_immune_mask` = 612597599, `flags_extra` = `flags_extra` & (~0x80), `ScriptName` = 'npc_flash_freeze_player' WHERE `entry` = 32926;
UPDATE `creature_template` SET `ScriptName` = 'npc_flash_freeze_helper' WHERE `entry` = 32938;
UPDATE `creature_template` SET `flags_extra` = `flags_extra` & (~0x80) WHERE `entry` = 33352;
