DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceGroup` = 7 AND `SourceEntry` IN (62909, 62911, 62906, 62533, 64414) AND `SourceId` = 0;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 7, 62909, 0, 1, 31, 0, 3, 33370, 0, 0, 0, 0, '', 'Spell Mimiron\'s Inferno - target Mimiron\'s Inferno'),
(13, 7, 62911, 0, 1, 31, 0, 3, 33365, 0, 0, 0, 0, '', 'Spell Thorim\'s Hammer - target Thorim\'s hammer'),
(13, 7, 62906, 0, 1, 31, 0, 3, 33367, 0, 0, 0, 0, '', 'Spell Freya\'s Ward - target Freya\'s Ward'),
(13, 7, 62533, 0, 1, 31, 0, 3, 33212, 0, 0, 0, 0, '', 'Spell Hodir\'s fury - target Hodir\'s fury'),
(13, 7, 64414, 0, 1, 31, 0, 3, 33109, 0, 0, 0, 0, '', 'Spell Load Into Catapult - target Salvaged Demolisher');

UPDATE `creature_template` SET `unit_flags` = `unit_flags` | 33554432, `InhabitType` = 4 WHERE `entry` IN (33364, 33369, 33108, 33366);

DELETE FROM `spell_script_names` WHERE `spell_id` = 62907;
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (62907, 'spell_leviathan_freyas_ward');

DELETE FROM `vehicle_template_accessory` WHERE `entry` IN (33114);
INSERT INTO `vehicle_template_accessory` (`entry`, `accessory_entry`, `seat_id`, `minion`, `description`, `summontype`, `summontimer`) VALUES
(33114, 33142, 1, 1, 'Flame Leviathan Seat', 6, 30000),
(33114, 33143, 2, 1, 'Flame Leviathan Seat', 6, 30000);

DELETE FROM `achievement_criteria_data` WHERE (`ScriptName` = 'achievement_unbroken') OR (`criteria_id` IN (10044, 10045) AND `type` = 18);
INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`, `ScriptName`) VALUES
(10044, 18, 0, 0, ''),
(10045, 18, 0, 0, '');

UPDATE `creature_template` SET `unit_flags` = 33554432, `flags_extra` = 128, `AIName` = 'SmartAI' WHERE entry = 34054;
REPLACE INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `ScriptName`, `VerifiedBuild`) VALUES
(213818, 34054, 603, 0, 0, 1, 1, 0, 1, -688.056, -54.2622, 428.04, 0.0528037, 300, 0, 0, 75600, 0, 0, 0, 0, 0, '', 0);

DELETE FROM `creature_text` WHERE `CreatureID` IN (34054, 33579, 33624);
INSERT INTO `creature_text` (`CreatureID`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(34054, 0, 0, 'Okay! Let\'s move out. Get into your machines; I\'ll speak to you from here via the radio!', 12, 0, 0, 0, 0, 15807, 34157, 2, 'Bronzebeard Radio - Intro'),
(34054, 1, 0, 'The iron dwarves have been seen emerging from the bunkers at the base of the pillars straight ahead of you! Destroy the bunkers, and they\'ll be forced to fall back!', 12, 0, 0, 0, 0, 15794, 34145, 2, 'Bronzebeard Radio - Intro 2'),
(34054, 2, 0, 'There are four generators powering the defense structures. If you sabotage the generators, the missile attacks will stop!', 12, 0, 0, 0, 0, 15796, 34147, 2, 'Bronzebeard Radio - Intro 3'),
(34054, 3, 0, 'This tower powers the Hammer of Hodir. It is said to have the power to turn entire armies to ice! ', 12, 0, 0, 0, 0, 15797, 34148, 2, 'Bronzebeard Radio - Tower of Frost'),
(34054, 4, 0, 'You\'re approaching the tower of Freya. It contains the power to turn barren wastelands into jungles teeming with life overnight.', 12, 0, 0, 0, 0, 15798, 34149, 2, 'Bronzebeard Radio - Tower of Life'),
(34054, 5, 0, 'This generator powers Mimiron\'s Gaze. In moments, it can turn earth to ash, stone to magma--we cannot let it reach full power!', 12, 0, 0, 0, 0, 15799, 34150, 2, 'Bronzebeard Radio - Tower of Flames'),
(34054, 6, 0, 'Ah, the tower of Krolmir. It is said that the power of Thorim has been used only once... and that it turned an entire continent to dust. ', 12, 0, 0, 0, 0, 15801, 34151, 2, 'Bronzebeard Radio - Tower of Storms'),
(34054, 7, 0, 'It appears you are near a repair station! Drive your vehicle onto the platform, and it should be automatically repaired.', 12, 0, 0, 0, 0, 15803, 34153, 2, 'Bronzebeard Radio - Repair Station'),
(34054, 8, 0, 'You\'ve done it! You\'ve broken the defenses of Ulduar. In a few moments, we will be dropping in to...', 12, 0, 0, 0, 0, 15804, 34154, 2, 'Bronzebeard Radio - Before Leviathan pull 1'),
(34054, 9, 0, 'What is that? Be careful! Something\'s headed your way!', 12, 0, 0, 0, 0, 15805, 34155, 2, 'Bronzebeard Radio - Before Leviathan pull 2'),
(34054, 10, 0, 'Quickly! Evasive action! Evasive act--', 12, 0, 0, 0, 0, 15806, 34156, 2, 'Bronzebeard Radio - Before Leviathan pull 3'),
(34054, 11, 0, 'The control unit for the missile generator is on the top floor of the tower. If you can find a way to get up there, you can shut it down yourselves.', 12, 0, 0, 0, 0, 15802, 34152, 2, 'Bronzebeard Radio - '),
(34054, 12, 0, 'Watch out! Our air scouts report that the generators for the missile silos are coming online!', 12, 0, 0, 0, 0, 15795, 34146, 2, 'Bronzebeard Radio - '),
(33579, 0, 0, 'Pentarus, you heard the $gman:woman;. Have your mages release the shield and let these brave souls through!', 14, 0, 0, 0, 0, 0, 33671, 0, 'Brann - Intro 1'),
(33624, 0, 0, 'Of course, Brann. We will have the shield down momentarily.', 14, 0, 0, 0, 0, 0, 33673, 0, 'Pentarus - Intro 1'),
(33624, 1, 0, 'Mages of the Kirin Tor, on Brann\'s Command, release the shield! Defend this platform and our allies with your lives! For Dalaran!', 14, 0, 0, 0, 0, 0, 33677, 0, 'Pentarus - Intro 2'),
(33579, 1, 0, 'Our allies are ready. Bring down the shield and make way!', 14, 0, 0, 0, 0, 0, 33687, 0, 'Brann - Intro 2');

DELETE FROM `smart_scripts` WHERE (source_type = 0 AND entryorguid = 33701);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(33701, 0, 0, 0, 38, 0, 100, 1, 1, 0, 0, 0, 80, 3370100, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'High Explorer Delorah - On set data (from AreaTrigger) - Start intro talk');

DELETE FROM `smart_scripts` WHERE (source_type = 9 AND entryorguid = 3370100);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(3370100, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'High Explorer Dellorah - Say Line 0'),
(3370100, 9, 1, 0, 0, 0, 100, 0, 9000, 9000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 10, 136605, 33686, 0, 0, 0, 0, 0, 'Lore Keeper of Norgannon - Say Line 0'),
(3370100, 9, 2, 0, 0, 0, 100, 0, 13000, 13000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'High Explorer Dellorah - Say Line 1'),
(3370100, 9, 3, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 10, 136605, 33686, 0, 0, 0, 0, 0, 'Lore Keeper of Norgannon - Say Line 1'),
(3370100, 9, 4, 0, 0, 0, 100, 0, 11000, 11000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'High Explorer Dellorah - Say Line 2'),
(3370100, 9, 5, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 2, 0, 0, 0, 0, 0, 10, 136605, 33686, 0, 0, 0, 0, 0, 'Lore Keeper of Norgannon - Say Line 2'),
(3370100, 9, 6, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 3, 0, 0, 0, 0, 0, 10, 136605, 33686, 0, 0, 0, 0, 0, 'Lore Keeper of Norgannon - Say Line 3'),
(3370100, 9, 7, 0, 0, 0, 100, 0, 12000, 12000, 0, 0, 1, 3, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'High Explorer Dellorah - Say Line 3'),
(3370100, 9, 8, 0, 0, 0, 100, 0, 7000, 7000, 0, 0, 1, 4, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'High Explorer Dellorah - Say Line 4'),
(3370100, 9, 9, 0, 0, 0, 100, 0, 6000, 6000, 0, 0, 1, 5, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'High Explorer Dellorah - Say Line 5'),
(3370100, 9, 10, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 1, 0, 2000, 0, 0, 0, 0, 10, 136761, 33696, 0, 0, 0, 0, 0, 'Archmage Rhydian - Say Line 0'),
(3370100, 9, 11, 0, 0, 0, 100, 0, 9000, 9000, 0, 0, 1, 4, 0, 0, 0, 0, 0, 10, 136605, 33686, 0, 0, 0, 0, 0, 'Lore Keeper of Norgannon - Say Line 4'),
(3370100, 9, 12, 0, 0, 0, 100, 0, 13000, 13000, 0, 0, 1, 6, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'High Explorer Dellorah - Say Line 6');

UPDATE `creature_template` SET `AIName` = 'SmartAI', `ScriptName` = '' WHERE `entry` = 33579;
UPDATE `creature_template` SET `ScriptName` = 'npc_ulduar_gauntlet_generator' WHERE `entry` = 33571;

DELETE FROM `smart_scripts` WHERE (source_type = 0 AND entryorguid = 33579);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(33579, 0, 0, 1, 62, 0, 100, 513, 10355, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Close gossip for player'),
(33579, 0, 1, 2, 61, 0, 100, 0, 0, 0, 0, 0, 83, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Remove gossip flag'),
(33579, 0, 2, 3, 61, 0, 100, 0, 0, 0, 0, 0, 80, 3357900, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Start intro talk script'),
(33579, 0, 3, 0, 61, 0, 100, 0, 0, 0, 0, 0, 83, 1, 0, 0, 0, 0, 0, 10, 136605, 33686, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Remove gossip flag for Lorekeeper');

DELETE FROM `smart_scripts` WHERE (source_type = 9 AND entryorguid = 3357900);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(3357900, 9, 0, 0, 0, 0, 100, 0, 1000, 1000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 10, 136253, 33579, 0, 0, 0, 0, 0, 'Brann Bronzebeard - Say Line 0'),
(3357900, 9, 1, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 10, 136281, 33624, 0, 0, 0, 0, 0, 'Archmage Pentarus - Say Line 0'),
(3357900, 9, 2, 0, 0, 0, 100, 0, 9000, 9000, 0, 0, 1, 0, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Say Line 0'),
(3357900, 9, 3, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 1, 0, 0, 0, 0, 0, 10, 136281, 33624, 0, 0, 0, 0, 0, 'Archmage Pentarus - Say Line 1'),
(3357900, 9, 4, 0, 0, 0, 100, 0, 12000, 12000, 0, 0, 41, 0, 0, 0, 0, 0, 0, 14, 50363, 0, 0, 0, 0, 0, 0, 'Ulduar Protective Shield - Despawn'),
(3357900, 9, 5, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 10, 136253, 33579, 0, 0, 0, 0, 0, 'Brann Bronzebeard - Say Line 1');

DELETE FROM `smart_scripts` WHERE (source_type = 0 AND entryorguid = 34054) OR (source_type = 9 AND entryorguid = 3405400);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(34054, 0, 8, 9, 38, 0, 100, 1, 1, 8, 0, 0, 80, 3405400, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Start Timed Actionlist (Pull Leviathan)'),
(3405400, 9, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 1, 8, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Talk Pull Leviathan 1'),
(3405400, 9, 1, 0, 0, 0, 100, 0, 5000, 5000, 0, 0, 1, 9, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Talk Pull Leviathan 2'),
(3405400, 9, 2, 0, 0, 0, 100, 0, 8000, 8000, 0, 0, 1, 10, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Talk Pull Leviathan 3');

UPDATE `gameobject` SET `spawntimesecs` = 604800 WHERE `guid` = 50363;

DELETE FROM `areatrigger_scripts` WHERE `entry` IN (5359, 5414, 5415, 5416, 5417, 5443, 5428, 5442);
INSERT INTO `areatrigger_scripts` (`entry`, `ScriptName`) VALUES
(5359, 'SmartTrigger'),
(5414, 'SmartTrigger'),
(5415, 'SmartTrigger'),
(5416, 'SmartTrigger'),
(5417, 'SmartTrigger'),
(5443, 'SmartTrigger'),
(5428, 'SmartTrigger'),
(5442, 'SmartTrigger');

DELETE FROM `smart_scripts` WHERE (source_type = 2 AND entryorguid IN (5359, 5414, 5415, 5416, 5417, 5443, 5428));
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(5359, 2, 0, 0, 46, 0, 100, 1, 5359, 0, 0, 0, 45, 1, 0, 0, 0, 0, 0, 10, 136767, 33701, 0, 0, 0, 0, 0, 'Ulduar Enter - Start Intro Talk (No repeat)'),
(5414, 2, 0, 0, 46, 0, 100, 1, 5414, 0, 0, 0, 1, 4, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Trigger Bronzebeard Radio - Tower of Life'),
(5415, 2, 0, 0, 46, 0, 100, 1, 5415, 0, 0, 0, 1, 5, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Trigger Bronzebeard Radio - Tower of Flames'),
(5416, 2, 0, 0, 46, 0, 100, 1, 5416, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Trigger Bronzebeard Radio - Tower of Frost'),
(5417, 2, 0, 0, 46, 0, 100, 1, 5417, 0, 0, 0, 1, 6, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Trigger Bronzebeard Radio - Tower of Storms'),
(5443, 2, 0, 0, 46, 0, 100, 1, 5443, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Talk Intro 2'),
(5428, 2, 0, 0, 46, 0, 100, 1, 5428, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Talk Intro 3'),
(5442, 2, 0, 0, 46, 0, 100, 1, 5442, 0, 0, 0, 1, 7, 0, 0, 0, 0, 0, 10, 213818, 34054, 0, 0, 0, 0, 0, 'Bronzebeard Radio - Talk Repair Station')
