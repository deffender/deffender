-- Condition for source Spell implicit target condition type Near creature
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceGroup`=7 AND `SourceEntry`=63528 AND `SourceId`=0;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 7, 63528, 0, 1, 31, 0, 3, 33722, 0, 0, 0, 0, '', 'Spell Supercharged (effect 0 & 1 & 2) will hit the potential target of the spell if target is unit Storm Tempered Keeper within 10 yards.'),
(13, 7, 63528, 0, 2, 31, 0, 3, 33699, 0, 0, 0, 0, '', 'Spell Supercharged (effect 0 & 1 & 2) will hit the potential target of the spell if target is unit Storm Tempered Keeper within 10 yards.');

UPDATE `creature_template` SET `ScriptName` = 'npc_ulduar_storm_keeper' WHERE `entry` IN (33699, 33722);
UPDATE `creature_template` SET `ScriptName` = 'npc_ulduar_charged_sphere' WHERE `entry` = 33715;

DELETE FROM `spell_proc` WHERE `SpellId` = 62052;
INSERT INTO `spell_proc` (`SpellId`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `ProcFlags`, `SpellTypeMask`, `SpellPhaseMask`, `HitMask`, `AttributesMask`, `ProcsPerMinute`, `Chance`, `Cooldown`, `Charges`, `max_level`, `downscale`)
VALUES (62052, 0, 0, 0, 0, 0, 0, 0, 0, 16383, 2, 0, 100, 0, 0, 0, 0);
