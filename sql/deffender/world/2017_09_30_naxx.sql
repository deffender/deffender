UPDATE `creature_template` SET `mechanic_immune_mask` = 650854271 WHERE entry IN (16441, 30057);
UPDATE `creature_template` SET `ScriptName` = '', `AIName` = 'SmartAI', `Spell1` = 0 WHERE `entry` IN (16129, 16697);

DELETE FROM `smart_scripts` WHERE (source_type = 0 AND entryorguid = 16697);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(16697, 0, 0, 0, 11, 0, 100, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, ''),
(16697, 0, 1, 0, 60, 0, 100, 0, 1000, 1000, 1000, 1000, 11, 28865, 7, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '');

DELETE FROM `smart_scripts` WHERE (source_type = 0 AND entryorguid = 16129);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(16129, 0, 0, 0, 60, 0, 100, 1, 5500, 5500, 0, 0, 11, 27812, 7, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '');
