-- triggered spells proc
UPDATE `spell_proc` SET `AttributesMask` = 2 WHERE `SpellId` IN(-53569);
DELETE FROM `spell_proc` WHERE SpellId IN(70847);
INSERT INTO `spell_proc` (`SpellId`, `AttributesMask`) VALUES (70847, 2);
-- damage procs
UPDATE `spell_proc` SET `SpellTypeMask` = 1 WHERE `SpellId` IN(72417, 71903, 75457, -29723, 71573);
DELETE FROM `spell_proc` WHERE SpellId IN(71573, 71571);
INSERT INTO `spell_proc` (`SpellId`, `SpellTypeMask`) VALUES (71573, 1), (71571, 1);
-- proc on cast
UPDATE `spell_proc` SET `SpellPhaseMask`= 1 WHERE `SpellId` = -31833;
