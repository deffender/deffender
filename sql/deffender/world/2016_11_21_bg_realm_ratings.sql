SET @new2v2 = 2400;
SET @new3v3 = 2100;
SET @ilvl = "% 284%";
UPDATE creature_template SET HealthModifier = @new2v2, ManaModifier = @new3v3 WHERE ScriptName = "npc_bg_vendor" and name LIKE @ilvl;

SET @new2v2 = 2200;
SET @new3v3 = 1850;
SET @ilvl = "% 277%";
UPDATE creature_template SET HealthModifier = @new2v2, ManaModifier = @new3v3 WHERE ScriptName = "npc_bg_vendor" and name LIKE @ilvl;

SET @new2v2 = 2000;
SET @new3v3 = 1750;
SET @ilvl = "% 271%";
UPDATE creature_template SET HealthModifier = @new2v2, ManaModifier = @new3v3 WHERE ScriptName = "npc_bg_vendor" and name LIKE @ilvl;

SET @new2v2 = 1800;
SET @new3v3 = 1650;
SET @ilvl = "% 264%";
UPDATE creature_template SET HealthModifier = @new2v2, ManaModifier = @new3v3 WHERE ScriptName = "npc_bg_vendor" and name LIKE @ilvl;

SET @new2v2 = 1600;
SET @new3v3 = 1500;
SET @ilvl = "% 258%";
UPDATE creature_template SET HealthModifier = @new2v2, ManaModifier = @new3v3 WHERE ScriptName = "npc_bg_vendor" and name LIKE @ilvl;
