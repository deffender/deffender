DROP TABLE IF EXISTS `quest_realm_rewards`;
CREATE TABLE `quest_realm_rewards` (
  `ID` INT NOT NULL,
  `realm_id` VARCHAR(45) NULL,
  `realm_money` VARCHAR(45) NULL,
  PRIMARY KEY (`ID`, `realm_id`));

