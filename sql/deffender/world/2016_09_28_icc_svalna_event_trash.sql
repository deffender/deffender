UPDATE creature_template SET AIName="SmartAI" WHERE entry IN (37127, 37132, 37133, 37134);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (37127, 37132, 37133, 37134) AND `source_type`=0;

INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(37127,0,0,0,25,0,100,0,0,0,0,0,11,71270,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Frostbinder - Arctic Chill"),
(37133,0,0,0,0,0,100,0,2000,4000,7000,11000,11,41056,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Warlord - Whirlwind"),
(37132,0,0,0,0,0,100,0,2000,2000,2000,2000,11,71257,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Battle Maiden - Barbaric Strike"),
(37132,0,1,0,0,0,100,0,10000,12000,25000,30000,11,71258,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Battle Maiden - Adrenaline Rush"),
(37134,0,0,0,0,0,100,1,0,0,0,0,11,71251,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Huntress - Rapid Shot"),
(37134,0,1,0,0,1,100,0,1000,4000,8000,12000,11,71252,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Huntress - Volley"),
(37134,0,2,0,26,1,100,0,0,30,0,0,11,71253,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Huntress - Shoot"),
(37134,0,3,0,0,0,100,1,8000,8000,0,0,22,1,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Huntress - Phase 1"),
(37134,0,4,0,0,0,100,0,0,0,0,0,21,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Ymirjar Huntress - Don't move when shooting");
