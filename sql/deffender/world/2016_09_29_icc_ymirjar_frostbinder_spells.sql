DELETE FROM `spell_script_names` WHERE `spell_id` IN (71287, 71306);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(71287, 'spell_frostbinder_frozen_orb'),
(71306, 'spell_frostbinder_twisted_winds');
