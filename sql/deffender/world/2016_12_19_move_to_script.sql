DELETE FROM spell_script_names WHERE spell_id IN (38772,43937,59682,31956,43093,38801,48105,58517,59262,59263,35321,38363,39215,-980,28608,69404,71112,70911,72854,72855,72856);
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES
(38772, 'spell_gen_on_healed_eff01'),
(43937, 'spell_gen_on_healed_eff01'),
(59682, 'spell_gen_on_healed_eff01'),
(31956, 'spell_gen_on_healed_full0'),
(43093, 'spell_gen_on_healed_full0'),
(38801, 'spell_gen_on_healed_full0'),
(48105, 'spell_gen_on_healed_full1'),
(58517, 'spell_gen_on_healed_full0'),
(59262, 'spell_gen_on_healed_full0'),
(59263, 'spell_gen_on_healed_full1'),
(35321, 'spell_gen_on_healed_full0'),
(38363, 'spell_gen_on_healed_full0'),
(39215, 'spell_gen_on_healed_full0'),
(-980, 'spell_warl_curse_of_agony'),
(28608, 'spell_warl_curse_of_agony'),
(69404, 'spell_warl_curse_of_agony'),
(71112, 'spell_warl_curse_of_agony'),
(70911, 'spell_putricide_unbound_plague'),
(72854, 'spell_putricide_unbound_plague'),
(72855, 'spell_putricide_unbound_plague'),
(72856, 'spell_putricide_unbound_plague');

UPDATE spell_script_names SET ScriptName = 'spell_putricide_unbound_plague_search' WHERE spell_id = 70920 AND ScriptName = 'spell_putricide_unbound_plague';
UPDATE spell_script_names SET ScriptName = 'spell_gen_on_healed_eff01' WHERE ScriptName = 'spell_iron_ring_guard_impale';
