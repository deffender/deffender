UPDATE creature_template SET DamageModifier = 70, mechanic_immune_mask = mechanic_immune_mask | (0x2 | 0x10 | 0x20 | 0x100 | 0x200 | 0x1000 | 0x2000 | 0x10000 | 0x20000 | 0x80000) WHERE entry IN (40681, 40682);
UPDATE creature_template SET DamageModifier = 20 WHERE entry IN (40683, 40684);
