-- seal of righteous procs from judgement, Deadly Brew poisons proc
UPDATE `spell_proc` SET `AttributesMask`= 2 WHERE `SpellId` IN(20154, 21084, -51625);
-- seal of righteous proc only from dmg, Nightfall proc on cast rather than on hit
UPDATE `spell_proc` SET `SpellTypeMask`= 1 WHERE `SpellId` IN(20154, 21084, -18094);
