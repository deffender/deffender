DELETE FROM `spell_script_names` WHERE `ScriptName` IN ('spell_leviathan_vehicle_ride', 'spell_vehicle_grab_pyrite');

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(62309, 'spell_leviathan_vehicle_ride'),
(65031, 'spell_leviathan_vehicle_ride'),
(62482, 'spell_vehicle_grab_pyrite');

DELETE FROM `vehicle_template_accessory` WHERE `entry` IN (33109);
INSERT INTO `vehicle_template_accessory` (`entry`, `accessory_entry`, `seat_id`, `minion`, `description`, `summontype`, `summontimer`) VALUES
(33109, 33167, 1, 1, 'Salvaged Demolisher', 6, 30000),
(33109, 33620, 2, 1, 'Earthen Stoneshaper', 6, 30000);

DELETE FROM `smart_scripts` WHERE (`source_type` = 0 AND `entryorguid` = 33579);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(33579, 0, 0, 1, 62, 0, 100, 513, 10355, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Close gossip for player'),
(33579, 0, 1, 2, 61, 0, 100, 0, 0, 0, 0, 0, 83, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Remove gossip flag'),
(33579, 0, 2, 3, 61, 0, 100, 0, 0, 0, 0, 0, 80, 3357900, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Start intro talk script'),
(33579, 0, 3, 4, 61, 0, 100, 0, 0, 0, 0, 0, 83, 1, 0, 0, 0, 0, 0, 10, 136605, 33686, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Remove gossip flag for Lorekeeper'),
(33579, 0, 4, 0, 61, 0, 100, 0, 0, 0, 0, 0, 34, 79, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Brann Bronzebeard - On Gossip Select - Set instance data (spawn vehicles)');

DELETE FROM `creature` WHERE `id` IN (33060, 33062, 33109);
