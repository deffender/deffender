DELETE FROM `creature` WHERE `guid` IN (130958, 130959);

DELETE FROM `creature_summon_groups` WHERE `summonerId` = 15928 AND `summonerType` = 0;
INSERT INTO `creature_summon_groups` (`summonerId`, `summonerType`, `groupId`, `entry`, `position_x`, `position_y`, `position_z`, `orientation`, `summonType`, `summonTime`) VALUES
(15928, 0, 1, 15929, 3450.45, -2931.42, 312.092, 5.49779, 6, 30000),
(15928, 0, 2, 15930, 3508.14, -2988.65, 312.092, 2.37365, 6, 30000);

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceEntry` IN (28098, 28110);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`,
`ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`,
`ScriptName`, `Comment`) VALUES
(13, 1, 28098, 0, 0, 31, 0, 3, 15929, 0, 0, 0, 0, '', 'Stalagg Tesla Effect - target Stalagg'),
(13, 1, 28110, 0, 0, 31, 0, 3, 15930, 0, 0, 0, 0, '', 'Feugen Tesla Effect - target Feugen');
