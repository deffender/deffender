truncate `interrupt_logs`;
truncate `dispell_logs`;

ALTER TABLE `interrupt_logs`
CHANGE COLUMN `start` `start` INT(10) NOT NULL ,
CHANGE COLUMN `end` `end` INT(10) NOT NULL ,
CHANGE COLUMN `cast_time` `cast_time` INT(10) NOT NULL ,
ADD COLUMN `spell_id` INT(10) UNSIGNED NOT NULL AFTER `cast_time`;

ALTER TABLE `dispell_logs`
CHANGE COLUMN `delay` `delay` INT(15) NOT NULL ,
CHANGE COLUMN `duration` `duration` INT(15) NOT NULL ,
ADD COLUMN `spell_id` INT(15) UNSIGNED NOT NULL AFTER `duration`;

