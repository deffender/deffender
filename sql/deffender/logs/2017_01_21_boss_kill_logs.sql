CREATE TABLE `boss_kill_logs` (
  `log_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `entry` int UNSIGNED NOT NULL,
  `map` smallint UNSIGNED NOT NULL,
  `difficulty` tinyint UNSIGNED NOT NULL,
  `players` tinyint UNSIGNED NOT NULL,
  `fight_duration` smallint UNSIGNED NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE `boss_kill_player_logs` (
  `log_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `guid` int UNSIGNED NOT NULL,
  `name` varchar(12) NOT NULL,
  `account` int UNSIGNED NOT NULL,
  `ip` varchar(20) NOT NULL,
  
  PRIMARY KEY (`log_id`, `guid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

