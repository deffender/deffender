CREATE TABLE `character_bonus_rewards` (
  `guid` INT UNSIGNED NOT NULL,
  `em` INT UNSIGNED NULL,
  `eof` INT UNSIGNED NULL,
  `ap` INT UNSIGNED NULL,
  PRIMARY KEY (`guid`),
  UNIQUE INDEX `guid_UNIQUE` (`guid` ASC));

