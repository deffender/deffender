-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `bug_report`;
CREATE TABLE `bug_report` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `priority` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `createdBy` int(11) unsigned NOT NULL,
  `assignedTo` int(11) unsigned DEFAULT NULL,
  `resolvedBy` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `originalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bug_report_type` (`type`),
  KEY `bug_report_status` (`status`),
  KEY `bug_report_assignedTo` (`assignedTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `bug_report` (`id`, `type`, `status`, `priority`, `createdBy`, `assignedTo`, `resolvedBy`, `title`, `content`, `date`, `originalId`) VALUES
(1,	0,	4,	0,	2,	NULL,	NULL,	'ULD - Yogg brain',	'elity, ktere se nachazi v brainu yogga pouze pro \"efekt\" napadaji hrace.',	'2016-11-16 11:38:23',	1),
(2,	0,	5,	0,	2,	NULL,	NULL,	'ULD - Freya encounter',	'Po zabiti Freyi zustava encounter (nejde vejit ven), ackoliv combat nekterym hracum spadne',	'2016-11-16 11:38:23',	2),
(3,	0,	1,	0,	2,	NULL,	NULL,	'ULD - Razorscale',	'Razora nelze pritahnout k zemi, v polovine cesty dolu se zastavi, obcas je na range spelleru a pri 50% sleti, bohuzel vsak vetsinou na nej spelleri range nemaji.',	'2016-11-16 11:38:23',	3),
(4,	0,	1,	0,	2,	NULL,	NULL,	'ULD - XT srdce',	'Srdce se bud objevi na prvnich par sekund, pote zmizi, nebo se neobjevi vubec. Elity navic chodi jen pri prvni \"mezifazi\", pote uz se nedeje nic.',	'2016-11-16 11:38:23',	4),
(5,	0,	5,	0,	2,	NULL,	NULL,	'ULD - Kologarn ruce',	'Po zabiti Kologarna zustanou ruce nazivu, hracum nespadne combat a ruce maji hitbox pravdepodobne nekde dole s telem, tudiz se nedaji ani dodatecne zabit.',	'2016-11-16 11:38:23',	5),
(6,	0,	5,	0,	2,	NULL,	NULL,	'ULD - Hodir kostky',	'Na kostky u Hodira se neda utocit.',	'2016-11-16 11:38:23',	6),
(7,	0,	5,	0,	2,	NULL,	NULL,	'ULD - Thorim resisty',	'Spelly, ktere pouziva Sif se nedaji resistovat (Blizzard, Frostbolt Volley, Frostbolt, Frost Nova), u Thorimovo spellu si nejsem jisty, jestli vubec maji, popr. jestli resistuji.',	'2016-11-16 11:38:23',	7),
(8,	0,	5,	0,	2,	NULL,	NULL,	'ULD - Brana pred Vezzaxem',	'Brana pred Vezzaxem se neotevira.',	'2016-11-16 11:38:23',	8),
(9,	0,	1,	0,	2,	NULL,	NULL,	'ICC - LM Bonestorm',	'Lord se da pri Bonestormu tauntovat - po zmene targetu staci aby se tank pohnul a boss leti za nim.',	'2016-11-16 11:38:23',	9),
(10,	0,	1,	0,	2,	NULL,	NULL,	'ICC - GS Below Zero',	'Pri Below Zero spell obcas nezamrazi delo, mne osobne se to stalo 2x na hordacky lodi, mozna neco s crossfactionem?',	'2016-11-16 11:38:23',	10),
(11,	0,	1,	0,	2,	NULL,	NULL,	'ULD - Mimiron flamy',	'Mimiron v prvni fazi po druhem Shock blastu nehasi flamy, pri 2. fazi si nejsem jisty, pri 3. addi take nehasi.',	'2016-11-16 11:38:23',	11),
(12,	0,	1,	0,	2,	NULL,	NULL,	'DK Talents - Blood, Death rune mastery',	'report z fora, otestovano: http://wowwiki.wikia.com/wiki/Death_Rune_Mastery Talent neprocuje ani z jednoho spellu, death runy se proste neobjevi.',	'2016-11-16 11:38:23',	12),
(13,	0,	1,	0,	2,	NULL,	NULL,	'RS - Halion encounter po wipe',	'Pri wipe 25 man zustava encounter, par dni zpatky jsme byli 10hc a po 3 wipech (1 v 2. fazi, 2 ve 3. fazi) se Halion pokazde resetnul spravne, bug se tedy nachazi pouze na 25.',	'2016-11-16 11:38:23',	13),
(14,	0,	1,	0,	2,	NULL,	NULL,	'Q - The Purification of Quel\'Delar',	'report z fora, otestovano: https://www.youtube.com/watch?v=FILdWp5AaaQ Hadam ze vyse prilozene video je z offi - u nas chybi NPC pres SWP, je tam pouze quest giver, neni tedy mozne chain dokoncit.\nreport na foru: http://forum.deffender.eu/viewtopic.php?f=93&t=12167',	'2016-11-16 11:38:23',	14),
(15,	0,	5,	0,	2,	NULL,	NULL,	'Paladin spell - JoL + Beacon',	'Vsechny procy z Judgement of Light (heal i overheal) tikaji do Beacon of Light.',	'2016-11-16 11:38:23',	15),
(16,	0,	5,	0,	2,	NULL,	NULL,	'ICC - Sindragosa breath',	'Podivej se prosim te na ten breath ve 2. fazi, stale dava tankovi za 100k na 3. stacku mystic buffetu.',	'2016-11-16 11:38:23',	16),
(17,	0,	5,	0,	2,	NULL,	NULL,	'ICC - Vali hc stacky',	'25hc stacky nefunguji, sebrat se daji, debuff vsak nedela vubec nic.',	'2016-11-16 11:38:23',	17),
(18,	0,	5,	0,	2,	NULL,	NULL,	'Test',	'test',	'2016-11-19 02:05:42',	18),
(19,	0,	5,	0,	2,	NULL,	NULL,	'Kostky na sind?e',	'n?kdy nehodí kostky p?i air fázi, souvisí to pravd?podobn? s combat ressem',	'2016-11-21 14:42:54',	19);

DROP TABLE IF EXISTS `bug_report_changes`;
CREATE TABLE `bug_report_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bugreport_id` int(11) unsigned NOT NULL,
  `field` varchar(255) NOT NULL,
  `oldValue` varchar(255) NOT NULL,
  `newValue` varchar(255) NOT NULL,
  `user` int(10) unsigned DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bugreport_id` (`bugreport_id`),
  KEY `user` (`user`),
  CONSTRAINT `bug_report_changes_ibfk_1` FOREIGN KEY (`bugreport_id`) REFERENCES `bug_report` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bug_report_changes_ibfk_3` FOREIGN KEY (`user`) REFERENCES `bug_report_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `bug_report_changes` (`id`, `bugreport_id`, `field`, `oldValue`, `newValue`, `user`, `date`) VALUES
(3,	19,	'status',	'1',	'2',	1,	'2016-11-23 23:21:15'),
(4,	1,	'status',	'1',	'4',	1,	'2016-11-30 13:41:34'),
(5,	2,	'status',	'1',	'5',	1,	'2016-11-30 13:41:39'),
(6,	5,	'status',	'1',	'5',	1,	'2016-11-30 13:41:54'),
(7,	6,	'status',	'1',	'5',	1,	'2016-11-30 13:41:57'),
(8,	7,	'status',	'1',	'5',	1,	'2016-11-30 13:42:00'),
(9,	8,	'status',	'1',	'5',	1,	'2016-11-30 13:42:11'),
(10,	15,	'status',	'1',	'5',	1,	'2016-11-30 13:43:23'),
(11,	16,	'status',	'1',	'5',	1,	'2016-11-30 13:43:27'),
(12,	17,	'status',	'1',	'5',	1,	'2016-11-30 13:43:29'),
(13,	18,	'status',	'1',	'5',	1,	'2016-11-30 13:43:33'),
(14,	19,	'status',	'2',	'5',	1,	'2016-11-30 13:43:58');

DROP TABLE IF EXISTS `bug_report_comments`;
CREATE TABLE `bug_report_comments` (
  `id` int(11) NOT NULL,
  `bugreport_id` int(11) unsigned NOT NULL,
  `written_by` int(10) unsigned DEFAULT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `bugreport_id` (`bugreport_id`),
  KEY `written_by` (`written_by`),
  CONSTRAINT `bug_report_comments_ibfk_1` FOREIGN KEY (`bugreport_id`) REFERENCES `bug_report` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bug_report_comments_ibfk_2` FOREIGN KEY (`written_by`) REFERENCES `bug_report_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `bug_report_comments` (`id`, `bugreport_id`, `written_by`, `content`, `date`) VALUES
(0,	19,	1,	'resistovaly, fixed',	'2016-11-29 22:42:38');

DROP TABLE IF EXISTS `bug_report_users`;
CREATE TABLE `bug_report_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `joined` datetime NOT NULL,
  `display_name` varchar(20) NOT NULL,
  `roles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `bug_report_users` (`id`, `account_id`, `username`, `password`, `joined`, `display_name`, `roles`) VALUES
(1,	1064453,	'DEVLACHTANIK',	'$2y$10$DihmTnTVLqbYO23OF5vlt.jNESKDmF5MIE46R/FhddKo/QfUJPrey',	'2016-11-23 23:04:55',	'Lachtanik',	'reporter,developer'),
(2,	922055,	'BAGOS',	NULL,	'2016-11-23 23:09:50',	'Rix',	'reporter');

SET foreign_key_checks = 1;
