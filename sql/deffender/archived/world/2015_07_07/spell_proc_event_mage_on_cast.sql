DELETE FROM spell_linked_spell WHERE spell_effect IN(-48108,-44401,-57761);

REPLACE INTO `spell_proc_event` VALUES 
('57761', '4', '3', '1', '4096', '0', '33554432', '0', '0', '0', '0', '0', '0'),   -- Brain Freeze
('48108', '4', '3', '4194304', '0', '0', '33554432', '0', '0', '0', '0', '0', '0'),-- Hot Streak
('44401', '64', '3', '2048', '0', '0', '33554432', '0', '0', '0', '0', '0', '0');  -- Missile Barrage
UPDATE `spell_proc_event` SET `procFlags`='33554432' WHERE `entry`='74396';        -- Fingers of Frost
