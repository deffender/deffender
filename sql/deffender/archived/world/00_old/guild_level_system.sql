REPLACE INTO `trinity_string` VALUES ('13000', 'Guild Level: %u.', null, 
null, null, null, null, null, null, null);
REPLACE INTO `trinity_string` VALUES ('13001', 'Guild Experience: 
%u/%u.', null, null, null, null, null, null, null, null);

REPLACE INTO `command` VALUES ('guild setlevel', '1000', 'Syntax: .guild 
setlevel $GuildName $Level');
REPLACE INTO `command` VALUES ('guild givexp', '1001', 'Syntax: .guild 
givexp $GuildName $Xp');
