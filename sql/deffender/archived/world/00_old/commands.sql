DELETE FROM `command` WHERE `permission` IN(1000,1001,1002,1003,1004,1005,1007,794);
INSERT INTO `command` VALUES
('u', 1000, 'Syntax: .guild\nsetlevel $GuildName $Level'),
('u mmr', 1001, 'Syntax: .guild\nsetlevel $GuildName $Level'),
('spec leave', 1002, 'Leaves arena spectate mode'),
('guild info', 1003, 'Shows information about target\'s guild or a given guild identifier or name.'),
('guild setlevel', 1004, 'Syntax: .guild setlevel $GuildName $Level'),
('guild givexp', 1005, 'Syntax: .guild givexp $GuildName $Xp'),
('account set eventid', 1007, 'Forces all your object and creatures spawns to be spawned directly under world event\nSyntax: .account set eventid $id');
