-- disable proc of all these spells above level 63
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 17 AND `SourceEntry` IN (15366,22817,22818,22888,22820,24425);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, 
`ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, 
`ScriptName`, `Comment` ) VALUES 
(17, 0, 15366, 0, 0, 27, 1, 63, 4, 0, 0, 0, 0, '', 'Songflower Serenade'),
(17, 0, 22817, 0, 0, 27, 1, 63, 4, 0, 0, 0, 0, '', 'Fengus Ferocity'),
(17, 0, 22818, 0, 0, 27, 1, 63, 4, 0, 0, 0, 0, '', 'Moldars Moxie'),
(17, 0, 22888, 0, 0, 27, 1, 63, 4, 0, 0, 0, 0, '', 'Rallying Cry of the Dragonslayer'),
(17, 0, 22820, 0, 0, 27, 1, 63, 4, 0, 0, 0, 0, '', 'Slipkiks Savvy'),
(17, 0, 24425, 0, 0, 27, 1, 63, 4, 0, 0, 0, 0, '', 'Spirit of Zandalar');

-- disable spell power scaling of some spells
DELETE FROM `spell_bonus_data` WHERE `entry` IN (7712,7714,10577,16614,18798,27655,28788,38395,55756,6297,13897,20004,28005,20006,44525,28715,38616,43731,43733,20007,20005);
INSERT INTO `spell_bonus_data` (`entry`, `direct_bonus`, `dot_bonus`, `ap_bonus`, `ap_dot_bonus`, `comments`) VALUES
-- items
(7712,0,0,0,0,'Item - Blazefury Medallion & Fiery Retributor (Fire Strike)'),
(7714,0,0,0,0,'Item - Fiery Plate Gauntlets (Fire Strike)'),
(10577,0,0,0,0,'Item - Gauntlets of the Sea (Heal)'),
(16614,0,0,0,0,'Item - Storm Gauntlets (Lightning Strike)'),
(18798,0,0,0,0,'Item - Freezing Band (Freeze)'),
(27655,0,0,0,0,'Item - Heart of Wyrmthalak (Flame Lash)'),
(28788,0,0,0,0,'Item - Paladin T3 (8)'),
(38395,0,0,0,0,'Item - Warlock T5 (2)'),
(55756,0,0,0,0,'Item - Brunnhildar weapons (Chilling Blow)'),
-- enchants
(6297,0,0,0,0,'Enchant - Fiery Blaze'),
(13897,0,0,0,0,'Enchant - Fiery Weapon'),
(28005,0,0,0,0,'Enchant - Battlemaster'),
(20006,0,0,0,0,'Enchant - Unholy Weapon'),
(44525,0,0,0,0,'Enchant - Icebreaker'),
-- Consumables
(28715,0,0,0,0,'Consumable - Flamecap (Flamecap Fire)'),
(38616,0,0,0,0,'Poison - Bloodboil Poison'),
(43731,0,0,0,0,'Consumable - Stormchops (Lightning Zap)'),
(43733,0,0,0,0,'Consumable - Stormchops (Lightning Zap)');
-- missing comment
UPDATE `spell_bonus_data` SET `comments`='Enchant - Deathfrost' WHERE `entry`=46579;

ALTER TABLE `spell_bonus_data`
ADD COLUMN max_level tinyint(3) UNSIGNED DEFAULT 0,
ADD COLUMN downscale tinyint(3) UNSIGNED DEFAULT 0;

-- reduced effects for Lifestealing, crusader, Icy Chill (also disable sp calc. for Lifestealing)
INSERT INTO `spell_bonus_data` (entry, direct_bonus, dot_bonus, ap_bonus, ap_dot_bonus, comments, max_level, downscale) VALUES
(20004,0,0,0,0,'Enchant - Lifestealing',60,4),
(20007,-1,-1,-1,-1,'Enchant - Crusader',60,4),
(20005,-1,-1,-1,-1,'Enchant - Icy Chill',60,4);

ALTER TABLE `spell_enchant_proc_data`
ADD COLUMN max_level tinyint(3) UNSIGNED DEFAULT 0,
ADD COLUMN downscale tinyint(3) UNSIGNED DEFAULT 0;

-- Icy Chill enchant - 4% proc chance downscale per level
UPDATE `spell_enchant_proc_data` SET max_level = 60, downscale = 4 WHERE entry = 1894;

ALTER TABLE `spell_proc`
ADD COLUMN max_level tinyint(3) UNSIGNED DEFAULT 0,
ADD COLUMN downscale tinyint(3) UNSIGNED DEFAULT 0;

ALTER TABLE `spell_proc_event`
ADD COLUMN max_level tinyint(3) UNSIGNED DEFAULT 0,
ADD COLUMN downscale tinyint(3) UNSIGNED DEFAULT 0;
