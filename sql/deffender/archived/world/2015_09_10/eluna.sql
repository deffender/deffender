CREATE TABLE IF NOT EXISTS `lua_scripts` (
  `ScriptName` VARCHAR(45) NOT NULL,
  `Script` TEXT NOT NULL,
  PRIMARY KEY (`ScriptName`),
  UNIQUE INDEX `ScriptName_UNIQUE` (`ScriptName` ASC));
