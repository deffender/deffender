ALTER TABLE `item_template` 
ADD COLUMN `BuyPrice_realm_2` BIGINT(20) NOT NULL DEFAULT '0' AFTER `BuyPrice`,
ADD COLUMN `SellPrice_realm_2` INT(10) NOT NULL DEFAULT '0' AFTER `SellPrice`;
