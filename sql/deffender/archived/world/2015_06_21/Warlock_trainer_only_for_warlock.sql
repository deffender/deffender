DELETE FROM `gossip_menu_option` WHERE `menu_id`=1001;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`,`option_text`,`option_id`,`npc_option_npcflag`,`action_menu_id`,`action_poi_id`,`box_coded`,`box_money`,`box_text`, `OptionBroadcastTextID`)VALUES
(1001,0,3,"I would like training.",5,16,0,0,0,0,'', 2603),
(1001,1,0,"I wish to unlearn my talents.",16,16,4461,0,0,0,'', 8271),
(1001,2,0,"I wish to know about Dual Talent Specialization.",1,1,10371,0,0,0,'',33762);
DELETE FROM `gossip_menu` WHERE `Entry`=1001 AND `text_id` IN (4999, 5000);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES (1001, 4999), (1001, 5000);
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` IN (14,15) AND `SourceGroup`=1001;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`ErrorTextId`,`ScriptName`,`Comment`, `NegativeCondition`) VALUES
(14, 1001, 5000, 0, 15, 256, 0, 0, 0, '', 'Show gossip text if player is a Warlock', 0),
(14, 1001, 4999, 0, 15, 256, 0, 0, 0, '', 'Show gossip text if player is not a Warlock', 1),
(15, 1001, 0, 0, 15, 256, 0, 0, 0, '', 'Show gossip option if player is a Warlock', 0),
(15, 1001, 1, 0, 15, 256, 0, 0, 0, '', 'Show gossip option if player is a Warlock', 0),
(15, 1001, 2, 0, 15, 256, 0, 0, 0, '', 'Show gossip option if player is a Warlock', 0);
UPDATE `creature_template` SET `gossip_menu_id`=1001 WHERE  `entry`=26331;