ALTER TABLE `creature` DROP COLUMN `realm_id`;
ALTER TABLE `gameobject` DROP COLUMN `realm_id`;
ALTER TABLE `creature_template` ADD COLUMN `realm_id` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `VerifiedBuild`;
ALTER TABLE `gameobject_template` ADD COLUMN `realm_id` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `VerifiedBuild`;

