REPLACE INTO spell_area  VALUES
(100001,4710,0,0,0,0,2,1,64,11),
(100001,3277,0,0,0,0,2,1,64,11),
(100001,3358,0,0,0,0,2,1,64,11),
(100001,2597,0,0,0,0,2,1,64,11),
(100001,3820,0,0,0,0,2,1,64,11),
(100001,4384,0,0,0,0,2,1,64,11);

REPLACE INTO spell_dbc (Id, AttributesEx3, DurationIndex, Effect1, Effect2, EffectApplyAuraName1, EffectApplyAuraName2,
EffectMiscValue1, EffectMiscValue2, `Comment`) VALUES
(100002, 1048576, 21, 6, 6, 139, 139, 730, 1732, 'Force alliance forces reaction'),
(100003, 1048576, 21, 6, 6, 139, 139, 729, 1735, 'Force horde forces reaction');

REPLACE INTO spell_script_names VALUES
(100002, 'spell_bg_force_reaction'),
(100003, 'spell_bg_force_reaction');
