DELIMITER $$

DROP PROCEDURE IF EXISTS upg_gm_struct $$
CREATE PROCEDURE upg_gm_struct()
BEGIN

IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='startable_event_id_list' AND TABLE_NAME='gm_struct') ) THEN
    ALTER TABLE `gm_struct` ADD COLUMN `startable_event_id_list` TEXT NULL DEFAULT NULL AFTER `usable_event_id_list`;
END IF;

END $$

CALL upg_gm_struct() $$

DELIMITER ;