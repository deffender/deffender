/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50630
Source Host           : localhost:3306
Source Database       : logs

Target Server Type    : MYSQL
Target Server Version : 50630
File Encoding         : 65001

Date: 2016-04-20 14:35:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for character_logs
-- ----------------------------
DROP TABLE IF EXISTS `character_logs`;
CREATE TABLE `character_logs` (
  `guid` int(9) unsigned NOT NULL,
  `name` varchar(12) NOT NULL,
  `account` int(9) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of character_logs
-- ----------------------------

-- ----------------------------
-- Table structure for daily_players_reports
-- ----------------------------
DROP TABLE IF EXISTS `daily_players_reports`;
CREATE TABLE `daily_players_reports` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `creation_time` int(10) unsigned NOT NULL DEFAULT '0',
  `average` float NOT NULL DEFAULT '0',
  `total_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `speed_or_tele_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fly_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `jump_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `waterwalk_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `teleportplane_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `climb_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `air_walk_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of daily_players_reports
-- ----------------------------

-- ----------------------------
-- Table structure for dispell_logs
-- ----------------------------
DROP TABLE IF EXISTS `dispell_logs`;
CREATE TABLE `dispell_logs` (
  `guid` int(10) unsigned NOT NULL,
  `name` varchar(12) NOT NULL,
  `account` int(10) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delay` int(11) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dispell_logs
-- ----------------------------

-- ----------------------------
-- Table structure for gm_logs
-- ----------------------------
DROP TABLE IF EXISTS `gm_logs`;
CREATE TABLE `gm_logs` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `player` varchar(12) NOT NULL,
  `account` int(9) NOT NULL,
  `command` varchar(255) NOT NULL,
  `position` varchar(96) NOT NULL,
  `selected` varchar(96) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account` (`account`),
  KEY `player` (`player`)
) ENGINE=InnoDB AUTO_INCREMENT=618871 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gm_logs
-- ----------------------------

-- ----------------------------
-- Table structure for interrupt_logs
-- ----------------------------
DROP TABLE IF EXISTS `interrupt_logs`;
CREATE TABLE `interrupt_logs` (
  `guid` int(9) unsigned NOT NULL,
  `name` varchar(12) NOT NULL,
  `account` int(9) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `start` smallint(5) NOT NULL,
  `end` smallint(5) NOT NULL,
  `cast_time` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of interrupt_logs
-- ----------------------------

-- ----------------------------
-- Table structure for item_logs
-- ----------------------------
DROP TABLE IF EXISTS `item_logs`;
CREATE TABLE `item_logs` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `guid` int(9) NOT NULL,
  `player` varchar(12) NOT NULL,
  `account` int(9) NOT NULL,
  `item` int(6) NOT NULL,
  `item_guid` bigint(20) DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  `position` varchar(96) NOT NULL,
  `target` varchar(96) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`)
) ENGINE=InnoDB AUTO_INCREMENT=570303 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of item_logs
-- ----------------------------

-- ----------------------------
-- Table structure for players_reports_status
-- ----------------------------
DROP TABLE IF EXISTS `players_reports_status`;
CREATE TABLE `players_reports_status` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `creation_time` int(10) unsigned NOT NULL DEFAULT '0',
  `average` float NOT NULL DEFAULT '0',
  `total_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `speed_or_tele_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fly_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `jump_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `waterwalk_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `teleportplane_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `climb_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  `air_walk_reports` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of players_reports_status
-- ----------------------------

-- ----------------------------
-- Table structure for player_arena_logs
-- ----------------------------
DROP TABLE IF EXISTS `player_arena_logs`;
CREATE TABLE `player_arena_logs` (
  `log_id` int(11) unsigned NOT NULL,
  `guid` int(11) unsigned NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `damage` int(11) unsigned DEFAULT NULL,
  `heal` int(11) unsigned DEFAULT NULL,
  `kills` int(11) unsigned DEFAULT NULL,
  `rating` smallint(5) unsigned DEFAULT NULL,
  `rating_change` smallint(6) DEFAULT NULL,
  `arena_team` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`log_id`,`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of player_arena_logs
-- ----------------------------

-- ----------------------------
-- Table structure for team_arena_logs
-- ----------------------------
DROP TABLE IF EXISTS `team_arena_logs`;
CREATE TABLE `team_arena_logs` (
  `logs_id` int(11) unsigned NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `winner_team_id` int(11) unsigned DEFAULT NULL,
  `loser_team_id` int(11) unsigned DEFAULT NULL,
  `winner_rat` smallint(6) unsigned DEFAULT NULL,
  `loser_rat` smallint(6) unsigned DEFAULT NULL,
  `winner_change` smallint(6) DEFAULT NULL,
  `loser_change` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`logs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of team_arena_logs
-- ----------------------------

-- ----------------------------
-- Table structure for updates
-- ----------------------------
DROP TABLE IF EXISTS `updates`;
CREATE TABLE `updates` (
  `name` varchar(200) NOT NULL COMMENT 'filename with extension of the update.',
  `hash` char(40) DEFAULT '' COMMENT 'sha1 hash of the sql file.',
  `state` enum('RELEASED','ARCHIVED') NOT NULL DEFAULT 'RELEASED' COMMENT 'defines if an update is released or archived.',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp when the query was applied.',
  `speed` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'time the query takes to apply in ms.',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='List of all applied updates in this database.';

-- ----------------------------
-- Records of updates
-- ----------------------------

-- ----------------------------
-- Table structure for updates_include
-- ----------------------------
DROP TABLE IF EXISTS `updates_include`;
CREATE TABLE `updates_include` (
  `path` varchar(200) NOT NULL COMMENT 'directory to include. $ means relative to the source directory.',
  `state` enum('RELEASED','ARCHIVED') NOT NULL DEFAULT 'RELEASED' COMMENT 'defines if the directory contains released or archived updates.',
  PRIMARY KEY (`path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='List of directories where we want to include sql updates.';

-- ----------------------------
-- Records of updates_include
-- ----------------------------
INSERT INTO `updates_include` VALUES ('$/sql/deffender/logs', 'RELEASED');
INSERT INTO `updates_include` VALUES ('$/sql/deffender/archived/logs', 'ARCHIVED');

-- ----------------------------
-- Table structure for wt_logs_info
-- ----------------------------
DROP TABLE IF EXISTS `wt_logs_info`;
CREATE TABLE `wt_logs_info` (
  `arena_team_id` int(9) NOT NULL,
  `banned` int(1) NOT NULL DEFAULT '0',
  `last_number` int(5) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `checked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`arena_team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wt_logs_info
-- ----------------------------
