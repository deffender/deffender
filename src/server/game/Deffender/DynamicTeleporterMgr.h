#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <map>
#include <vector>
#include "Position.h"

enum MenuVisibility : uint8
{
    MENU_VISIBILITY_ALL      = 0,
    MENU_VISIBILITY_VOTE     = 1,
    MENU_VISIBILITY_DONATE   = 2,
    MENU_VISIBILITY_MAX
};

struct TeleportData
{
    uint32 entry;
    int32 menu_sub;
    uint8  icon;
    uint32 faction;
    std::string name;
    uint32 realmId;
    uint32 questId;
    int32 eventId;
    MenuVisibility visibility;
};

/**
 * Thread-safe, but you are expected to acquire lock when using
 * the following methods: FindLocation, GetSubmenuData and HasSubmenu.
 * Load is expected to be called during World load and is not thread-safe!
 */
class TC_GAME_API DynamicTeleporterMgr
{
    using UniqueLock = boost::unique_lock<boost::shared_mutex>;

    bool m_loaded;
    boost::shared_mutex m_loadLock;
    MenuVisibility m_minVisibility;
    std::map<uint32, std::vector<TeleportData>> m_teleportData;
    std::map<uint32, WorldLocation> m_teleportLocs;

    DynamicTeleporterMgr() : m_loaded(false) {}

public:
    using SharedLock = boost::shared_lock<boost::shared_mutex>;

    static DynamicTeleporterMgr* instance();

    bool IsLoaded();
    MenuVisibility GetMinVisibility() const;
    // Not thread-safe!
    void Load();
    void Reload();
    const WorldLocation* FindLocation(uint32 id) const;
    bool HasSubmenu(uint32 id) const;
    const std::vector<TeleportData>& GetSubmenuData(uint32 id) const;
    boost::shared_mutex& GetLock() { return m_loadLock; }
};

#define sTeleporterMgr DynamicTeleporterMgr::instance()
