#include "DatabaseEnv.h"
#include "DeffenderVendorMgr.h"
#include "Log.h"
#include "Timer.h"

DeffenderVendorMgr* DeffenderVendorMgr::instance()
{
    static DeffenderVendorMgr instance;
    return &instance;
}

bool DeffenderVendorMgr::IsLoaded()
{
    SharedLock guard(m_loadLock);
    return m_loaded;
}

void DeffenderVendorMgr::Load()
{
    if (m_loaded)
        return;

    uint32 oldMSTime = getMSTime();

    //                                               0         1           2        3      4         5           6          7     8      9
    QueryResult result = WorldDatabase.Query("SELECT id, vendor_entry, parent_id, icon, faction, item_entry, points_type, price, name, count FROM deffender_vendor");
    if (!result)
        return;

    do
    {
        Field *fields = result->Fetch();
        VendorMenuEntry e(fields);
        if (!e.item && fields[5].GetUInt32())
        {
            TC_LOG_ERROR("custom", "Deffender Vendor entry %u has invalid item %u in its list.", fields[1].GetUInt32(), fields[5].GetUInt32());
            continue;
        }

        m_menuData[fields[0].GetUInt16()] = e;
        m_vendorData[fields[1].GetUInt16()][fields[2].IsNull() ? 0 : fields[2].GetUInt16()].push_back(fields[0].GetUInt16());
    } while (result->NextRow());

    for (auto itr = m_menuData.begin(); itr != m_menuData.end();)
    {
        if (itr->second.parentId)
        {
            auto f = m_menuData.find(itr->second.parentId);
            if (f == m_menuData.end())
            {
                TC_LOG_ERROR("custom", "Deffender Vendor entry item %u has invalid parent %u specified.", itr->first, itr->second.parentId);
                itr = m_menuData.erase(itr);
                continue;
            }
        }

        ++itr;
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u Deffender Vendor entries in %u ms", m_menuData.size(), GetMSTimeDiffToNow(oldMSTime));
    m_loaded = true;
}

void DeffenderVendorMgr::Reload()
{
    UniqueLock guard(m_loadLock);
    m_loaded = false;
    m_menuData.clear();
    m_vendorData.clear();
    Load();
}

// this doesn't really need a lock
bool DeffenderVendorMgr::HasData(uint32 entry)
{
    return m_vendorData.find(entry) != m_vendorData.end();
}

DeffenderVendorMgr::ItemsType DeffenderVendorMgr::GetItems(uint32 entry, uint16 parent)
{
    SharedLock guard(m_loadLock);
    auto dataIt = m_vendorData.find(entry);
    if (dataIt == m_vendorData.end())
        return {};

    auto linkIt = dataIt->second.find(parent);
    if (linkIt == dataIt->second.end())
        return {};

    ItemsType ret;

    for (auto entry : linkIt->second)
        ret[entry] = m_menuData[entry];

    return ret;
}

VendorMenuEntry DeffenderVendorMgr::GetData(uint16 id)
{
    SharedLock guard(m_loadLock);
    auto it = m_menuData.find(id);
    if (it == m_menuData.end())
        return {};

    return it->second;
}
