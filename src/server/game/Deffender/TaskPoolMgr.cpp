#include "TaskPoolMgr.h"
#include "World.h"

void TaskPoolMgr::TaskHolder::_Schedule()
{
    _timer->expires_from_now(boost::posix_time::seconds(_frequency));
    _timer->async_wait(std::bind(&TaskPoolMgr::TaskHolder::Run, this, std::placeholders::_1));
}

void TaskPoolMgr::TaskHolder::Run(boost::system::error_code const& error)
{
    if (!World::IsStopped() && !error)
    {
        _callback();
        _Schedule();
    }
}

TaskPoolMgr* TaskPoolMgr::instance()
{
    static TaskPoolMgr instance;
    return &instance;
}

void TaskPoolMgr::Borrow(boost::asio::io_service* ioService)
{
    _ioService = ioService;
}

void TaskPoolMgr::Return()
{
    _ioService = nullptr;
}
