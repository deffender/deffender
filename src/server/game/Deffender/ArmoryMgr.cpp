#include "ArmoryMgr.h"
#include "Config.h"
#include "DatabaseEnv.h"
#include "GameTime.h"
#include "Log.h"
#include "Map.h"
#include "Player.h"

ArmoryMgr* ArmoryMgr::instance()
{
    static ArmoryMgr instance;
    return &instance;
}

void ArmoryMgr::Reload()
{
    _enabled = sConfigMgr->GetBoolDefault("Armory.Enable", false);
}

void ArmoryMgr::CreateFeed(ArmoryFeedType type, Player* player, uint32 data, uint32 item_guid, uint32 item_quality) const
{
    if (data == 0)
    {
        TC_LOG_ERROR("armory", "ArmoryMgr::CreateFeed: empty data (GUID: %u), ignore.", player->GetGUID().GetCounter());
        return;
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_ARMORY_FEED);

    stmt->setUInt32(0, player->GetGUID().GetCounter());
    stmt->setUInt32(1, uint32(type));
    stmt->setUInt32(2, data);
    stmt->setUInt32(3, GameTime::GetGameTime());
    stmt->setUInt32(4, 0);
    stmt->setUInt32(5, type == ARMORY_FEED_BOSS ? player->GetMap()->GetDifficulty() : 0);
    stmt->setUInt32(6, item_guid);
    stmt->setUInt32(7, item_quality);

    CharacterDatabase.Execute(stmt);
}

void ArmoryMgr::UpdateStats(Player* player)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_ARMORY_STATS);

    std::ostringstream ss;
    for (uint16 i = 0; i < player->GetValuesCount(); ++i)
        ss << player->GetUInt32Value(i) << " ";

    stmt->setUInt32(0, player->GetGUID().GetCounter());
    stmt->setString(1, ss.str());
    stmt->setUInt32(2, GameTime::GetGameTime());

    CharacterDatabase.Execute(stmt);
}
