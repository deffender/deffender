#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <map>
#include <vector>
#include "AccountPointsMgr.h"
#include "Field.h"
#include "Item.h"
#include "ObjectMgr.h"
#include "GossipDef.h"
#include "Position.h"

struct VendorMenuEntry
{
    uint16 parentId;
    uint32 icon;
    uint32 faction;
    std::string name;
    ItemTemplate const* item;
    AccountPointsType pointsType;
    uint32 price;
    uint16 count;

    VendorMenuEntry() : pointsType(ACCOUNT_POINTS_MAX) {}
    VendorMenuEntry(Field* fields) :
        parentId(fields[2].GetUInt16()),
        icon(fields[3].GetUInt8()),
        faction(fields[4].GetUInt32()),
        name(fields[8].GetString()),
        item(sObjectMgr->GetItemTemplate(fields[5].GetUInt32())),
        pointsType(AccountPointsType(fields[6].GetUInt8())),
        price(fields[7].GetUInt32()),
        count(fields[9].GetUInt16()) {}
};

class TC_GAME_API DeffenderVendorMgr
{
    using SharedLock = boost::shared_lock<boost::shared_mutex>;
    using UniqueLock = boost::unique_lock<boost::shared_mutex>;

    boost::shared_mutex m_loadLock;
    std::map<uint32, std::map<uint16, std::vector<uint16>>> m_vendorData;
    std::map<uint16, VendorMenuEntry> m_menuData;

    bool m_loaded;
    DeffenderVendorMgr() : m_loaded(false) {}

public:
    using ItemsType = std::map<uint16, VendorMenuEntry>;

    static DeffenderVendorMgr* instance();

    bool IsLoaded();
    void Load();
    void Reload();
    bool HasData(uint32 entry);
    ItemsType GetItems(uint32 entry, uint16 parent = 0);
    VendorMenuEntry GetData(uint16 id);
};

#define sDeffVendorMgr DeffenderVendorMgr::instance()
