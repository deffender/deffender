#ifndef ACCOUNT_POINTS_MGR_H
#define ACCOUNT_POINTS_MGR_H

#include "Common.h"
#include "LoginDatabase.h"

#define MIN_POINTS_UPDATE_DELAY 3
#define POINTS_UPDATE_DELAY 30

enum AccountPointsType
{
    ACCOUNT_POINTS_DONATE,
    ACCOUNT_POINTS_VOTE,
    ACCOUNT_POINTS_MAX
};

enum VotePointsCosts
{
    VOTECOST_INSTANCE_RESET = 25,
};

struct AccountPointsInfo
{
    uint32 points;
    time_t lastChanged;

    AccountPointsInfo() : points(0), lastChanged(0) {}
};

LoginDatabaseStatements const AccountPointsQueries[ACCOUNT_POINTS_MAX][3] = {
    { LOGIN_SEL_ALL_DONATEPOINTS_SINCE, LOGIN_UPD_ACCOUNT_DONATEPOINTS, LOGIN_INS_ACCOUNT_DONATEPOINTS },
    { LOGIN_SEL_ALL_VOTEPOINTS_SINCE,   LOGIN_UPD_ACCOUNT_VOTEPOINTS,   LOGIN_INS_ACCOUNT_VOTEPOINTS },
};

std::string const AccountPointsNames[ACCOUNT_POINTS_MAX] = {
    "Deffender Coin",
    "Vote point"
};

typedef std::map<uint32, AccountPointsInfo> AccountPointsMap;

class TC_GAME_API AccountPointsMgr
{
    std::mutex m_lock;
    std::array<AccountPointsMap, ACCOUNT_POINTS_MAX> m_points;
    std::array<time_t, ACCOUNT_POINTS_MAX> m_lastUpdate;

    AccountPointsMgr() : m_lastUpdate({0, 0}) {}

public:
    static AccountPointsMgr* instance();

    void Load();
    void Update();
    void UpdateType(AccountPointsType type, bool force = false, bool load = false);
    bool GetPoints(AccountPointsType type, uint32 account_id, AccountPointsInfo& result);
    void SetPoints(AccountPointsType type, uint32 account_id, uint32 points);
};

#define sAccountPointsMgr AccountPointsMgr::instance()
#endif
