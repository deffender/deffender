#include "Common.h"
#include "World.h"

class Player;

enum ArmoryFeedType
{
    ARMORY_FEED_ACHIEVEMENT = 1,
    ARMORY_FEED_ITEM,
    ARMORY_FEED_BOSS,
    ARMORY_FEED_MAX
};

class TC_GAME_API ArmoryMgr
{
    bool _enabled;

    ArmoryMgr() : _enabled(false) {}

public:
    static ArmoryMgr* instance();

    bool IsEnabled() const { return _enabled; }
    void Reload();
    void CreateFeed(ArmoryFeedType type, Player* player, uint32 data, uint32 item_guid = 0, uint32 item_quality = 0) const;
    void UpdateStats(Player* player);
};

#define sArmoryMgr ArmoryMgr::instance()
