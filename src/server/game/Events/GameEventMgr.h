/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_GAMEEVENT_MGR_H
#define TRINITY_GAMEEVENT_MGR_H

#include "Common.h"
#include "ObjectGuid.h"
#include "SharedDefines.h"
#include "Define.h"
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>

#define max_ge_check_delay DAY  // 1 day in seconds

enum GameEventState
{
    GAMEEVENT_NORMAL           = 0, // standard game events
    GAMEEVENT_WORLD_INACTIVE   = 1, // not yet started
    GAMEEVENT_WORLD_CONDITIONS = 2, // condition matching phase
    GAMEEVENT_WORLD_NEXTPHASE  = 3, // conditions are met, now 'length' timer to start next event
    GAMEEVENT_WORLD_FINISHED   = 4, // next events are started, unapply this one
    GAMEEVENT_INTERNAL         = 5, // never handled in update
};

struct GameEventFinishCondition
{
    float reqNum;  // required number // use float, since some events use percent
    float done;    // done number
    uint32 max_world_state;  // max resource count world state update id
    uint32 done_world_state; // done resource count world state update id
};

struct GameEventQuestToEventConditionNum
{
    uint16 event_id;
    uint32 condition;
    float num;
};

typedef std::map<uint32 /*condition id*/, GameEventFinishCondition> GameEventConditionMap;

struct GameEventData
{
    GameEventData() : start(1), end(0), nextstart(0), occurence(0), length(0), holiday_id(HOLIDAY_NONE), state(GAMEEVENT_NORMAL),
                      announce(0) { }
    time_t start;           // occurs after this time
    time_t end;             // occurs before this time
    time_t nextstart;       // after this time the follow-up events count this phase completed
    uint32 occurence;       // time between end and start
    uint32 length;          // length of the event (minutes) after finishing all conditions
    HolidayIds holiday_id;
    uint8 holidayStage;
    GameEventState state;   // state of the game event, these are saved into the game_event table on change!
    GameEventConditionMap conditions;  // conditions to finish
    std::set<int32 /*gameevent id*/> prerequisite_events;  // events that must be completed before starting this event
    std::string description;
    uint8 announce;         // if 0 dont announce, if 1 announce, if 2 take config value

    bool isValid() const { return length > 0 || state > GAMEEVENT_NORMAL; }
};

struct ModelEquip
{
    uint32 modelid;
    uint32 modelid_prev;
    uint8 equipment_id;
    uint8 equipement_id_prev;
};

struct NPCVendorEntry
{
    uint32 entry;                                           // creature entry
    uint32 item;                                            // item id
    int32  maxcount;                                        // 0 for infinite
    uint32 incrtime;                                        // time for restore items amount if maxcount != 0
    uint32 ExtendedCost;
};

class Player;
class Creature;
class Quest;

class TC_GAME_API GameEventMgr
{
    private:
        GameEventMgr();
        ~GameEventMgr() { }

    public:
        static GameEventMgr* instance();

        typedef std::set<int32> ActiveEvents;
        typedef std::map<int32, GameEventData> GameEventDataMap;
        ActiveEvents const& GetActiveEventList() const { return m_ActiveEvents; }
        GameEventDataMap& GetEventMap() const { return mGameEvent; }
        bool CheckOneGameEvent(int32 entry) const;
        uint32 NextCheck(int32 entry) const;
        void LoadFromDB();
        void LoadHolidayDates();
        uint32 Update();
        bool IsActiveEvent(int32 event_id) { return (m_ActiveEvents.find(event_id) != m_ActiveEvents.end()); }
        uint32 StartSystem();
        void Initialize();
        void StartArenaSeason();
        void StartInternalEvent(int32 event_id);
        bool StartEvent(int32 event_id, bool overwrite = false);
        void StopEvent(int32 event_id, bool overwrite = false);
        void HandleQuestComplete(uint32 quest_id);  // called on world event type quest completions
        void HandleWorldEventGossip(Player* player, Creature* c);
        uint32 GetNPCFlag(Creature* cr);
        uint32 GetNpcTextId(uint32 guid);
        void AddCreatureToEvent(uint32 guid, int32 eventId, Player* plr);
        void AddCreatureToEvent(uint32 guid, int32 eventId);
        void AddGameObjectToEvent(uint32 guid, int32 eventId, Player* plr);
        void AddGameObjectToEvent(uint32 guid, int32 eventId);

        bool ExportEvent(int32 eventId);
        bool ImportEvent(std::string path);
    private:
        void CreateNewGameEvent(int32 eventId, Player* plr);
        void SendWorldStateUpdate(Player* player, int32 event_id);
        void AddActiveEvent(int32 event_id) { m_ActiveEvents.insert(event_id); }
        void RemoveActiveEvent(int32 event_id) { m_ActiveEvents.erase(event_id); }
        void ApplyNewEvent(int32 event_id);
        void UnApplyEvent(int32 event_id);
        void GameEventSpawn(int32 event_id);
        void GameEventUnspawn(int32 event_id);
        void ChangeEquipOrModel(int32 event_id, bool activate);
        void UpdateEventQuests(int32 event_id, bool activate);
        void UpdateWorldStates(int32 event_id, bool Activate);
        void UpdateEventNPCFlags(int32 event_id);
        void UpdateEventNPCVendor(int32 event_id, bool activate);
        void UpdateBattlegroundSettings();
        void RunSmartAIScripts(int32 event_id, bool activate);    //! Runs SMART_EVENT_GAME_EVENT_START/_END SAI
        bool CheckOneGameEventConditions(int32 event_id);
        void SaveWorldEventStateToDB(int32 event_id);
        bool hasCreatureQuestActiveEventExcept(uint32 quest_id, int32 event_id);
        bool hasGameObjectQuestActiveEventExcept(uint32 quest_id, int32 event_id);
        bool hasCreatureActiveEventExcept(ObjectGuid::LowType creature_guid, int32 event_id);
        bool hasGameObjectActiveEventExcept(ObjectGuid::LowType go_guid, int32 event_id);
        void SetHolidayEventTime(GameEventData& event);

        typedef std::list<ObjectGuid::LowType> GuidList;
        typedef std::list<uint32> IdList;
        typedef std::map<int32 ,GuidList> GameEventGuidMap;
        typedef std::map<int32, IdList> GameEventIdMap;
        typedef std::pair<ObjectGuid::LowType, ModelEquip> ModelEquipPair;
        typedef std::list<ModelEquipPair> ModelEquipList;
        typedef std::map<int32 ,ModelEquipList> GameEventModelEquipMap;
        typedef std::pair<uint32, uint32> QuestRelation;
        typedef std::list<QuestRelation> QuestRelList;
        typedef std::map<int32,QuestRelList> GameEventQuestMap;
        typedef std::list<NPCVendorEntry> NPCVendorList;
        typedef std::map<int32,NPCVendorList> GameEventNPCVendorMap;
        typedef std::map<uint32 /*quest id*/, GameEventQuestToEventConditionNum> QuestIdToEventConditionMap;
        typedef std::pair<ObjectGuid::LowType /*guid*/, uint32 /*npcflag*/> GuidNPCFlagPair;
        typedef std::list<GuidNPCFlagPair> NPCFlagList;
        typedef std::map<int32,NPCFlagList> GameEventNPCFlagMap;
        typedef std::map<int32,uint32> GameEventBitmask;
        GameEventQuestMap mGameEventCreatureQuests;
        GameEventQuestMap mGameEventGameObjectQuests;
        GameEventNPCVendorMap mGameEventVendors;
        GameEventModelEquipMap mGameEventModelEquip;
        GameEventIdMap    mGameEventPoolIds;
        mutable GameEventDataMap  mGameEvent;
        GameEventBitmask  mGameEventBattlegroundHolidays;
        QuestIdToEventConditionMap mQuestToEventConditions;
        GameEventNPCFlagMap mGameEventNPCFlags;
        ActiveEvents m_ActiveEvents;
        bool isSystemInit;
    public:
        GameEventGuidMap  mGameEventCreatureGuids;
        GameEventGuidMap  mGameEventGameobjectGuids;
        std::set<uint32> modifiedHolidays;
};

#define sGameEventMgr GameEventMgr::instance()

TC_GAME_API bool IsHolidayActive(HolidayIds id);
TC_GAME_API bool IsEventActive(int32 event_id);

#endif

