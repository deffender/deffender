#include <boost/filesystem.hpp>
#include <fstream>
#include "Config.h"
#include "Creature.h"
#include "DatabaseEnv.h"
#include "GameEventDump.h"
#include "GameEventMgr.h"
#include "GameObject.h"
#include "MapManager.h"
#include "ObjectMgr.h"

/*
 * MISSING THINGS:
 * _______________
 * conditions
 * prerequisite events
 * pools
 * waypoints
 * quests
 * new custom quests
 * new custom creatures
 * new custom Equipment
 * new custom gameobjects
 */

namespace
{
    template<typename T>
    void wr(std::ostream& out, T value)
    {
        out.write(reinterpret_cast<char*>(&value), sizeof(T));
    }

    template<typename T>
    void rd(std::istream& in, T& val)
    {
        in.read(reinterpret_cast<char*>(&val), sizeof(T));
    }

    template<typename T>
    auto rdRet(std::istream& in) -> T
    {
        T val;
        in.read(reinterpret_cast<char*>(&val), sizeof(T));
        return val;
    }
}

bool GameEventDump::Export(int32 eventId)
{
    GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();
    if (events.find(eventId) == events.end())
        return false;
    std::string exportPath = sConfigMgr->GetStringDefault("EventExportDir", "./");
    boost::filesystem::path dir(exportPath);
    if (!(boost::filesystem::exists(dir)))
    {
        if (!boost::filesystem::create_directory(dir))
            return false; // unable to save to specified directory
    }
    // path is not directory, but file
    else if (!boost::filesystem::is_directory(dir))
        return false;

    std::string fileName = exportPath + "event_" + std::to_string(eventId);
    std::ofstream file;
    file.open(fileName, std::ios::out | std::ios::binary);

    // Info about event
    {
        GameEventData& eventData = events[eventId];
        wr(file, eventId);
        wr(file, eventData.announce);
        wr(file, eventData.holiday_id);
        wr(file, eventData.length);
        wr(file, eventData.nextstart);
        wr(file, eventData.occurence);
        wr(file, eventData.start);
        wr(file, eventData.state);
        wr(file, static_cast<uint32>(eventData.description.size()));
        file.write(eventData.description.c_str(), eventData.description.size());
        wr(file, sGameEventMgr->IsActiveEvent(eventId));
    }

    // copy all gameobjects
    {
        uint32 objectCount = 0;
        for (auto itr :sGameEventMgr->mGameEventGameobjectGuids[eventId])
            if (itr > 0 && sObjectMgr->GetGameObjectData(itr)) // filter invalid guids
                ++objectCount;

        wr(file, objectCount);
        for (auto itr :sGameEventMgr->mGameEventGameobjectGuids[eventId])
        {
            if (itr > 0)
            if (GameObjectData* data = const_cast<GameObjectData*>(sObjectMgr->GetGameObjectData(itr)))
            {
                wr(file, data->animprogress);
                wr(file, data->artKit);
                wr(file, data->dbData);
                wr(file, data->goState);
                wr(file, data->id);
                wr(file, data->spawnPoint.m_mapId);
                wr(file, data->spawnPoint.GetOrientation());
                wr(file, data->phaseMask);
                wr(file, data->spawnPoint.m_positionX);
                wr(file, data->spawnPoint.m_positionY);
                wr(file, data->spawnPoint.m_positionZ);
                wr(file, data->rotation.x);
                wr(file, data->rotation.y);
                wr(file, data->rotation.z);
                wr(file, data->rotation.w);
                wr(file, data->spawnMask);
                wr(file, data->spawntimesecs);
            }
        }
    }

    // copy all creatures
    {
        uint32 creatureCount = 0;
        for (auto itr :sGameEventMgr->mGameEventCreatureGuids[eventId])
            if (sObjectMgr->GetCreatureData(itr)) // filter invalid guids
                ++creatureCount;

        wr(file, creatureCount);
        for (auto itr :sGameEventMgr->mGameEventCreatureGuids[eventId])
        {
            if (itr > 0)
            if (CreatureData* data = const_cast<CreatureData*>(sObjectMgr->GetCreatureData(itr)))
            {
                wr(file, data->curhealth);
                wr(file, data->curmana);
                wr(file, data->currentwaypoint);
                wr(file, data->dbData);
                wr(file, data->displayid);
                wr(file, data->dynamicflags);
                wr(file, data->equipmentId);
                wr(file, data->id);
                wr(file, data->spawnPoint.m_mapId);
                wr(file, data->movementType);
                wr(file, data->npcflag);
                wr(file, data->spawnPoint.GetOrientation());
                wr(file, data->phaseMask);
                wr(file, data->spawnPoint.m_positionX);
                wr(file, data->spawnPoint.m_positionY);
                wr(file, data->spawnPoint.m_positionZ);
                wr(file, data->spawndist);
                wr(file, data->spawnMask);
                wr(file, data->spawntimesecs);
                wr(file, data->unit_flags);
            }
        }
    }

    // @TODO: create new waypoints if needed
    // @TODO: missing quests

    return true;
}

bool GameEventDump::Import(std::string path)
{
    std::ifstream file(path, std::ios::in | std::ios::binary);
    if (!file.is_open())
        return false;

    file.seekg(0);
    int32 eventId;
    uint32 count;
    rd(file, eventId);

    GameEventMgr::GameEventDataMap& events = sGameEventMgr->GetEventMap();
    if (events.find(eventId) != events.end())
    {
        file.close();
        return false;
    }

    GameEventData& eventData = events[eventId];
    rd(file, eventData.announce);
    rd(file, eventData.holiday_id);
    rd(file, eventData.length);
    rd(file, eventData.nextstart);
    rd(file, eventData.occurence);
    rd(file, eventData.start);
    rd(file, eventData.state);
    rd(file, count);
    char* descr = new char[count + 1];
    descr[count] = '\0';
    file.read(descr, count);
    eventData.description = descr;
    eventData.description.resize(count);
    if (rdRet<bool>(file))
        sGameEventMgr->StartEvent(eventId);

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_GAME_EVENT);
    stmt->setInt32(0, eventId);
    stmt->setString(1, descr);
    WorldDatabase.Execute(stmt);

    delete descr;

    rd(file, count);
    for (uint32 i = 0; i < count; ++i)
        ImportGO(file, eventId);

    rd(file, count);
    for (uint32 i = 0; i < count; ++i)
        ImportNPC(file, eventId);

    return true;
}

void GameEventDump::ImportGO(std::ifstream& file, int32 eventId)
{
    GameObjectData data;
    rd(file, data.animprogress);
    rd(file, data.artKit);
    rd(file, data.dbData);
    rd(file, data.goState);
    rd(file, data.id);
    rd(file, data.spawnPoint.m_mapId);
    data.spawnPoint.SetOrientation(rdRet<float>(file));
    rd(file, data.phaseMask);
    rd(file, data.spawnPoint.m_positionX);
    rd(file, data.spawnPoint.m_positionY);
    rd(file, data.spawnPoint.m_positionZ);
    rd(file, data.rotation.x);
    rd(file, data.rotation.y);
    rd(file, data.rotation.z);
    rd(file, data.rotation.w);
    rd(file, data.spawnMask);
    rd(file, data.spawntimesecs);

    Map* map = sMapMgr->FindBaseNonInstanceMap(data.spawnPoint.m_mapId);

    GameObject* object = new GameObject;
    uint32 guidLow = map->GenerateLowGuid<HighGuid::GameObject>();

    if (!object->Create(guidLow, data.id, map, data.phaseMask, data.spawnPoint, data.rotation, data.animprogress, data.goState, data.artKit))
    {
        delete object;
        return;
    }

    object->SetRespawnTime(data.spawntimesecs);

    // fill the gameobject data and save to the db
    object->SaveToDB(map->GetId(), (1 << map->GetSpawnMode()), data.phaseMask);

    uint32 spawnId = object->GetSpawnId();

    if (eventId)
    {
        PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_GAME_EVENT_GAMEOBJECT);
        stmt->setInt32(0, eventId);
        stmt->setUInt32(1, object->GetSpawnId());
        WorldDatabase.Execute(stmt);
        sGameEventMgr->AddGameObjectToEvent(object->GetSpawnId(), eventId);
    }

    // delete the old object and do a clean load from DB with a fresh new GameObject instance.
    // this is required to avoid weird behavior and memory leaks
    delete object;

    bool addToMap = !eventId || sGameEventMgr->IsActiveEvent(eventId);
    object = new GameObject();
    // this will generate a new guid if the object is in an instance
    if (!object->LoadFromDB(spawnId, map, addToMap))
    {
        delete object;
        return;
    }

    if (addToMap) // add only in case event is in progress
        sObjectMgr->AddGameobjectToGrid(guidLow, sObjectMgr->GetGameObjectData(spawnId));
}

void GameEventDump::ImportNPC(std::ifstream& file, int32 eventId)
{
    CreatureData data;
    rd(file, data.curhealth);
    rd(file, data.curmana);
    rd(file, data.currentwaypoint);
    rd(file, data.dbData);
    rd(file, data.displayid);
    rd(file, data.dynamicflags);
    rd(file, data.equipmentId);
    rd(file, data.id);
    rd(file, data.spawnPoint.m_mapId);
    rd(file, data.movementType);
    rd(file, data.npcflag);
    data.spawnPoint.SetOrientation(rdRet<float>(file));
    rd(file, data.phaseMask);
    rd(file, data.spawnPoint.m_positionX);
    rd(file, data.spawnPoint.m_positionY);
    rd(file, data.spawnPoint.m_positionZ);
    rd(file, data.spawndist);
    rd(file, data.spawnMask);
    rd(file, data.spawntimesecs);
    rd(file, data.unit_flags);

    if (!sObjectMgr->GetCreatureTemplate(data.id))
        return;

    Map* map = sMapMgr->FindBaseNonInstanceMap(data.spawnPoint.m_mapId);
    if (!map)
    {
        TC_LOG_ERROR("events.dump", "ImportGO: Failed to get map ID %u", data.spawnPoint.m_mapId);
        return;
    }

    Creature* creature = new Creature();
    uint32 guidLow = map->GenerateLowGuid<HighGuid::Unit>();
    if (!creature->Create(guidLow, map, data.phaseMask, data.id, data.spawnPoint, &data))
    {
        delete creature;
        return;
    }

    creature->SetRespawnDelay(data.spawntimesecs);
    creature->SetRespawnRadius(data.spawndist);
    creature->SetUInt32Value(UNIT_FIELD_FLAGS, data.unit_flags);
    creature->SetUInt32Value(UNIT_DYNAMIC_FLAGS, data.dynamicflags);
    creature->SetHealth(data.curhealth);
    creature->SetPower(POWER_MANA, data.curmana);
    creature->SetDisplayId(data.displayid);
    creature->SetNativeDisplayId(data.displayid);
    creature->SetDefaultMovementType(MovementGeneratorType(data.movementType));
    creature->SetCurrentEquipmentId(data.equipmentId);

    creature->SaveToDB(map->GetId(), (1 << map->GetSpawnMode()), data.phaseMask);

    uint32 db_guid = creature->GetSpawnId();

    if (eventId)
    {
        PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_GAME_EVENT_CREATURE);
        stmt->setInt32(0, eventId);
        stmt->setUInt32(1, db_guid);
        WorldDatabase.Execute(stmt);
        sGameEventMgr->AddCreatureToEvent(db_guid, eventId);
    }

    // To call _LoadGoods(); _LoadQuests(); CreateTrainerSpells()
    // current "creature" variable is deleted and created fresh new, otherwise old values might trigger asserts or cause undefined behavior
    creature->CleanupsBeforeDelete();
    delete creature;

    bool addToMap = sGameEventMgr->IsActiveEvent(eventId);
    creature = new Creature();
    if (!creature->LoadFromDB(db_guid, map, addToMap, false))
    {
        delete creature;
        return;
    }

    if (addToMap) // add only in case if event is in progress
        sObjectMgr->AddCreatureToGrid(guidLow, sObjectMgr->GetCreatureData(db_guid));
}
