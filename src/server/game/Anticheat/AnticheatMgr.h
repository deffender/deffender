/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SC_ACMGR_H
#define SC_ACMGR_H

//#include <ace/Singleton.h>
#include "AnticheatData.h"

class Player;

// GUIDLow is the key.
typedef std::map<uint32, AnticheatData> AnticheatPlayersDataMap;

class TC_GAME_API AnticheatMgr
{
//    friend class ACE_Singleton<AnticheatMgr, ACE_Null_Mutex>;
    AnticheatMgr();
    ~AnticheatMgr();

    public:
        static AnticheatMgr* instance();
        void StartHackDetection(Player* player, MovementInfo movementInfo, uint32 opcode);
        void SavePlayerData(Player* player);

        void StartScripts();

        void HandlePlayerLogin(Player* player);
        void HandlePlayerLogout(Player* player);

        uint32 GetTotalReports(uint32 lowGUID);
        float GetAverage(uint32 lowGUID);
        uint32 GetTypeReports(uint32 lowGUID, uint8 type);

        void AnticheatGlobalCommand(ChatHandler* handler);
        void AnticheatDeleteCommand(uint32 guid);

        void SavePQRData(Player* player);
        void SaveInterruptHackLogs(Player* caster, SQLTransaction& trans);
        void SaveDispellHackLogs(Player* caster, SQLTransaction& trans);
        void InterruptHackDetection(Player* caster, Spell* spell);
        void DispellHackDetection(Player* caster, Aura* aura);

        void ResetDailyReportStates();

        void UpdateSpeedHistory(Player* player, float speed);
    private:
        void AirWalkHackDetection(Player* player, MovementInfo movementInfo, bool isFlying);
        void SpeedHackDetection(Player* player, MovementInfo movementInfo);
        void FlyHackDetection(Player* player, MovementInfo movementInfo);
        void WalkOnWaterHackDetection(Player* player, MovementInfo movementInfo);
        void JumpHackDetection(Player* player, uint32 opcode);
        void TeleportPlaneHackDetection(Player* player, MovementInfo);
        void ClimbHackDetection(Player* player, MovementInfo movementInfo);

        std::string GetReportName(uint8 reportType);
        bool CanBeAutomaticallyBanned(uint8 reportType);
        void DebugLog(Player* player, uint8 reportType);
        void BuildReport(Player* player,uint8 reportType);

        bool IsValidOpCode(uint32 opcode);

        bool MustCheckTempReports(uint8 type);

        AnticheatPlayersDataMap m_Players;                        ///< Player data
};

#define sAnticheatMgr AnticheatMgr::instance()

#endif
