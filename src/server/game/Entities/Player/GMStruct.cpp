#include "GMStruct.h"
#include "Util.h"
#include "DatabaseEnv.h"
#include "World.h"

void GMStruct::LoadEventIdList(EventIdList* list, Field& field)
{
    Tokenizer eventIdList(field.GetString(), ';');
    for (Tokenizer::const_iterator itr = eventIdList.begin(); itr != eventIdList.end(); ++itr)
    {
        Tokenizer pairs(*itr, ',');
        const char* cmin = nullptr;
        const char* cmax = nullptr;
        Tokenizer::const_iterator iter = pairs.begin();
        if (iter != pairs.end())
            cmin = *iter;
        if (++iter != pairs.end())
            cmax = *iter;
        if (!cmax)
            cmax = cmin;
        if (cmin && cmax)
        {
            int32 min = int32(atoi(cmin));
            int32 max = int32(atoi(cmax));
            if (min > max)
                std::swap(min,max);
            std::pair<int32, int32> range(min,max);
            list->push_back(range);
        }
    }
}

GMStruct::GMStruct(uint32 accountId)
{
    eventId = 0;
    realmId = 0;
    canSpawnObject = false;
    this->accountId = accountId;
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GM_STRUCT);
    stmt->setUInt32(0, accountId);
    PreparedQueryResult result = CharacterDatabase.Query(stmt);
    if (!result)
        return;

    Field* fields = result->Fetch();
    int32 event_id = fields[0].GetInt32();
    LoadEventIdList(&usableEventIdList,fields[1]);
    if (fields[2].GetBool())
        SetEventId(event_id);
    else
        SetEventId(eventId);
    LoadEventIdList(&startableEventIdList,fields[3]);
    announceRank = fields[4].GetString();
}

GMStruct::~GMStruct()
{
    usableEventIdList.clear();
    startableEventIdList.clear();
}

bool GMStruct::CanUseEventId(int32 id)
{
    for(auto itr : usableEventIdList)
        if (itr.first <= id && itr.second >= id)
            return true;
    return false;
}

bool GMStruct::CanStartEventId(int32 id)
{
    for(auto itr : startableEventIdList)
        if (itr.first <= id && itr.second >= id)
            return true;
    return false;
}

bool GMStruct::SetEventId(int32 id)
{
    if (CanUseEventId(id))
    {
        eventId = id;
        canSpawnObject = true;
        return true;
    }
    return false;
}

std::string GMStruct::GetUsableEventIdRange()
{
    std::string ret;
    if (!usableEventIdList.empty())
    {
        ret = "Usable eventIds:";
        for (auto itr : usableEventIdList)
        {
            ret += "\n";
            ret += std::to_string(itr.first);
            ret += " - ";
            ret += std::to_string(itr.second);
        }
    }
    else
        ret = "You do not have any usable eventIds.";
    return ret;
}

// save dynamic parts of struct only, others are handled manually via DB
void GMStruct::SaveToDB(SQLTransaction &trans)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GM_STRUCT);
    stmt->setInt32(0, eventId);
    stmt->setUInt32(1, accountId);
    trans->Append(stmt);
}
