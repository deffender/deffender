#ifndef __GMSTRUCT
#define __GMSTRUCT

#include "GameEventMgr.h"
#include "Field.h"
#include <memory>

class Transaction;
typedef std::shared_ptr<Transaction> SQLTransaction;
typedef std::list<std::pair<int32, int32>> EventIdList;
struct TC_GAME_API GMStruct
{
public:
    GMStruct(uint32 accountId);
    ~GMStruct();
    int32 GetEventId() { return eventId; }
    bool CanUseEventId(int32 id);
    bool CanStartEventId(int32 id);
    bool CanSpawnObject() { return canSpawnObject; }
    bool SetEventId(int32 id);
    const std::string GetAnnounceRank() { return announceRank; }
    void SaveToDB(SQLTransaction& trans);
    std::string GetUsableEventIdRange();
protected:
    void LoadEventIdList(EventIdList* list, Field& field);
private:
    EventIdList usableEventIdList;
    EventIdList startableEventIdList;
    int32 eventId;
    bool canSpawnObject;
    uint32 accountId;
    uint32 realmId;
    std::string announceRank;
};

#endif
