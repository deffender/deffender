#include "MovementInfo.h"
#include "UnitDefines.h"

void MovementInfo::TransportInfo::Reset()
{
    guid.Clear();
    pos.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
    seat = -1;
    time = 0;
    time2 = 0;
}

MovementInfo::MovementInfo() :
    guid(), flags(0), flags2(0), time(0), pitch(0.0f), fallTime(0), splineElevation(0.0f)
{
    transport.Reset();
    jump.Reset();
}

void MovementInfo::OutDebug(std::ostream& out)
{
    out << "MOVEMENT INFO" << std::endl;
    out << guid.ToString().c_str() << std::endl;
    out << "flags " << flags << std::endl;
    out << "flags2 " << flags2 << std::endl;
    out << "time " << flags2 << " current time " << uint64(::time(nullptr)) << std::endl;
    out << "position: `" << pos.ToString().c_str() << "`" << std::endl;
    if (flags & MOVEMENTFLAG_ONTRANSPORT)
    {
        out << "TRANSPORT:" << std::endl;
        out << transport.guid.ToString().c_str() << std::endl;
        out << "position: `" << transport.pos.ToString().c_str() << "`" << std::endl;
        out << "seat: " << transport.seat << std::endl;
        out << "time: " << transport.time << std::endl;
        if (flags2 & MOVEMENTFLAG2_INTERPOLATED_MOVEMENT)
            out << "time2: " << transport.time2 << std::endl;
    }

    if ((flags & (MOVEMENTFLAG_SWIMMING | MOVEMENTFLAG_FLYING)) || (flags2 & MOVEMENTFLAG2_ALWAYS_ALLOW_PITCHING))
        out << "pitch: " << pitch << std::endl;

    out << "fallTime: " << fallTime << std::endl;
    if (flags & MOVEMENTFLAG_FALLING)
        out << "j_zspeed: " << jump.zspeed << " j_sinAngle: " << jump.sinAngle << " j_cosAngle: " << jump.cosAngle << " j_xyspeed: " << jump.xyspeed << std::endl;

    if (flags & MOVEMENTFLAG_SPLINE_ELEVATION)
        out << "splineElevation: " << splineElevation << std::endl;
}
