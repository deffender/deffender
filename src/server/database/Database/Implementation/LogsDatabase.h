/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LOGSDATABASE_H
#define _LOGSDATABASE_H

#include "MySQLConnection.h"

enum LogsDatabaseStatements : uint32
{
    /*  Naming standard for defines:
        {DB}_{SEL/INS/UPD/DEL/REP}_{Summary of data changed}
        When updating more than one field, consider looking at the calling function
        name for a suiting suffix.
    */

    LOGS_INS_ITEM_GET,
    LOGS_INS_GM_COMMANDS,
    LOGS_INS_ARENA_TEAM,
    LOGS_INS_ARENA_PLAYER,
    LOGS_SEL_ARENA_ID,
    LOGS_INS_LOGIN,
    LOGS_INS_INTERRUPT,
    LOGS_INS_DISPELL,
    LOGS_INS_BOSS_KILLS,
    LOGS_INS_BOSS_KILL_PLAYERS,
    LOGS_SEL_BOSS_KILL_ID,
    MAX_LOGSDATABASE_STATEMENTS
};

class TC_DATABASE_API LogsDatabaseConnection : public MySQLConnection
{
public:
    typedef LogsDatabaseStatements Statements;

    //- Constructors for sync and async connections
    LogsDatabaseConnection(MySQLConnectionInfo& connInfo);
    LogsDatabaseConnection(ProducerConsumerQueue<SQLOperation*>* q, MySQLConnectionInfo& connInfo);
    ~LogsDatabaseConnection();

    //- Loads database type specific prepared statements
    void DoPrepareStatements() override;
};

#endif
