#include "AccountMgr.h"
#include "Chat.h"
#include "Language.h"
#include "Log.h"
#include "Player.h"
#include "Pet.h"
#include "Group.h"
#include "ScriptMgr.h"
#include "GMStruct.h"
#include "ObjectAccessor.h"
#include "Map.h"
#include "World.h"
#include "ObjectMgr.h"
#include "WorldSession.h"
#include "SpellAuras.h"
#include "VMapFactory.h"


class em_commandscript : public CommandScript
{
    template<typename... Args>
    static void NotifyModification(ChatHandler* handler, Unit* target, TrinityStrings resourceMessage, TrinityStrings resourceReportMessage, Args&&... args)
    {
        if (Player* player = target->ToPlayer())
        {
            handler->PSendSysMessage(resourceMessage, handler->GetNameLink(player).c_str(), args...);
            if (handler->needReportToTarget(player))
                ChatHandler(player->GetSession()).PSendSysMessage(resourceReportMessage, handler->GetNameLink().c_str(), std::forward<Args>(args)...);
        }
    }

    static uint8 GetDuelType(Player *initiator, Player *opponent) {
        playerSpec initiatorSpec = initiator->GetSpec();
        playerSpec opponentSpec = opponent->GetSpec();


        uint8 flags = 1;
        uint8 nonDisc = 0;
        uint8 anyHeals = 0;
        switch (initiatorSpec)
        {
        case PALADIN_HOLY:
        case SHAMAN_RESTO:
        case DRUID_RESTO:
        case PRIEST_HOLY:
            nonDisc++;
        case PRIEST_DISCO:
            anyHeals++;
            break;
        default:
            break;
        }

        switch (opponentSpec)
        {
        case PALADIN_HOLY:
        case SHAMAN_RESTO:
        case DRUID_RESTO:
        case PRIEST_HOLY:
            nonDisc++;
        case PRIEST_DISCO:
            anyHeals++;
            break;
        default:
            break;
        }

        if (anyHeals == 2)
        {
            flags += 4;
        }

        if (nonDisc > 0)
        {
            flags += 2;
        }

        return flags;
    }

    static inline int32 GetCount(char const* args)
    {
        int32 count = strtol(args, nullptr, 10);
        return std::max(count, 1);
    }

    enum IDs
    {
        EMBLEM_OF_FROST = 49426,
        EMBLEM_OF_TRIUMPH = 47241,
        EMBLEM_OF_CONQUEST = 45624,
        EMBLEM_OF_VALOR = 40753,
        EMBLEM_OF_HEROISM = 40752,
        TRANSMOG_COIN = 38186,
        AURA_DMG = 29659,
        AURA_BLOAT = 72219,
        AURA_DEMENTIA_BOOST = 41406,
        AURA_DEMENTIA_NERF = 41409,
        AURA_SHACKLE = 38505,
        AURA_SHADOWS = 71264,
        AURA_PLAGUE = 70911,
        AURA_TORMENT = 58835,
        AURA_DESPAIR = 62692,
        DUEL_FLAG = 21680,
        DUEL_SPELL = 7266,
        GM_FREEZE = 9454,
        AURA_PACIFY = 100028,

        DUEL_ARENA_CHAIR = 189941,

        ARENA_PREPARATION = 32728,
    };
public:
    em_commandscript() : CommandScript("em_commandscript") { }

    std::vector<ChatCommand> GetCommands() const override
    {

        static std::vector<ChatCommand> emBuffCommandTable =
        {
            { "dmg",           rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmBuffDmgCommand,             "" },
            { "healdmg",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmBuffHealdmgCommand,         "" },
        };
        static std::vector<ChatCommand> emDebuffCommandTable =
        {
            { "healdmg",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmDebuffHealdmgCommand,       "" },
            { "shackle",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmDebuffShackleCommand,       "" },
            { "flames",        rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmDebuffFlamesCommand,        "" },
            { "plague",        rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmDebuffPlagueCommand,        "" },
            { "torment",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmDebuffTormentCommand,       "" },
            //{ "despair",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmDebuffDespairCommand,       "" },
        };
        static std::vector<ChatCommand> emUnbuffCommandTable =
        {
            { "dmg",           rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffDmgCommand,           "" },
            { "healdmgbuff",   rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffHealdmgBuffCommand,   "" },
            { "healdmgdebuff", rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffHealdmgDebuffCommand, "" },
            { "shackle",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffShackleCommand,       "" },
            { "flames",        rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffFlamesCommand,        "" },
            { "plague",        rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffPlagueCommand,        "" },
            { "torment",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffTormentCommand,       "" },
            //{ "despair",       rbac::RBAC_PERM_COMMAND_EM_BUFF, false, &HandleEmUnbuffDespairCommand,       "" },
        };
        static std::vector<ChatCommand> emRewardCommandTable =
        {
            { "heroism",       rbac::RBAC_PERM_COMMAND_EM_REWARD, false, &HandleEmRewardHeroismCommand,     "" },
            { "valor",         rbac::RBAC_PERM_COMMAND_EM_REWARD, false, &HandleEmRewardValorCommand,       "" },
            { "conquest",      rbac::RBAC_PERM_COMMAND_EM_REWARD, false, &HandleEmRewardConquestCommand,    "" },
            { "triumph",       rbac::RBAC_PERM_COMMAND_EM_REWARD, false, &HandleEmRewardTriumphCommand,     "" },
            { "frost",         rbac::RBAC_PERM_COMMAND_EM_REWARD, false, &HandleEmRewardFrostCommand,       "" },
            { "transmog",      rbac::RBAC_PERM_COMMAND_EM_REWARD, false, &HandleEmRewardTransmogCommand,    "" },
        };
        static std::vector<ChatCommand> emCommandTable =
        {
            { "buff",          rbac::RBAC_PERM_COMMAND_EM_BUFF,     false, nullptr,       "", emBuffCommandTable },
            { "debuff",        rbac::RBAC_PERM_COMMAND_EM_BUFF,     false, nullptr,       "", emDebuffCommandTable },
            { "unbuff",        rbac::RBAC_PERM_COMMAND_EM_BUFF,     false, nullptr,       "", emUnbuffCommandTable },
            { "reward",        rbac::RBAC_PERM_COMMAND_EM_REWARD,   false, nullptr,       "", emRewardCommandTable },
            { "blinktest",     rbac::RBAC_PERM_COMMAND_EM_OTHER,    false, &HandleBlinkCommand,    "",},
            { "unall",         rbac::RBAC_PERM_COMMAND_EM_BUFF,     false, &HandleEmUnallCommand,  "", },
            { "rduel",         rbac::RBAC_PERM_COMMAND_EM_DUEL,     false, &HandleEmDuelCommand,   "Write nicks of two players for duel in 1v1 arena in .tele fatduel.", },
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "em",           rbac::RBAC_PERM_COMMAND_EM, true,  nullptr,              "",  emCommandTable },
        };

        return commandTable;
    }

    static bool HandleEmRewardHeroismCommand(ChatHandler* handler, char const* args) { return AddItem(handler, EMBLEM_OF_HEROISM, GetCount(args)); }
    static bool HandleEmRewardValorCommand(ChatHandler* handler, char const* args) { return AddItem(handler, EMBLEM_OF_VALOR, GetCount(args)); }
    static bool HandleEmRewardConquestCommand(ChatHandler* handler, char const* args) { return AddItem(handler, EMBLEM_OF_CONQUEST, GetCount(args)); }
    static bool HandleEmRewardTriumphCommand(ChatHandler* handler, char const* args) { return AddItem(handler, EMBLEM_OF_TRIUMPH, GetCount(args)); }
    static bool HandleEmRewardFrostCommand(ChatHandler* handler, char const* args) { return AddItem(handler, EMBLEM_OF_FROST, GetCount(args)); }
    static bool HandleEmRewardTransmogCommand(ChatHandler* handler, char const* args) { return AddItem(handler, TRANSMOG_COIN, GetCount(args)); }
    static bool AddItem(ChatHandler* handler, uint32 itemId, int32 count)
    {
        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
        {
            handler->PSendSysMessage(LANG_COMMAND_ITEMIDINVALID, itemId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        Player* player = handler->GetSession()->GetPlayer();
        Player* playerTarget = handler->getSelectedPlayer();
        if (!playerTarget)
            playerTarget = player;

        // Subtract
        if (count < 0)
        {
            playerTarget->DestroyItemCount(itemId, -count, true, false);
            handler->PSendSysMessage(LANG_REMOVEITEM, itemId, -count, handler->GetNameLink(playerTarget).c_str());
            return true;
        }

        // Adding items
        uint32 noSpaceForCount = 0;

        // check space and find places
        ItemPosCountVec dest;
        InventoryResult msg = playerTarget->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, count, &noSpaceForCount);
        if (msg != EQUIP_ERR_OK)                               // convert to possible store amount
            count -= noSpaceForCount;

        if (count == 0 || dest.empty())                         // can't add any
        {
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);
            handler->SetSentErrorMessage(true);
            return false;
        }

        Item* item = playerTarget->StoreNewItem(dest, itemId, true, GenerateItemRandomPropertyId(itemId));

        // remove binding (let GM give it to another player later)
        if (player == playerTarget)
            for (ItemPosCountVec::const_iterator itr = dest.begin(); itr != dest.end(); ++itr)
                if (Item* item1 = player->GetItemByPos(itr->pos))
                    item1->SetBinding(false);

        if (count > 0 && item)
        {
            player->SendNewItem(item, count, false, true);
            if (player != playerTarget)
                playerTarget->SendNewItem(item, count, true, false);
        }

        if (noSpaceForCount > 0)
            handler->PSendSysMessage(LANG_ITEM_CANNOT_CREATE, itemId, noSpaceForCount);

        std::string itemname;
        switch (itemId)
        {
            case EMBLEM_OF_HEROISM:
                itemname = "Emblem of Heroism";
                break;
            case EMBLEM_OF_VALOR:
                itemname = "Emblem of Valor";
                break;
            case EMBLEM_OF_CONQUEST:
                itemname = "Emblem of Conquest";
                break;
            case EMBLEM_OF_TRIUMPH:
                itemname = "Emblem of Triumph";
                break;
            case EMBLEM_OF_FROST:
                itemname = "Emblem of Frost";
                break;
            default:
                return true;
        }

        playerTarget->DisplayMessage(player, CHAT_MSG_RAID_BOSS_EMOTE, "You have been awarded %i times %s! by %s", count, itemname, player->GetName());
        return true;
    }


    static bool HandleEmBuffDmgCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_BLOAT); }
    static bool HandleEmBuffHealdmgCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_DEMENTIA_BOOST); }
    static bool HandleEmDebuffHealdmgCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_DEMENTIA_NERF); }
    static bool HandleEmDebuffShackleCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_SHACKLE); }
    static bool HandleEmDebuffFlamesCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_SHADOWS); }
    static bool HandleEmDebuffPlagueCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_PLAGUE); }
    static bool HandleEmDebuffTormentCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_TORMENT); }
    //static bool HandleEmDebuffDespairCommand(ChatHandler* handler, char const* args) { return AddAura(handler, AURA_DESPAIR); }
    static bool AddAura(ChatHandler* handler, uint32 spellId)
    {
        Unit* target = handler->getSelectedUnit();
        if (!target)
        {
            handler->SendSysMessage(LANG_SELECT_CHAR_OR_CREATURE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId))
            Aura::TryRefreshStackOrCreate(spellInfo, MAX_EFFECT_MASK, target, target);

        return true;
    }


    static bool HandleEmUnbuffDmgCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_BLOAT); }
    static bool HandleEmUnbuffHealdmgBuffCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_DEMENTIA_BOOST); }
    static bool HandleEmUnbuffHealdmgDebuffCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_DEMENTIA_NERF); }
    static bool HandleEmUnbuffShackleCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_SHACKLE); }
    static bool HandleEmUnbuffFlamesCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_SHADOWS); }
    static bool HandleEmUnbuffPlagueCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_PLAGUE); }
    static bool HandleEmUnbuffTormentCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_TORMENT); }
    //static bool HandleEmUnbuffDespairCommand(ChatHandler* handler, char const* args) { return UnAura(handler, AURA_DESPAIR); }
    static bool UnAura(ChatHandler* handler, uint32 spellId)
    {
        Unit* target = handler->getSelectedUnit();
        if (!target)
        {
            handler->SendSysMessage(LANG_SELECT_CHAR_OR_CREATURE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        target->RemoveAurasDueToSpell(spellId);

        return true;
    }

    static bool HandleEmUnallCommand(ChatHandler* handler, char const* args)
    {
        Unit* target = handler->getSelectedUnit();
        if (!target)
        {
            handler->SendSysMessage(LANG_SELECT_CHAR_OR_CREATURE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        target->RemoveAurasDueToSpell(AURA_BLOAT);
        target->RemoveAurasDueToSpell(AURA_DEMENTIA_BOOST);
        target->RemoveAurasDueToSpell(AURA_DEMENTIA_NERF);
        target->RemoveAurasDueToSpell(AURA_SHACKLE);
        target->RemoveAurasDueToSpell(AURA_SHADOWS);
        target->RemoveAurasDueToSpell(AURA_PLAGUE);
        target->RemoveAurasDueToSpell(AURA_TORMENT);
        //target->RemoveAurasDueToSpell(AURA_DESPAIR);
        return true;
    }

    static bool HandleEmDuelCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        Player* gm = handler->GetSession()->GetPlayer();
        if (!gm)
            return false;

        char const* player1 = strtok((char*)args, " ");
        if (!player1)
        {
            handler->PSendSysMessage("Write nicks of two players for duel in 1v1 arena in .tele fatduel.");
            return false;
        }
        char const*player2 = strtok(nullptr, " ");
        if (!player2)
        {
            handler->PSendSysMessage("Write nicks of two players for duel in 1v1 arena in .tele fatduel.");
            return false;
        }

        Player* p1 = ObjectAccessor::FindPlayerByName(player1);
        Player* p2 = ObjectAccessor::FindPlayerByName(player2);

        if (!p1 || !p2 || p1 == p2)
        {
            handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (p1->duel || p2->duel)
        {
            handler->PSendSysMessage("Duel Failed: %s or %s is already in duel.", player1, player2);
            return false;
        }


        if (p1->GetZoneId() != 267 || p2->GetZoneId() != 267 || p1->GetAreaId() != 2397 || p2->GetAreaId() != 2397)
        {
            handler->PSendSysMessage("Duel Failed: %s or %s isn't in fatduel zone.", player1, player2);
            return false;
        }


        if (gm->GetZoneId() != 267 || gm->GetAreaId() != 2397)
        {
            handler->PSendSysMessage("Duel Failed: You must be in .tele fatduel zone.");
            return false;
        }

        //CREATE DUEL FLAG OBJECT
        GameObject* pGameObj = new GameObject;

        Position const pos = { -1110.0f, 1461.0f, 54.16f, 0 };

        Map* map = p1->GetMap();
        QuaternionData rot = QuaternionData::fromEulerAnglesZYX(pos.GetOrientation(), 0.f, 0.f);
        if (!pGameObj->Create(map->GenerateLowGuid<HighGuid::GameObject>(), DUEL_FLAG, map, p1->GetPhaseMask(), pos, rot, 0, GO_STATE_READY))
        {
            delete pGameObj;
            return false;
        }

        pGameObj->SetFaction(p1->GetFaction());
        pGameObj->SetUInt32Value(GAMEOBJECT_LEVEL, p1->getLevel() + 1);
        pGameObj->SetRespawnTime(0);
        pGameObj->SetSpellId(DUEL_SPELL);

        p1->AddGameObject(pGameObj);
        map->AddToMap(pGameObj);
        //END


        Position pos1 = p1->GetPosition();
        Position pos2 = p2->GetPosition();
        // create duel-info
        uint8 flags = GetDuelType(p1, p2);
        DuelInfo* duel = new DuelInfo;
        duel->initiator = p1;
        duel->opponent = p2;
        duel->savedPos = pos1;
        duel->flags = flags;
        duel->updateTime = -10;
        if ((pos1.m_positionX > -1072.0f && pos1.m_positionX < -1069.1f) || pos1.m_positionX > -1068.0f)
            duel->flags += 8;
        duel->objectTimers[DUEL_OBJECT_SHADOW_SIGHT] = 60;
        duel->objectTimers[DUEL_OBJECT_INVISIBILITY] = 75;
        duel->objectTimers[DUEL_OBJECT_SPEED] = 40;
        duel->objectTimers[DUEL_OBJECT_REGENERATION] = 90;
        duel->objectTimers[DUEL_OBJECT_BERSERK] = 75;
        duel->objectTimers[DUEL_OBJECT_BLINK] = 80;
        duel->objectTimers[DUEL_OBJECT_RED_TELEPORT] = 100;
        duel->objectTimers[DUEL_OBJECT_GREEN_TELEPORT] = 100;
        duel->objectTimers[DUEL_OBJECT_BLUE_TELEPORT] = 100;
        duel->objectTimers[DUEL_OBJECT_YELLOW_TELEPORT] = 100;
        p1->duel = duel;

        DuelInfo* duel2 = new DuelInfo;
        duel2->initiator = p1;
        duel2->opponent = p1;
        duel2->savedPos = pos2;
        duel2->flags = flags;
        duel2->updateTime = -10;
        if ((pos2.m_positionX > -1072.0f && pos2.m_positionX < -1069.1f) || pos2.m_positionX > -1068.0f)
            duel2->flags += 8;
        p2->duel = duel2;

        p1->SetGuidValue(PLAYER_DUEL_ARBITER, pGameObj->GetGUID());
        p2->SetGuidValue(PLAYER_DUEL_ARBITER, pGameObj->GetGUID());

        sScriptMgr->OnPlayerDuelRequest(p2, p1);

        time_t now = time(nullptr);
        p1->duel->startTimer = now + 15;
        p2->duel->startTimer = now + 15;
        p1->duel->startTime = now + 15;
        p2->duel->startTime = now + 15;

        p1->UpdateVisibilityForPlayer();
        p2->UpdateVisibilityForPlayer();


        // Unfreeze
        p1->RemoveAurasDueToSpell(GM_FREEZE);
        p2->RemoveAurasDueToSpell(GM_FREEZE);

        // Remove lock port
        p1->RemoveAura(62388);
        p2->RemoveAura(62388);
        p1->RemoveGameObject(48018, true);
        p2->RemoveGameObject(48018, true);

        // Unbuff
        p1->RemoveArenaAuras();
        if (p1->GetPet())
            p1->GetPet()->RemoveArenaAuras();
        p2->RemoveArenaAuras();
        if (p2->GetPet())
            p2->GetPet()->RemoveArenaAuras();

        // Disable regen with 2 healers
        if (flags & DUEL_TWO_HEALS)
        {
            if (SpellInfo const* despair = sSpellMgr->GetSpellInfo(AURA_DESPAIR))
            {
                Aura::TryRefreshStackOrCreate(despair, MAX_EFFECT_MASK, p1, p1);
                Aura::TryRefreshStackOrCreate(despair, MAX_EFFECT_MASK, p2, p2);
            }
        }

        // Start with 100% damage boost if healer playing
        if (flags & DUEL_ONE_HEAL)
        {
            if (SpellInfo const* dmg = sSpellMgr->GetSpellInfo(AURA_DMG))
            {
                if (Aura *p1dmg = Aura::TryRefreshStackOrCreate(dmg, MAX_EFFECT_MASK, p1, p1))
                    p1dmg->ModStackAmount(9);
                if (Aura *p2dmg = Aura::TryRefreshStackOrCreate(dmg, MAX_EFFECT_MASK, p2, p2))
                    p2dmg->ModStackAmount(9);
            }
        }

        // Give free and instant spells
        if (SpellInfo const* arenaPreparation = sSpellMgr->GetSpellInfo(ARENA_PREPARATION))
        {
            Aura::TryRefreshStackOrCreate(arenaPreparation, MAX_EFFECT_MASK, p1, p1);
            Aura::TryRefreshStackOrCreate(arenaPreparation, MAX_EFFECT_MASK, p2, p2);
        }
        p1->SetCommandStatusOn(CHEAT_POWER);
        p2->SetCommandStatusOn(CHEAT_POWER);
        p1->SetCommandStatusOn(CHEAT_CASTTIME);
        p2->SetCommandStatusOn(CHEAT_CASTTIME);

        // Root at start positions
        if (SpellInfo const* shackle = sSpellMgr->GetSpellInfo(AURA_SHACKLE))
        {
            Aura::TryRefreshStackOrCreate(shackle, MAX_EFFECT_MASK, p1, p1);
            Aura::TryRefreshStackOrCreate(shackle, MAX_EFFECT_MASK, p2, p2);
        }

        p1->TeleportTo(0, -1110.0f, 1453.0f, 54.16f, 1.57f);
        p2->TeleportTo(0, -1110.0f, 1469.0f, 54.16f, 4.71f);

        // ANNOUNCE
        std::string class1 = p1->GetSpecName();
        std::string class2 = p2->GetSpecName();
        p1->DisplayMessage(gm, CHAT_MSG_RAID_BOSS_EMOTE, "Your duel is against %s - %s!", p2->GetName(), class2);
        p2->DisplayMessage(gm, CHAT_MSG_RAID_BOSS_EMOTE, "Your duel is against %s - %s!", p1->GetName(), class1);
        return true;
    }

    static bool HandleBlinkCommand(ChatHandler* handler, char const* args)
    {
        Player* plr = handler->GetSession()->GetPlayer();
        if (!plr)
            return false;

        Position destination = plr->GetPosition();
        // Consts to modify behaviour
        float maxDistance = 30.0f; // = m_spellInfo->Effects[effIndex].CalcRadius(m_caster);
        float groundCheckDistance = 10.0f; // how far down to check for ground
        float stepDistance = 1.0f; // how big horizontal steps to do each step
        float stepAngleTolerance = 2.0f; // tangent of angle which it will allow to climb (up or down)

        int airMode = 1; // enables blinking ending in air
        float edgeMaxDistance = 3.0f; // if mage stands at edge aiming into empty air, how close to edge does he have to be to blink into air and not end at edge (only relevant if airmode on), set to 2*maxDistance to disable

        char *dist, *ground, *step, *angle, *air, *edgeD;
        dist = strtok((char*)args, " ");
        ground = strtok(nullptr, " ");
        step = strtok(nullptr, " ");
        angle = strtok(nullptr, " ");
        air = strtok(nullptr, " ");
        edgeD = strtok(nullptr, " ");

        if (dist)
            maxDistance = atof(dist);

        if (ground)
            groundCheckDistance = atof(ground);

        if (step)
            stepDistance = atof(step);

        if (angle)
            stepAngleTolerance = atof(angle);

        if (air)
            airMode = atoi(air);

        if (edgeD)
            edgeMaxDistance = atof(edgeD);


        // Initialize //
        Position solidPos = plr->GetPosition(); // last legit position
        Position airPos = solidPos; // last valid air position
        float x, y, z; plr->GetPosition(x, y, z); // scouting positions ahead
        float horizontalDistance = 0.0f;
        bool edge = false; // did we end at edge (need to prevent sliding)
        bool found = false; // did we find solid spot

        // first try looking straight ahead (doesnt matter if already on ground or not)
        while (plr->GetDistance(x,y,solidPos.GetPositionZ()) < maxDistance - stepDistance/2.0f) // with no big vertical changes, maxDistance is the real distance traveled, otherwise most likely between distance +- stepDistance/2
        {
            airPos.Relocate(x, y, solidPos.GetPositionZ());
            horizontalDistance += stepDistance;
            plr->GetNearPoint2D(x, y, horizontalDistance, solidPos.GetOrientation()); // check step forward
            plr->UpdateGroundPositionZ(x, y, z);

            if (z > solidPos.GetPositionZ() + stepAngleTolerance*stepDistance) // hit wall
                break;

            if (z > solidPos.GetPositionZ() - stepAngleTolerance*stepDistance)
            {
                if (!plr->GetMap()->isInLineOfSight(x, y, z + 2.0f, solidPos.GetPositionX(), solidPos.GetPositionY(), solidPos.GetPositionZ() + 2.0f, plr->GetPhaseMask(), LINEOFSIGHT_ALL_CHECKS, VMAP::ModelIgnoreFlags::Nothing))
                    break; // some slim wall in the way

                solidPos.Relocate(x,y,z);
                found = true;
                edge = false;
            }
            else
            {
                if (found)
                    edge = true;
            }

            z = solidPos.GetPositionZ() + 3.0f; //Map::GetHeight() checks from oldValue+2y down, need to check from original height +5y
        }

        if (!found) // going ahead didnt find solid position, so check if there is some ground below
        {
            plr->GetPosition(x, y, z);
            horizontalDistance = 0.0f;
            edge = false;
            plr->UpdateGroundPositionZ(x, y, z); // first check right below

            // this means that player that just dropped under texture can blink back up (not checking LoS here)
            // next steps need to be withing LoS either from previous solidPos or Player
            if (z > solidPos.GetPositionZ() - groundCheckDistance)
            {
                solidPos.Relocate(x, y, z);
                found = true;
            }

            while (plr->GetDistance(x, y, solidPos.GetPositionZ()) < maxDistance - stepDistance / 2.0f)
            {
                horizontalDistance += stepDistance;
                plr->GetNearPoint2D(x, y, horizontalDistance, solidPos.GetOrientation()); // check step forward
                z = plr->GetPositionZ() + 3.0f; // we want highest ground up to a bit over casters height to know if we hit wall
                plr->UpdateGroundPositionZ(x, y, z);

                if (z > plr->GetPositionZ() + stepAngleTolerance*stepDistance) // hit wall
                    break;

                // first try to move ahead from ground (or original) position - allows blinking into tunnel not visible from original position
                if (z < solidPos.GetPositionZ() + stepAngleTolerance*stepDistance) // didnt hit wall
                {
                    if (z > solidPos.GetPositionZ() - stepAngleTolerance*stepDistance)
                    {
                        if (plr->GetMap()->isInLineOfSight(x, y, z + 2.0f, solidPos.GetPositionX(), solidPos.GetPositionY(), solidPos.GetPositionZ() + 2.0f, plr->GetPhaseMask(), LINEOFSIGHT_ALL_CHECKS, VMAP::ModelIgnoreFlags::Nothing))
                        {
                            solidPos.Relocate(x, y, z);
                            found = true;
                            edge = false;
                            continue;
                        }
                    }
                    else
                    {
                        if (found)
                            edge = true;
                    }
                }

                if (z < solidPos.GetPositionZ() - groundCheckDistance) // ground too low / none
                {
                    if (found)
                        edge = true;
                }
                else if (plr->IsWithinLOS(x, z, y)) // fine height and visible, land here
                {
                    solidPos.Relocate(x, y, z);
                    found = true;
                    edge = false;
                }
            }
        }

        // lets blink in the air if activated and 1) didnt find solid ground 2) too close to edge
        if (airMode && (plr->GetDistance(solidPos) < stepDistance || (edge && plr->GetDistance(solidPos) < edgeMaxDistance)))
        {
            destination.Relocate(airPos);
        }
        else
        {
            if (edge) // might be checkd in more details
            {
                float backX = solidPos.GetPositionX() - 2.0f * cos(solidPos.GetOrientation());
                float backY = solidPos.GetPositionY() - 2.0f * sin(solidPos.GetOrientation());
                z = solidPos.GetPositionZ();
                plr->UpdateGroundPositionZ(backX, backY, z);

                if (z > solidPos.GetPositionZ())
                    destination.Relocate(backX, backY, z);
                else
                    destination.Relocate(solidPos);
            }
            else
                destination.Relocate(solidPos);
        }

        plr->TeleportTo(WorldLocation(plr->GetMapId(), destination));
        return true;
    }
};

void AddSC_em_commandscript()
{
    new em_commandscript();
}



class challenge_commandscript : public CommandScript
{
    enum ArenaSlot
    {
        ARENA_2v2 = 0,
        ARENA_3v3 = 1,
        ARENA_1v1 = 2,
    };
public:
    challenge_commandscript() : CommandScript("challenge_commandscript") { }

    std::vector<ChatCommand> GetCommands() const
    {
        static std::vector<ChatCommand> challengeCommandTable =
        {
            //{ "1v1",         rbac::RBAC_PERM_COMMAND_CHALLENGE,    false, &HandleChallenge1x1Command,      ""},
            { "2v2",         rbac::RBAC_PERM_COMMAND_CHALLENGE,    false, &HandleChallenge2x2Command,      ""},
            { "3v3",         rbac::RBAC_PERM_COMMAND_CHALLENGE,    false, &HandleChallenge3x3Command,      ""},
            { "on",          rbac::RBAC_PERM_COMMAND_CHALLENGE,    false, &HandleChallengeOnCommand,       ""},
            { "off",         rbac::RBAC_PERM_COMMAND_CHALLENGE,    false, &HandleChallengeOffCommand,      ""},
        };
        static std::vector<ChatCommand> gmchallenge2v2CommandTable =
        {
            { "random",              rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge2v2RandomCommand,             "" },
            { "nagrandarena",        rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge2v2NagrandArenaCommand,       "" },
            { "bladesedgearena",     rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge2v2BladesEdgeArenaCommand,    "" },
            { "ruinsoflordaeron",    rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge2v2RuinsOfLordaeronCommand,   "" },
            { "dalaransewers",       rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge2v2DalaranSewersCommand,      "" },
            { "ringofvalor",         rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge2v2RingOfValorCommand,        "" },
        };
        static std::vector<ChatCommand> gmchallenge3v3CommandTable =
        {
            { "random",              rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge3v3RandomCommand,             "" },
            { "nagrandarena",        rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge3v3NagrandArenaCommand,       "" },
            { "bladesedgearena",     rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge3v3BladesEdgeArenaCommand,    "" },
            { "ruinsoflordaeron",    rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge3v3RuinsOfLordaeronCommand,   "" },
            { "dalaransewers",       rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge3v3DalaranSewersCommand,      "" },
            { "ringofvalor",         rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, &HandleGmChallenge3v3RingOfValorCommand,        "" },
        };
        static std::vector<ChatCommand> gmchallengeCommandTable =
        {
            { "2v2",        rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, nullptr, "", gmchallenge2v2CommandTable },
            { "3v3",        rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,   false, nullptr, "", gmchallenge3v3CommandTable },
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "challenge",       rbac::RBAC_PERM_COMMAND_CHALLENGE,     false, nullptr,  "", challengeCommandTable },
            { "gmchallenge",     rbac::RBAC_PERM_COMMAND_GM_CHALLENGE,  false, nullptr,  "", gmchallengeCommandTable },
        };
        return commandTable;
    }      ///// FIX TABLE FUNCTIONS
    static bool HandleGmChallenge2v2RandomCommand(ChatHandler* handler, char const* args)           { return SendCommand(handler, args, ARENA_2v2, BATTLEGROUND_AA); }
    static bool HandleGmChallenge2v2NagrandArenaCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_2v2, BATTLEGROUND_NA); }
    static bool HandleGmChallenge2v2BladesEdgeArenaCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_2v2, BATTLEGROUND_BE); }
    static bool HandleGmChallenge2v2RuinsOfLordaeronCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_2v2, BATTLEGROUND_RL); }
    static bool HandleGmChallenge2v2DalaranSewersCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_2v2, BATTLEGROUND_DS); }
    static bool HandleGmChallenge2v2RingOfValorCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_2v2, BATTLEGROUND_RV); }
    static bool HandleGmChallenge3v3RandomCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_3v3, BATTLEGROUND_AA); }
    static bool HandleGmChallenge3v3NagrandArenaCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_3v3, BATTLEGROUND_NA); }
    static bool HandleGmChallenge3v3BladesEdgeArenaCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_3v3, BATTLEGROUND_BE); }
    static bool HandleGmChallenge3v3RuinsOfLordaeronCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_3v3, BATTLEGROUND_RL); }
    static bool HandleGmChallenge3v3DalaranSewersCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_3v3, BATTLEGROUND_DS); }
    static bool HandleGmChallenge3v3RingOfValorCommand(ChatHandler* handler, char const* args) { return SendCommand(handler, args, ARENA_3v3, BATTLEGROUND_RV); }
    static bool SendCommand(ChatHandler* handler, char const* args, uint8 type, BattlegroundTypeId bgTypeId)
    {
        if (!*args)
            return false;

        // nope..
        if (!handler)
            return false;

        return CreateSkirmish(handler, (char*)args, type, true, handler->GetSession()->GetAccountId(), bgTypeId);
    }

    static bool CreateSkirmish(ChatHandler* handler, char* args, uint8 type, bool isGroup, uint32 gmId = 0, BattlegroundTypeId bgTypeId = BATTLEGROUND_AA)
    {
        char* pParam1 = strtok(args, " ");
        Player *player1, *player2;

        if (!pParam1)
            return false;

        if (gmId)
        {
            char* pParam2 = strtok(NULL, " ");
            if (!pParam2)
                return false;

            if (!handler->extractPlayerTarget(pParam2, &player2))
            {
                handler->PSendSysMessage("Second player not found.");
                handler->SetSentErrorMessage(true);
                return false;
            }
        }
        else
            player2 = handler->GetSession()->GetPlayer();

        if (!handler->extractPlayerTarget(pParam1, &player1))
        {
            handler->PSendSysMessage("First player not found.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player1->GetGUID() == player2->GetGUID())
        {
            handler->PSendSysMessage("Player cannot fight against himself.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // if (type == 0) then its 2v2, if (type == 1) its 3v3
        if (type == 0 || type == 1)
        {
            Group *gr1 = player1->GetGroup();
            Group *gr2 = player2->GetGroup();

            // check having party
            if (!gr1)
            {
                handler->PSendSysMessage("Player %s doesn't have a party.", player1->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }
            if (!gr2)
            {
                handler->PSendSysMessage("Player %s doesn't have a party.", player2->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }

            // check being leaders
            if (!gr1->IsLeader(player1->GetGUID()))
            {
                handler->PSendSysMessage("Player %s isn't party leader.", player1->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }
            if (!gr2->IsLeader(player2->GetGUID()))
            {
                handler->PSendSysMessage("Player %s isn't party leader.", player2->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }

            // check if their party size matches
            if (type == 0)
            {
                if (gr1->GetMembersCount() != 2)
                {
                    handler->PSendSysMessage("Player %s has %u people in party (must have exactly 2)", player1->GetName(), gr1->GetMembersCount());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
                else if (gr2->GetMembersCount() != 2)
                {
                    handler->PSendSysMessage("Player %s has %u people in party (must have exactly 2)", player2->GetName(), gr2->GetMembersCount());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }
            // 3v3
            else
            {
                if (gr1->GetMembersCount() != 3)
                {
                    handler->PSendSysMessage("Player %s has %u people in party (must have exactly 3)", player1->GetName(), gr1->GetMembersCount());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
                else if (gr2->GetMembersCount() != 3)
                {
                    handler->PSendSysMessage("Player %s has %u people in party (must have exactly 3)", player2->GetName(), gr2->GetMembersCount());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }

            // dont check challenge status as gm
            if (!gmId)
            {
                if (!player1->_challengeOn)
                {
                    handler->PSendSysMessage("Player %s hasn't activated accepting challenges.", player1->GetName());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }

            // check combat
            for (GroupReference* itr = gr1->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* plrg = itr->GetSource();
                if (!plrg)
                    continue;

                if (plrg->IsInCombat())
                {
                    handler->PSendSysMessage("Player %s is in combat.", plrg->GetName());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }
            for (GroupReference* itr = gr2->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* plrg = itr->GetSource();
                if (!plrg)
                    continue;

                if (plrg->IsInCombat())
                {
                    handler->PSendSysMessage("Player %s is in combat.", plrg->GetName());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }

            // check if anyone is in bg
            for (GroupReference* itr = gr1->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* plrg = itr->GetSource();
                if (!plrg)
                    continue;

                if (plrg->GetMap() && plrg->GetMap()->IsBattlegroundOrArena())
                {
                    handler->PSendSysMessage("Player %s is in battleground or arena.", plrg->GetName());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }
            for (GroupReference* itr = gr2->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* plrg = itr->GetSource();
                if (!plrg)
                    continue;

                if (plrg->GetMap() && plrg->GetMap()->IsBattlegroundOrArena())
                {
                    handler->PSendSysMessage("Player %s is in battleground or arena.", plrg->GetName());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }

            // check if anyone is in bgqueue
            for (GroupReference* itr = gr1->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* plrg = itr->GetSource();
                if (!plrg)
                    continue;

                if (plrg->InBattlegroundQueue())
                {
                    handler->PSendSysMessage("Hrac %s je v battleground queue.", plrg->GetName());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }
            for (GroupReference* itr = gr2->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* plrg = itr->GetSource();
                if (!plrg)
                    continue;

                if (plrg->InBattlegroundQueue())
                {
                    handler->PSendSysMessage("Hrac %s je v battleground queue.", plrg->GetName());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }
        }
        // 1v1
        else
        {
            // grp1 is group of the guy extracted from 1st param
            Group *gr1 = player1->GetGroup();
            // grp2 is group of the guy who wrote the challenge command
            Group *gr2 = player2->GetGroup();

            // check if any of them have group
            if (gr2)
            {
                handler->PSendSysMessage("You must leave the party to join 1v1 challenge.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            if (gr1)
            {
                handler->PSendSysMessage("Player %s has a party.", player1->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }
            // check if plr has challenge on
            if (!player1->_challengeOn)
            {
                handler->PSendSysMessage("Player %s hasn't activated accepting challenges.", player1->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }
            // check combat
            if (player2->IsInCombat())
            {
                handler->PSendSysMessage("You are in combat.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            if (player1->IsInCombat())
            {
                handler->PSendSysMessage("Player %s is in combat.", player1->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }
            // check if any of them is in battleground or arena
            if (player2->GetMap() && player2->GetMap()->IsBattlegroundOrArena())
            {
                handler->PSendSysMessage("You are in battleground or arena.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            if (player1->GetMap() && player1->GetMap()->IsBattlegroundOrArena())
            {
                handler->PSendSysMessage("Player %s is in battleground or arena.", player1->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }
            // check if any of them is in queue
            if (player2->InBattlegroundQueue())
            {
                handler->PSendSysMessage("You must leave battleground / arena queue.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            if (player1->InBattlegroundQueue())
            {
                handler->PSendSysMessage("Player %s is in battleground / arena queue.", player1->GetName());
                handler->SetSentErrorMessage(true);
                return false;
            }
        }


        WorldPacket data1(CMSG_BATTLEMASTER_JOIN_ARENA, 8 + 1 + 1 + 1);
        data1 << uint64(0);
        data1 << uint8(type);
        if (isGroup)
            data1 << uint8(1);
        else
            data1 << uint8(0);

        data1 << uint8(0);

        WorldPacket data2(CMSG_BATTLEMASTER_JOIN_ARENA, 8 + 1 + 1 + 1);
        data2 << uint64(0);
        data2 << uint8(type);
        if (isGroup)
            data2 << uint8(1);
        else
            data2 << uint8(0);
        data2 << uint8(0);

        ArenaType aType;

        switch (type)
        {
        case 0:
            aType = ARENA_TYPE_2v2;
            break;
        case 1:
            aType = ARENA_TYPE_3v3;
            break;
        case 2:
            aType = ARENA_TYPE_5v5;
            break;
        default:
            //sLog->outError("Unknown arena slot %u at CreateSkirmish", type);
            return false;
        }

        player1->SetChallengeStatus(aType, CHALLENGE_READY, bgTypeId, gmId);
        player2->SetChallengeStatus(aType, CHALLENGE_READY, bgTypeId, gmId);
        player1->GetSession()->HandleBattlemasterJoinArena(data1);
        player2->GetSession()->HandleBattlemasterJoinArena(data2);

        if (player1->GetChallengeStatus(aType) != CHALLENGE_JOINED)
        {
            player1->SetChallengeStatus(aType, CHALLENGE_NONE);
            handler->PSendSysMessage("First team couldn't join the challenge, please contact developer.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player2->GetChallengeStatus(aType) != CHALLENGE_JOINED)
        {
            player2->SetChallengeStatus(aType, CHALLENGE_NONE);
            handler->PSendSysMessage("Second team couldn't join the challenge, please contact developer.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (gmId)
        {
            ChatHandler(player1->GetSession()).PSendSysMessage("GM %s selected you for a challenge against %s.", handler->GetSession()->GetPlayer()->GetName(), player2->GetName());
            ChatHandler(player2->GetSession()).PSendSysMessage("GM %s selected you for a challenge against %s.", handler->GetSession()->GetPlayer()->GetName(), player1->GetName());
            handler->PSendSysMessage("You sent %s and %s into a challenge.", player1->GetName(), player2->GetName());
        }
        else
        {
            ChatHandler(player1->GetSession()).PSendSysMessage("%s has requested a challenge.", player2->GetName());
        }

        return true;
    }


    static bool HandleChallenge2x2Command(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        return CreateSkirmish(handler, (char*)args, ARENA_2v2, true);
    }

    static bool HandleChallenge3x3Command(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;
        return CreateSkirmish(handler, (char*)args, ARENA_3v3, true);
    }

    static bool HandleChallenge1x1Command(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        return CreateSkirmish(handler, (char*)args, ARENA_1v1, false);
    }

    static bool HandleChallengeOnCommand(ChatHandler* handler, char const* args)
    {
        handler->GetSession()->GetPlayer()->_challengeOn = true;
        handler->PSendSysMessage("You activated accepting challenges.");
        return true;
    }

    static bool HandleChallengeOffCommand(ChatHandler* handler, char const* args)
    {
        handler->GetSession()->GetPlayer()->_challengeOn = false;
        handler->PSendSysMessage("You deactivated accepting challenges.");
        return true;
    }
};

void AddSC_challenge_commandscript()
{
    new challenge_commandscript();
}
