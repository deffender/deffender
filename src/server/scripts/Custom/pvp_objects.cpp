#include "ScriptMgr.h"
#include "GameObjectAI.h"
#include "Chat.h"
#include "SpellAuras.h"
#include "Log.h"
#include "Player.h"
#include "ObjectAccessor.h"
#include "VMapFactory.h"
#include "WorldSession.h"
#include "Map.h"
#include "Random.h"

class pvp_object : public GameObjectScript
{
    enum IDs
    {
        AURA_SHADOW_SIGHT = 34709,
        AURA_INVISIBILITY = 32612,
        AURA_SPEED = 23451,
        AURA_RESTORATION = 23493,
        AURA_BERSERK = 23505,
    };

public:
    pvp_object() : GameObjectScript("pvp_object") { }

    struct pvp_objectAI : public GameObjectAI
    {
        pvp_objectAI(GameObject* obj) : GameObjectAI(obj) { }

        bool GossipHello(Player* plr)
        {
            if (!plr)
                return false;

            plr->RemoveAurasDueToSpell(1784); // break Stealth
            plr->RemoveAurasDueToSpell(32612); // break Invisibility
            uint16 respawnTime = 120;
            switch (me->GetEntry())
            {
                case DUEL_OBJECT_SHADOW_SIGHT:
                    plr->AddAura(AURA_SHADOW_SIGHT, plr);
                    respawnTime = 60;
                    break;
                case DUEL_OBJECT_INVISIBILITY:
                    plr->AddAura(AURA_INVISIBILITY, plr);
                    respawnTime = 75;
                    break;
                case DUEL_OBJECT_SPEED:
                    if (SpellInfo const* speed = sSpellMgr->GetSpellInfo(AURA_SPEED))
                    {
                        if (Aura* aura = Aura::TryRefreshStackOrCreate(speed, MAX_EFFECT_MASK, plr, plr))
                        {
                            aura->SetDuration(3000);
                        }
                    }
                    respawnTime = 40;
                    break;
                case DUEL_OBJECT_REGENERATION:
                    plr->AddAura(AURA_RESTORATION, plr);
                    respawnTime = 90;
                    break;
                case DUEL_OBJECT_BERSERK:
                    plr->AddAura(AURA_BERSERK, plr);
                    respawnTime = 75;
                    break;
                case DUEL_OBJECT_BLINK:
                {
                    const float dis = 30.0f;
                    // Start Info //
                    float cx, cy, cz, angle;
                    plr->GetPosition(cx, cy, cz, angle);
                    float dx, dy, dz, zz;

                    bool useVmap = false;
                    bool swapZone = true;
                    if (VMAP::VMapFactory::createOrGetVMapManager()->isHeightCalcEnabled()) // test Vmap
                        useVmap = true;

                    // Pri vyskoku/gripu apod zacneme na zemi..
                    if (cz - plr->GetMap()->GetHeight(cx, cy, cz, useVmap) < 5)
                        plr->UpdateGroundPositionZ(cx, cy, cz);

                    dz = cz;
                    float bx, by, bz; // na zapamatovani posledniho pevneho bodu
                    bool air = false;

                    for (float i = 0.5f; i <= dis; i += 0.5f) // posun dopredu po 0.5 yardu
                    {
                        if (!air)
                        {
                            bx = cx;
                            by = cy;
                            bz = cz;
                        }

                        plr->GetNearPoint2D(dx, dy, i, angle);
                        zz = plr->GetMap()->GetHeight(dx, dy, cz, useVmap);

                        if ((zz - cz) < 2.0f && (zz - cz) > -2.0f)
                            dz = zz;

                        if ((zz - cz) < 2.0f && (plr->IsWithinLOS(dx, dy, dz)))
                        {
                            cx = dx;
                            cy = dy;
                            cz = dz;
                            if (air)
                            {
                                if ((zz - cz) > -2.0f)
                                    air = false;
                            }
                            else
                            {
                                if ((zz - cz) <= -2.0f)
                                    air = true;
                            }
                        }
                        else
                        {
                            //Na hranicich map muzou blbnout vmapy
                            if (swapZone)
                            {
                                // zkusit bez nich / s nima pokud se to obracene nepovedlo
                                swapZone = false;
                                useVmap = !useVmap;
                                i -= 0.5f;
                            }
                            else
                            {
                                // kdyz nic z toho nevyjde tak vyuzijem posledni souradnice
                                dz += 0.5f;
                                break;
                            }
                        }
                    }

                    bool updated = false;
                    //Nakonec postavit na zem a portnout
                    if (cz - plr->GetMap()->GetHeight(cx, cy, cz, useVmap) > 4)
                    {
                        cx = bx - 2.0f * cos(angle);
                        cy = by - 2.0f * sin(angle);
                        cz = bz;

                        if (cz - plr->GetMap()->GetHeight(cx, cy, cz, useVmap) < 3)
                        {
                            plr->UpdateGroundPositionZ(cx, cy, cz);
                            plr->TeleportTo(plr->GetMapId(), cx, cy, cz, angle);
                            updated = true;
                        }
                    }
                    if (!updated)
                    {
                        if (bz - plr->GetMap()->GetHeight(bx, by, bz, useVmap) < 7)
                            plr->UpdateGroundPositionZ(bx, by, bz);

                        plr->TeleportTo(plr->GetMapId(), bx, by, bz, angle);
                    }
                    respawnTime = 80;
                    break;
                }
                case DUEL_OBJECT_RED_TELEPORT:
                case DUEL_OBJECT_GREEN_TELEPORT:
                case DUEL_OBJECT_BLUE_TELEPORT:
                case DUEL_OBJECT_YELLOW_TELEPORT:
                {
                    if (!plr->duel)
                        return false;

                    uint32 id = me->GetEntry();
                    uint32 r = urand(1, 3);
                    for (int i = 0; i < r; i++)
                    {
                        id = id == DUEL_OBJECT_YELLOW_TELEPORT ? DUEL_OBJECT_RED_TELEPORT : id + 1;
                    }

                    if (GameObject *go = me->FindNearestGameObject(id, 100))
                        plr->TeleportTo(go->GetWorldLocation());

                    respawnTime = plr->duel->initiator->duel->updateTime + 100;
                    plr->duel->initiator->duel->objectTimers[DUEL_OBJECT_RED_TELEPORT] = respawnTime;
                    plr->duel->initiator->duel->objectTimers[DUEL_OBJECT_GREEN_TELEPORT] = respawnTime;
                    plr->duel->initiator->duel->objectTimers[DUEL_OBJECT_BLUE_TELEPORT] = respawnTime;
                    plr->duel->initiator->duel->objectTimers[DUEL_OBJECT_YELLOW_TELEPORT] = respawnTime;
                    me->UpdateObjectVisibility();
                    return true;
                }
                case DUEL_OBJECT_TOWER_PORTAL_ONE:
                    if (GameObject *go = me->FindNearestGameObject(DUEL_OBJECT_TOWER_PORTAL_TWO, 200))
                        plr->TeleportTo(go->GetWorldLocation());
                    break;
                case DUEL_OBJECT_TOWER_PORTAL_TWO:
                    if (GameObject *go = me->FindNearestGameObject(DUEL_OBJECT_TOWER_PORTAL_ONE, 200))
                        plr->TeleportTo(go->GetWorldLocation());
                    break;
                case DUEL_OBJECT_RANDOM_PORTAL:
                    if (GameObject *go = me->FindNearestGameObject(urand(DUEL_OBJECT_RED_TELEPORT, DUEL_OBJECT_YELLOW_TELEPORT), 100))
                        plr->TeleportTo(go->GetWorldLocation());
                    break;
                default:
                    return false;
            }
            if (!plr->duel || me->GetEntry() > DUEL_OBJECT_YELLOW_TELEPORT)
                return true;

            plr->duel->initiator->duel->objectTimers[me->GetEntry()] = plr->duel->initiator->duel->updateTime + respawnTime;
            me->UpdateObjectVisibility();
            return true;
        }
    };

    GameObjectAI* GetAI(GameObject* creature) const override
    {
        return new pvp_objectAI(creature);
    }
};

void AddSC_pvp_object()
{
    new pvp_object();
}
