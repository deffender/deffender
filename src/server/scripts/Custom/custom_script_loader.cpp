/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// This is where scripts' loading functions should be declared:
void AddSC_arena_spectator_script();
// void AddSC_arena_top_teams();
void AddSC_command_mmr();
void AddSC_DeffWorldScript();
void AddSC_donate_commands();
void AddSC_lachtan_scripts();
void AddSC_lifetime_titles();
void AddSC_npc_deffender_vendor();
void AddSC_npc_dynamic_teleporter();
void AddSC_npc_changer();
void AddSC_object_arena_crystal();
// void AddSC_object_display_id_detect();
void AddSC_remove_dementia();
void AddSC_Transmogrification();
void AddSC_trolling_commandscript();
void AddSC_em_commandscript();
void AddSC_challenge_commandscript();
void AddSC_pvp_object();

// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddCustomScripts()
{
    AddSC_arena_spectator_script();
    // AddSC_arena_top_teams();
    AddSC_command_mmr();
    AddSC_DeffWorldScript();
    AddSC_donate_commands();
    AddSC_lachtan_scripts();
    AddSC_lifetime_titles();
    AddSC_npc_deffender_vendor();
    AddSC_npc_dynamic_teleporter();
    AddSC_npc_changer();
    AddSC_object_arena_crystal();
    // AddSC_object_display_id_detect();
    AddSC_remove_dementia();
    AddSC_Transmogrification();
    AddSC_trolling_commandscript();
    AddSC_em_commandscript();
    AddSC_challenge_commandscript();
    AddSC_pvp_object();
}
