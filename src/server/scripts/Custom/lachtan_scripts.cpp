#include "Creature.h"
#include "MotionMaster.h"
#include "ScriptedCreature.h"
#include "ScriptMgr.h"
#include "WaypointManager.h"

#define RELOAD_SPELL_DUMMY 36177

struct npc_smooth_wp : public ScriptedAI
{
    npc_smooth_wp(Creature* creature) : ScriptedAI(creature) { }

    void JustAppeared() override
    {
        DoMovePath();
    }

    void Reset() override
    {
        DoMovePath();
    }

    void EnterEvadeMode(EvadeReason why) override
    {
        me->DespawnOrUnsummon(0, Seconds(5));
    }

private:
    inline bool compare(float a, float b)
    {
        return std::fabs(a - b) < std::numeric_limits<float>::epsilon();
    }

    void DoMovePath()
    {
        if (me->GetMotionMaster()->GetCurrentMovementGeneratorType() == EFFECT_MOTION_TYPE)
            return;

        ObjectGuid::LowType pathId = me->GetSpawnId();
        // check overflow
        if (pathId > std::pow(2, sizeof(ObjectGuid::LowType) * 8) / 10)
            return;

        pathId *= 10;
        sWaypointMgr->ReloadPath(pathId);
        auto* path = sWaypointMgr->GetPath(pathId);
        if (!path)
            return;

        auto& first = *path->nodes.begin();
        auto& last = *path->nodes.rbegin();
        size_t size = path->nodes.size();

        if (size < 2)
            return;

        bool cyclic = compare(first.x, last.x) && compare(first.y, last.y) && compare(first.z, last.z);
        if (cyclic)
            size -= 1;

        float gz = first.z;
        me->UpdateGroundPositionZ(first.x, first.y, gz);
        bool fly = std::abs(gz - first.z) > 2.0f;

        Position* points = new Position[size];
        std::transform(path->nodes.begin(), path->nodes.end() - (cyclic ? 1 : 0), points, [](const WaypointNode& node)
        {
            return Position(node.x, node.y, node.z, node.orientation);
        });

        me->GetMotionMaster()->MoveSmoothPath(0, points, size, first.moveType == WAYPOINT_MOVE_TYPE_WALK, cyclic);
        delete[] points;
    }
};

void AddSC_lachtan_scripts()
{
    RegisterCreatureAI(npc_smooth_wp);
}
