#include "AccountMgr.h"
#include "AccountPointsMgr.h"
#include "Chat.h"
#include "DatabaseEnv.h"
#include "DBCStores.h"
#include "EventProcessor.h"
#include "Language.h"
#include "Player.h"
#include "RBAC.h"
#include "ScriptMgr.h"
#include "World.h"
#include "WorldSession.h"

class donate_commands : public CommandScript
{
public:
    donate_commands() : CommandScript("donate_commands") { }

    std::vector<ChatCommand> GetCommands() const
    {
        static std::vector<ChatCommand> changeCommandTable =
        {
            { "donate",       rbac::RBAC_PERM_COMMAND_CURRENCY_CHANGE_DONATE,   false, &HandleCurrencyChangeDonateCommand, "" },
            { "vote",         rbac::RBAC_PERM_COMMAND_CURRENCY_CHANGE_VOTE,     false, &HandleCurrencyChangeVoteCommand,   "" },
        };

        static std::vector<ChatCommand> currencyCommandTable =
        {
            { "change",       rbac::RBAC_PERM_COMMAND_CURRENCY_CHANGE, false, nullptr,                   "", changeCommandTable },
            { "",             rbac::RBAC_PERM_COMMAND_CURRENCY,        false, &HandleCurrencyCommand,    "" },
        };

        static std::vector<ChatCommand> commandTable =
        {
            { "currency",     rbac::RBAC_PERM_COMMAND_CURRENCY,        false, nullptr,                   "", currencyCommandTable },
            { "flightpaths",  rbac::RBAC_PERM_COMMAND_FLIGHTPATHS,     false, &HandleFlightPathsCommand, "" },
        };

        return commandTable;
    }

    static bool HandleCurrencyChangeDonateCommand(ChatHandler* handler, const char* args)
    {
        if (!*args)
        {
            handler->SendSysMessage(LANG_CMD_SYNTAX);
            handler->SetSentErrorMessage(true);
            return false;
        }

        return CurrencyChangeHelper(ACCOUNT_POINTS_DONATE, handler, args);
    }

    static bool HandleCurrencyChangeVoteCommand(ChatHandler* handler, const char* args)
    {
        if (!*args)
        {
            handler->SendSysMessage(LANG_CMD_SYNTAX);
            handler->SetSentErrorMessage(true);
            return false;
        }

        return CurrencyChangeHelper(ACCOUNT_POINTS_VOTE, handler, args);
    }

    static bool CurrencyChangeHelper(AccountPointsType type, ChatHandler* handler, const char* args)
    {
        char* accStr = strtok((char*)args, " ");
        char* numStr = strtok(nullptr, " ");

        if (!accStr || !numStr)
            return false;

        int changeVal = atoi(numStr);
        if (!changeVal)
            return false;

        std::string accountName = accStr;
        if (!Utf8ToUpperOnlyLatin(accountName))
        {
            handler->PSendSysMessage(LANG_ACCOUNT_NOT_EXIST, accountName.c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }

        uint32 accId = AccountMgr::GetId(accountName);
        if (!accId)
        {
            handler->PSendSysMessage(LANG_ACCOUNT_NOT_EXIST, accountName.c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }

        AccountPointsInfo info;
        sAccountPointsMgr->GetPoints(type, accId, info);

        uint32 newVal = std::max(int(info.points) + changeVal, 0);
        sAccountPointsMgr->SetPoints(type, accId, newVal);
        handler->PSendSysMessage("You have changed %s's %ss to %u.", accStr, AccountPointsNames[type], newVal);

        return true;
    }

    static bool HandleCurrencyCommand(ChatHandler* handler, const char* args)
    {
        uint32 acc = handler->GetSession()->GetAccountId();
        std::string message = "You have %u %ss";

        if (handler->GetSession()->HasPermission(rbac::RBAC_PERM_COMMAND_CURRENCY_CHANGE) && *args)
        {
            std::string accountName = args;
            if (!Utf8ToUpperOnlyLatin(accountName))
            {
                handler->PSendSysMessage(LANG_ACCOUNT_NOT_EXIST, accountName.c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            acc = AccountMgr::GetId(accountName);
            if (!acc)
            {
                handler->PSendSysMessage(LANG_ACCOUNT_NOT_EXIST, accountName.c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            handler->PSendSysMessage("Listing points for account %s:", accountName);
            message = "%u %ss";
        }

        for (uint8 i = 0; i < ACCOUNT_POINTS_MAX; ++i)
        {
            AccountPointsInfo info;
            sAccountPointsMgr->GetPoints(AccountPointsType(i), acc, info);
            handler->PSendSysMessage(message.c_str(), info.points, AccountPointsNames[i]);
        }

        return true;
    }

    static bool HandleFlightPathsCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* player = handler->GetSession()->GetPlayer();
        if (player->IsGameMaster())
            if (Player* target = handler->getSelectedPlayer())
                player = target;

        for (auto it : sTaxiNodesStore)
            player->m_taxi.SetTaximaskNode(it->ID);

        if (player != handler->GetSession()->GetPlayer())
            handler->PSendSysMessage("You have granted all taxinodes to %s.", player->GetName());

        ChatHandler(player->GetSession()).PSendSysMessage("You have learned all taxi nodes.");
        return true;
    }
};

// std::array<uint32, 34> dkQuests =
// {
//     12593, // In Service Of The Lich King
//     12619, // The Emblazoned Runeblade
//     12842, // Runeforging: Preparation For Battle
//     12848, // The Endless Hunger
//     12636, // The Eye Of Acherus
//     12641, // Death Comes From On High
//     12657, // The Might Of The Scourge
//     12850, // Report To Scourge Commander Thalanor
//     12670, // The Scarlet Harvest
//     12678, // If Chaos Drives, Let Suffering Hold The Reins
//     12697, // Gothik the Harvester
//     12698, // The Gift That Keeps On Giving
//     12700, // An Attack Of Opportunity
//     12701, // Massacre At Light\'s Point
//     12706, // Victory At Death\'s Breach!
//     12714, // The Will Of The Lich King
//     12715, // The Crypt of Remembrance
//     12719, // Nowhere To Run And Nowhere To Hide
//     12720, // How To Win Friends And Influence Enemies
//     12723, // Behind Scarlet Lines
//     12724, // The Path Of The Righteous Crusader
//     12725, // Brothers In Death
//     12727, // Bloody Breakout
//     12738, // A Cry For Vengeance!
//     12751, // A Sort Of Homecoming
//     12754, // Ambush At The Overlook
//     12755, // A Meeting With Fate
//     12756, // The Scarlet Onslaugh Emerges
//     12757, // Scarlet Armies Approach
//     12778, // An End To All Things...
//     12800, // The Lich King\'s Command
//     12801, // The Light of Dawn
//     13165, // Taking Back Acherus
//     13166, // The Battle For The Ebon Hold
// };

// 25-[url=http://www.wowhead.com/?search=A+special+sur]A Special Surprise[/url] (Race/faction based)
// 36-[url=http://www.wowhead.com/?quest=13188]Where Kings Walk[/url] (alliance)
// [url=http://www.wowhead.com/?quest=13189]Warchief\'s Blessing[/url] (horde)

class item_donate_level_80 : public ItemScript
{
public:
    item_donate_level_80(const char* scriptName) : ItemScript(scriptName) { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override
    {
        if (player->getLevel() < sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL))
        {
            player->GiveLevel(sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL));
            player->InitTalentForLevel();
            player->SetUInt32Value(PLAYER_XP, 0);
            //player->DestroyItemCount(m_item, 1, true);
        }

        return true;
    }
};

class item_donate_level_80_dk : public item_donate_level_80
{
public:
    item_donate_level_80_dk() : item_donate_level_80("item_donate_level_80_dk") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& targets) override
    {
        if (!item_donate_level_80::OnUse(player, item, targets))
            return false;

        // for (auto quest : dkQuests)
        // {
        //     ;
        // }

        // player->m_Events.AddEvent(new GiveMaxLevelEvent(player, item->GetEntry()), player->m_Events.CalculateTime(1 * IN_MILLISECONDS));
        return true;
    }
};

template<uint32 SpellId>
class item_donate_profession : public ItemScript
{
public:
    item_donate_profession(const char* scriptName) : ItemScript(scriptName) { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override
    {
        SpellInfo const* spell = sSpellMgr->GetSpellInfo(SpellId);
        if (!spell || !spell->IsProfession())
            return true;

        uint32 skillId = 0;
        for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
            if (spell->Effects[i].Effect == SPELL_EFFECT_SKILL)
                skillId = spell->Effects[i].MiscValue;

        if (!skillId)
            return true;

        if (!spell->IsPrimaryProfession() || player->GetFreePrimaryProfessionPoints())
        {
            bool done = false;
            if (!player->HasSpell(SpellId))
            {
                player->LearnSpell(SpellId, false);
                done = true;
            }

            if (!player->HasSkill(skillId) || player->GetPureSkillValue(skillId) < 450)
            {
                player->SetSkill(skillId, player->GetSkillValue(skillId) != 0 ? player->GetSkillStep(skillId) : 1, 450, 450);
                done = true;
            }

            if (done)
                player->DestroyItemCount(item->GetEntry(), 1, true);
            else
                ChatHandler(player->GetSession()).PSendSysMessage("You already have this profession at max skill.");
        }
        else
            ChatHandler(player->GetSession()).PSendSysMessage("You've reached your primary profession limit.");

        return true;
    }
};

void AddSC_donate_commands()
{
    new donate_commands();
    new item_donate_level_80("item_donate_level_80");
    new item_donate_profession<51304>("item_donate_profession_alchemy");
    new item_donate_profession<51300>("item_donate_profession_blacksmithing");
    new item_donate_profession<51313>("item_donate_profession_enchanting");
    new item_donate_profession<51306>("item_donate_profession_engineering");
    new item_donate_profession<50300>("item_donate_profession_herbalism");
    new item_donate_profession<45363>("item_donate_profession_inscription");
    new item_donate_profession<51311>("item_donate_profession_jewelcrafting");
    new item_donate_profession<51302>("item_donate_profession_leatherworking");
    new item_donate_profession<50310>("item_donate_profession_mining");
    new item_donate_profession<50305>("item_donate_profession_skinning");
    new item_donate_profession<51309>("item_donate_profession_tailoring");
    new item_donate_profession<51296>("item_donate_profession_cooking");
    new item_donate_profession<45542>("item_donate_profession_first_aid");
    new item_donate_profession<51294>("item_donate_profession_fishing");
}
