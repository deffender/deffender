#include "Battleground.h"
#include "Pet.h"
#include "Player.h"
#include "ScriptMgr.h"

struct RemoveDementia : public PlayerScript
{
    RemoveDementia() : PlayerScript("RemoveDementia") {}

    void OnUpdateZone(Player* player, uint32 /*newZone*/, uint32 /*newArea*/)
    {
        DoRemove(player);
    }

    void OnLogin(Player* player, bool /* OnLogin */)
    {
        DoRemove(player);
    }

private:
	inline void DoRemove(Player* player)
	{
		if (Aura* dementia = player->GetAura(DEMENTIA_SPELL))
            player->RemoveAura(dementia);

        if (Pet* pet = player->GetPet())
            if (Aura* dementia = pet->GetAura(DEMENTIA_SPELL))
                pet->RemoveAura(dementia);
	}
};

void AddSC_remove_dementia()
{
    new RemoveDementia();
}
