#include "AccountPointsMgr.h"
#include "Chat.h"
#include "Creature.h"
#include "DeffenderVendorMgr.h"
#include "Item.h"
#include "Log.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "World.h"
#include "WorldSession.h"

#define RELOAD_ACTION 1000000
#define PAGE_ACTION   1100000
#define BACK_MENU_ADD (2 << 16)
#define PER_PAGE 10

struct npc_deffender_vendor : public ScriptedAI
{
    npc_deffender_vendor(Creature* creature) : ScriptedAI(creature) { }

    bool CanShow(Player* plr)
    {
        if (plr->IsInCombat())
        {
            plr->GetSession()->SendNotification("You are in combat!");
            CloseGossipMenuFor(plr);
            return false;
        }
        else if (!sDeffVendorMgr->IsLoaded())
        {
            plr->GetSession()->SendNotification("Vendor reloading, please try again in a second.");
            CloseGossipMenuFor(plr);
            return false;
        }
        else
            return sDeffVendorMgr->HasData(me->GetEntry());
    }

    void ShowMenu(Player* plr, uint32 menu_id, uint32 action)
    {
        bool next_page = false;
        uint32 count = 0;
        uint32 page = (action >= PAGE_ACTION) ? (action - PAGE_ACTION) : 0;

        auto thisData = sDeffVendorMgr->GetData(menu_id);
        if (menu_id && thisData.pointsType == ACCOUNT_POINTS_MAX)
        {
            CloseGossipMenuFor(plr);
            return;
        }

        if (!menu_id && plr->IsGameMaster())
        {
            ++count;
            AddGossipItemFor(plr, GOSSIP_ICON_INTERACT_2, "~RELOAD VENDOR DATA~\n", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + RELOAD_ACTION);
        }

        auto data = sDeffVendorMgr->GetItems(me->GetEntry(), menu_id);
        for (auto& entry : data)
        {
            if (entry.second.faction && entry.second.faction != plr->GetTeam())
                continue;

            if (count++ < page * PER_PAGE)
                continue;

            if (count > (page + 1) * PER_PAGE)
            {
                next_page = true;
                break;
            }

            if (!entry.second.price)
                AddGossipItemFor(plr, entry.second.icon, entry.second.name, GOSSIP_SENDER_MAIN + entry.first, GOSSIP_ACTION_INFO_DEF);
            else
            {
                std::stringstream ss;
                if (entry.second.count > 1)
                    ss << entry.second.count << "x ";

                if (entry.second.name.size())
                    ss << entry.second.name;
                else
                    ss << entry.second.item->Name1;
                ss  << " ["
                    << entry.second.price
                    << " "
                    << AccountPointsNames[entry.second.pointsType]
                    << "s]";

                AddGossipItemFor(plr, entry.second.icon, ss.str(), GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + entry.first);
            }
        }

        if (page)
            AddGossipItemFor(plr, GOSSIP_ICON_INTERACT_1, "<< Previous page", GOSSIP_SENDER_MAIN + menu_id, GOSSIP_ACTION_INFO_DEF + PAGE_ACTION + page - 1);

        if (next_page)
            AddGossipItemFor(plr, GOSSIP_ICON_INTERACT_1, ">> Next page", GOSSIP_SENDER_MAIN + menu_id, GOSSIP_ACTION_INFO_DEF + PAGE_ACTION + page + 1);

        if (menu_id)
            AddGossipItemFor(plr, GOSSIP_ICON_INTERACT_1, "Back", GOSSIP_SENDER_MAIN + BACK_MENU_ADD + thisData.parentId, GOSSIP_ACTION_INFO_DEF);

        SendGossipMenuFor(plr, 1, me);
    }

    bool HandleMenuAction(Player* plr, uint32 action)
    {
        auto data = sDeffVendorMgr->GetData(action);
        if (data.pointsType == ACCOUNT_POINTS_MAX || !data.item)
            return false;

        AccountPointsInfo pinfo;
        if (!sAccountPointsMgr->GetPoints(data.pointsType, plr->GetSession()->GetAccountId(), pinfo) || pinfo.points < data.price)
        {
            GetHandler(plr).PSendSysMessage("You need %u %ss to buy this item (you have %u).", data.price, AccountPointsNames[data.pointsType], pinfo.points);
            return false;
        }

        ItemPosCountVec dest;
        InventoryResult msg = plr->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, data.item->ItemId, data.count);
        if (msg != EQUIP_ERR_OK || dest.empty())
        {
            GetHandler(plr).PSendSysMessage("You do not have enough bag slots to store this item, please make some room and try again.");
            return false;
        }

        Item* item = plr->StoreNewItem(dest, data.item->ItemId, true, GenerateItemRandomPropertyId(data.item->ItemId));
        if (item)
            plr->SendNewItem(item, data.count, true, true);

        sAccountPointsMgr->SetPoints(data.pointsType, plr->GetSession()->GetAccountId(), pinfo.points - data.price);
        GetHandler(plr).PSendSysMessage("You have successfully bought %ux %s!", data.count, data.name.size() ? data.name : (data.item ? data.item->Name1 : ""));
        CloseGossipMenuFor(plr);
        return false;
    }

    bool GossipHello(Player* plr) override
    {
        if (CanShow(plr))
            ShowMenu(plr, 0, 0);

        return true;
    }

    bool GossipSelect(Player* plr, uint32 sender, uint32 action) override
    {
        if (!CanShow(plr))
            return true;

        sender = plr->PlayerTalkClass->GetGossipOptionSender(action) - GOSSIP_SENDER_MAIN;
        action = GetGossipActionFor(plr, action) - GOSSIP_ACTION_INFO_DEF;
        ClearGossipMenuFor(plr);

        if (sender >= BACK_MENU_ADD)
            sender -= BACK_MENU_ADD;

        if (action == RELOAD_ACTION && plr->IsGameMaster())
        {
            sDeffVendorMgr->Reload();
            CloseGossipMenuFor(plr);
            plr->GetSession()->SendNotification("Vendor reloaded");
            return true;
        }

        if (action == 0 || action >= PAGE_ACTION)
            ShowMenu(plr, sender, action);
        else if (!HandleMenuAction(plr, action))
            CloseGossipMenuFor(plr);

        return true;
    }

private:
    inline ChatHandler GetHandler(Player* plr)
    {
        return ChatHandler(plr->GetSession());
    }
};

struct ws_deffender_vendor : public WorldScript
{
    ws_deffender_vendor() : WorldScript("ws_deffender_vendor") { }

    void OnStartup() override
    {
        TC_LOG_INFO("server.loading", "Loading Deffender Vendor data...");
        sDeffVendorMgr->Load();
    }
};

void AddSC_npc_deffender_vendor()
{
    RegisterCreatureAI(npc_deffender_vendor);
    new ws_deffender_vendor();
}
