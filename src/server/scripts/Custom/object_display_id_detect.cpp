// #include "Player.h"
// #include "ScriptMgr.h"
// #include "GameObjectAI.h"
// #include "ScriptedGossip.h"
// #include "WorldSession.h"

// class object_display_id_detect : public GameObjectScript
// {
// public:
//     object_display_id_detect() : GameObjectScript("object_display_id_detect") { }

//     bool OnGossipHello(Player* player, GameObject* go) override
//     {
//         PlayerMenu* ptc = player->PlayerTalkClass;
//         GossipMenu& menu = ptc->GetGossipMenu();
//         menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "next id", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "", 0);
//         if (go->GetDisplayId() > 1)
//             menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "previous id", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2, "", 0);
//         ptc->SendGossipMenu(1, go->GetGUID());
//         return true;
//     }

//     bool OnGossipSelect(Player* player, GameObject* go, uint32 /*sender*/, uint32 action) override
//     {
//         go->SetFlag(GAMEOBJECT_FLAGS, GO_FLAG_DESTROYED);
//         go->RemoveFlag(GAMEOBJECT_FLAGS, GO_FLAG_DESTROYED);

//         switch (action)
//         {
//         case GOSSIP_ACTION_INFO_DEF + 1:
//             go->SetDisplayId(go->GetDisplayId() + 1);
//             break;
//         case GOSSIP_ACTION_INFO_DEF + 2:
//             go->SetDisplayId(go->GetDisplayId() - 1);
//             break;
//         }

//         player->GetSession()->SendNotification("Display ID changed to %u!", go->GetDisplayId());
//         PlayerMenu* ptc = player->PlayerTalkClass;
//         GossipMenu& menu = ptc->GetGossipMenu();
//         menu.ClearMenu();
//         menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "next id", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1, "", 0);
//         if (go->GetDisplayId() > 1)
//             menu.AddMenuItem(-1, GOSSIP_ICON_CHAT, "previous id", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2, "", 0);
//         ptc->SendGossipMenu(1, go->GetGUID());
//         return true;
//     }

//     struct object_display_id_detectAI : public GameObjectAI
//     {
//         uint32 timer;
//         object_display_id_detectAI(GameObject* go) : GameObjectAI(go)
//         {
            
//         }

//         void UpdateAI(uint32 /*diff*/) override
//         {

//         }
//     };

//     GameObjectAI* GetAI(GameObject* go) const override
//     {
//         return new object_display_id_detectAI(go);
//     }
// };

// void AddSC_object_display_id_detect()
// {
//     new object_display_id_detect();
// }
