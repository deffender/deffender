#include "AccountPointsMgr.h"
#include "Common.h"
#include "Creature.h"
#include "DynamicTeleporterMgr.h"
#include "GameEventMgr.h"
#include "Log.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "World.h"
#include "WorldSession.h"

#define RELOAD_ACTION 666

struct npc_dynamic_teleporter : public ScriptedAI
{
    npc_dynamic_teleporter(Creature* creature) : ScriptedAI(creature) { }

    bool GossipHello(Player* plr) override
    {
        if (CanShow(plr))
            ShowMenu(plr, 0);

        return true;
    }

    bool GossipSelect(Player* plr, uint32 sender, uint32 action) override
    {
        if (!CanShow(plr))
            return true;

        sender = plr->PlayerTalkClass->GetGossipOptionSender(action) - GOSSIP_SENDER_MAIN;
        action = GetGossipActionFor(plr, action) - GOSSIP_ACTION_INFO_DEF;
        ClearGossipMenuFor(plr);
        if (action == RELOAD_ACTION && plr->IsGameMaster())
        {
            sTeleporterMgr->Reload();
            plr->PlayerTalkClass->SendCloseGossip();
            plr->GetSession()->SendNotification("Teleporter reloaded");
            me->UpdateObjectVisibility();
            return true;
        }

        if (!HandleMenuAction(plr, sender, action))
            plr->PlayerTalkClass->SendCloseGossip();

        return true;
    }

private:
    inline bool CanShow(Player* plr)
    {
        if (plr->IsInCombat())
        {
            plr->GetSession()->SendNotification("You are in combat!");
            plr->PlayerTalkClass->SendCloseGossip();
            return false;
        }
        else if (!sTeleporterMgr->IsLoaded())
        {
            plr->GetSession()->SendNotification("Teleporter reloading, please try again in a second.");
            plr->PlayerTalkClass->SendCloseGossip();
            return false;
        }
        else if (!CheckVisibility(plr, sTeleporterMgr->GetMinVisibility()))
        {
            switch (sTeleporterMgr->GetMinVisibility())
            {
                case MENU_VISIBILITY_VOTE:
                    plr->GetSession()->SendNotification("Please vote for us on our website to be able to use teleporter.");;
                    break;
                case MENU_VISIBILITY_DONATE:
                    plr->GetSession()->SendNotification("We're sorry, but you have to be donator to use teleporter.");;
                    break;
            }

            plr->PlayerTalkClass->SendCloseGossip();
            return false;
        }
        else
            return true;
    }

    void ShowMenu(Player* plr, uint32 menu_id)
    {
        uint32 count = 0;
        uint32 send_counter = 0;

        if (!menu_id && plr->IsGameMaster())
        {
            ++count;
            ++send_counter;
            AddGossipItemFor(plr, 0, "~RELOAD TELEPORTER DATA~\n", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + RELOAD_ACTION);
        }

        DynamicTeleporterMgr::SharedLock guard(sTeleporterMgr->GetLock());
        for (auto& td : sTeleporterMgr->GetSubmenuData(menu_id))
        {
            if ((td.realmId && td.realmId != plr->GetRealmId()) || (td.faction && td.faction != plr->GetTeam()))
                continue;

            if (td.questId && plr->GetQuestStatus(td.questId) != QUEST_STATUS_REWARDED)
                continue;

            if (td.eventId)
            {
                if (td.eventId > 0)
                {
                    if (!sGameEventMgr->IsActiveEvent(td.eventId))
                        continue;
                }
                else if (sGameEventMgr->IsActiveEvent(td.eventId))
                    continue;
            }

            if (!CheckVisibility(plr, td.visibility))
                continue;

            AddGossipItemFor(plr, td.icon, td.name, GOSSIP_SENDER_MAIN + menu_id, GOSSIP_ACTION_INFO_DEF + td.entry);
            ++count;
            ++send_counter;
            if (send_counter >= 10)
            {
                plr->PlayerTalkClass->SendGossipMenu(1, me->GetGUID());
                send_counter = 0;
            }
        }

        if (send_counter || !count)
            SendGossipMenuFor(plr, 1, me);
    }

    bool HandleMenuAction(Player* plr, uint32 sender, uint32 action)
    {
        DynamicTeleporterMgr::SharedLock guard(sTeleporterMgr->GetLock());
        if (!sTeleporterMgr->HasSubmenu(sender))
            return false;

        for (auto& td : sTeleporterMgr->GetSubmenuData(sender))
        {
            if (td.entry == action)
            {
                if (td.menu_sub >= 0)
                    ShowMenu(plr, td.menu_sub);
                else if (auto loc = sTeleporterMgr->FindLocation(action))
                    plr->TeleportTo(*loc);

                return true;
            }
        }

        return false;
    }

    inline bool CheckVisibility(const Player* player, uint8 visTarget)
    {
        if (player->IsGameMaster())
            return true;

        switch (visTarget)
        {
            case MENU_VISIBILITY_ALL: return true;
            case MENU_VISIBILITY_VOTE:
            {
                AccountPointsInfo info;
                return  (sAccountPointsMgr->GetPoints(ACCOUNT_POINTS_VOTE, player->GetSession()->GetAccountId(), info)
                        && (info.lastChanged + 12 * HOUR) >= GameTime::GetGameTime())
                        || CheckVisibility(player, visTarget + 1);
            }
            case MENU_VISIBILITY_DONATE: return player->GetSession()->GetSecurity() >= SEC_MODERATOR;
            default: return false;
        }
    }
};

struct ws_dynamic_teleporter : public WorldScript
{
    ws_dynamic_teleporter() : WorldScript("ws_dynamic_teleporter") { }

    void OnStartup() override
    {
        TC_LOG_INFO("server.loading", "Loading Dynamic Teleporter...");
        sTeleporterMgr->Load();
    }
};

void AddSC_npc_dynamic_teleporter()
{
    RegisterCreatureAI(npc_dynamic_teleporter);
    new ws_dynamic_teleporter();
}
