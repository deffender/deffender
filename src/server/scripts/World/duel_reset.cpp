/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "GameTime.h"
#include "Pet.h"
#include "Player.h"
#include "Unit.h"
#include "SpellHistory.h"
#include "SpellInfo.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "SpellMgr.h"
#include "World.h"

class DuelResetScript : public PlayerScript
{
    public:
        DuelResetScript() : PlayerScript("DuelResetScript") { }

        // Called when a duel starts (after 3s countdown)
        void OnDuelStart(Player* player1, Player* player2) override
        {
            // Cooldowns reset
            if (sWorld->getBoolConfig(CONFIG_RESET_DUEL_COOLDOWNS))
            {
                player1->GetSpellHistory()->SaveCooldownStateBeforeDuel();
                player2->GetSpellHistory()->SaveCooldownStateBeforeDuel();

                ResetSpellCooldowns(player1, true);
                ResetSpellCooldowns(player2, true);
            }

            // Health and mana reset
            if (sWorld->getBoolConfig(CONFIG_RESET_DUEL_HEALTH_MANA))
            {
                player1->SaveHealthBeforeDuel();
                player1->SetHealth(player1->GetMaxHealth());

                player2->SaveHealthBeforeDuel();
                player2->SetHealth(player2->GetMaxHealth());

                // check if player1 class uses mana
                if (player1->getPowerType() == POWER_MANA || player1->getClass() == CLASS_DRUID)
                {
                    player1->SaveManaBeforeDuel();
                    player1->SetPower(POWER_MANA, player1->GetMaxPower(POWER_MANA));
                }

                // check if player2 class uses mana
                if (player2->getPowerType() == POWER_MANA || player2->getClass() == CLASS_DRUID)
                {
                    player2->SaveManaBeforeDuel();
                    player2->SetPower(POWER_MANA, player2->GetMaxPower(POWER_MANA));
                }
            }
        }

        // Called when a duel ends
        void OnDuelEnd(Player* winner, Player* loser, DuelCompleteType type) override
        {
            // do not reset anything if DUEL_INTERRUPTED or DUEL_FLED
            if (type == DUEL_WON)
            {
                // Cooldown restore
                if (sWorld->getBoolConfig(CONFIG_RESET_DUEL_COOLDOWNS))
                {
                    ResetSpellCooldowns(winner, false);
                    ResetSpellCooldowns(loser, false);

                    winner->GetSpellHistory()->RestoreCooldownStateAfterDuel();
                    loser->GetSpellHistory()->RestoreCooldownStateAfterDuel();
                }

                // Health and mana restore
                if (sWorld->getBoolConfig(CONFIG_RESET_DUEL_HEALTH_MANA))
                {
                    winner->RestoreHealthAfterDuel();
                    loser->RestoreHealthAfterDuel();

                    // check if player1 class uses mana
                    if (winner->getPowerType() == POWER_MANA || winner->getClass() == CLASS_DRUID)
                        winner->RestoreManaAfterDuel();

                    // check if player2 class uses mana
                    if (loser->getPowerType() == POWER_MANA || loser->getClass() == CLASS_DRUID)
                        loser->RestoreManaAfterDuel();
                }
            }

            if (winner->duel->flags & DUEL_RATED)
            {
                // Remove possible duel auras - Positive Charge, Shackle, Arena Preparation
                for (auto iter = winner->GetAppliedAuras().begin(); iter != winner->GetAppliedAuras().end();)
                {
                    Aura const* aura = iter->second->GetBase();
                    switch (aura->GetId())
                    {
                    case 29659:
                    case 38505:
                    case 32728:
                        winner->RemoveAura(iter);
                        iter = winner->GetAppliedAuras().begin();
                        break;
                    default:
                        ++iter;
                        break;
                    }
                }

                for (auto iter = loser->GetAppliedAuras().begin(); iter != loser->GetAppliedAuras().end();)
                {
                    Aura const* aura = iter->second->GetBase();
                    switch (aura->GetId())
                    {
                    case 29659:
                    case 38505:
                    case 32728:
                        loser->RemoveAura(iter);
                        iter = loser->GetAppliedAuras().begin();
                        break;
                    default:
                        ++iter;
                        break;
                    }
                }

                // Remove despair
                winner->RemoveOwnedAura(62692);
                loser->RemoveOwnedAura(62692);

                // remove free and instant spells
                winner->SetCommandStatusOff(CHEAT_POWER);
                loser->SetCommandStatusOff(CHEAT_POWER);
                winner->SetCommandStatusOff(CHEAT_CASTTIME);
                loser->SetCommandStatusOff(CHEAT_CASTTIME);

                // Port Back & Freeze
                if (type == DUEL_WON)
                {
                    Position p1 = winner->duel->savedPos;
                    Position p2 = loser->duel->savedPos;
                    winner->TeleportTo(0, p1.m_positionX, p1.m_positionY, p1.m_positionZ, p1.GetOrientation());
                    if (loser->duel->flags & DUEL_LOSER)
                    {
                        loser->AddAura(38505, loser); // shackle
                        winner->AddAura(9454, winner); // freeze
                        loser->TeleportTo(0, -1067, 1478, 59.77, 3.0);
                        return;
                    }

                    winner->AddAura(9454, winner); // freeze
                    loser->AddAura(9454, loser); // freeze
                    loser->TeleportTo(0, p2.m_positionX + 2, p2.m_positionY, p2.m_positionZ, p2.GetOrientation());
                }
            }
        }

        // On Zone Change
        void OnUpdateZone(Player* player, uint32 newZone, uint32 newArea) 
        { 
            if (newArea != 2397) // duel arena - the great sea
            {
                player->RemoveAura(38505); //Shackle
                player->RemoveAura(29659); //Dmg buff
            }
        }

        static void ResetSpellCooldowns(Player* player, bool onStartDuel)
        {
            if (player->GetAreaId() != 2251  //  duel zone
                && player->GetAreaId() != 2397  //  event zone - the great sea
                && player->GetAreaId() != 2317) // trade zone
                return;

            if (onStartDuel)
            {
                // remove cooldowns on spells that have < 10 min CD > 30 sec and has no onHold
                player->GetSpellHistory()->ResetCooldowns([](SpellHistory::CooldownStorageType::iterator itr) -> bool
                {
                    SpellHistory::Clock::time_point now = GameTime::GetGameTimeSystemPoint();
                    uint32 cooldownDuration = static_cast<uint32>(itr->second.CooldownEnd > now ? std::chrono::duration_cast<std::chrono::milliseconds>(itr->second.CooldownEnd - now).count() : 0);
                    SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(itr->first);
                    return spellInfo->RecoveryTime < 10 * MINUTE * IN_MILLISECONDS
                           && spellInfo->CategoryRecoveryTime < 10 * MINUTE * IN_MILLISECONDS
                           && !itr->second.OnHold
                           && cooldownDuration > 0
                           && ( spellInfo->RecoveryTime - cooldownDuration ) > (MINUTE / 2) * IN_MILLISECONDS
                           && ( spellInfo->CategoryRecoveryTime - cooldownDuration ) > (MINUTE / 2) * IN_MILLISECONDS;
                }, true);

                player->RemoveAura(57724); // Remove Sated Debuff
                switch(player->getClass())
                {
                    case CLASS_MAGE:
                        player->RemoveAura(41425); // Hypothermia
                        break;
                    case CLASS_PALADIN:
                        player->RemoveAura(61987); // Avenging Wrath Marker
                        player->RemoveAura(61988); // Immune Shield Marker
                        player->RemoveAura(25771); // Forbearance
                        break;
                    case CLASS_DRUID:
                        player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA));
                        break;
                }
            }
            else
            {
                // remove cooldowns on spells that have < 10 min CD and has no onHold
                player->GetSpellHistory()->ResetCooldowns([](SpellHistory::CooldownStorageType::iterator itr) -> bool
                {
                    SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(itr->first);
                    return spellInfo->RecoveryTime < 10 * MINUTE * IN_MILLISECONDS
                           && spellInfo->CategoryRecoveryTime < 10 * MINUTE * IN_MILLISECONDS
                           && !itr->second.OnHold;
                }, true);
            }

            // pet cooldowns
            if (Pet* pet = player->GetPet())
                pet->GetSpellHistory()->ResetAllCooldowns();
        }
};

void AddSC_duel_reset()
{
    new DuelResetScript();
}

