/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_dk_".
 */

#include "CellImpl.h"
#include "CombatAI.h"
#include "GridNotifiersImpl.h"
#include "MotionMaster.h"
#include "PetAI.h"
#include "ScriptedCreature.h"
#include "ScriptMgr.h"

enum DeathKnightSpells
{
    SPELL_DK_SUMMON_GARGOYLE_1      = 49206,
    SPELL_DK_SUMMON_GARGOYLE_2      = 50514,
    SPELL_DK_DISMISS_GARGOYLE       = 50515,
    SPELL_DK_SANCTUARY              = 54661,
    SPELL_GARGOYLE_STRIKE           = 51963,
};

#define POINT_INIT 666

struct npc_pet_dk_ebon_gargoyle : PetAI
{
    npc_pet_dk_ebon_gargoyle(Creature* creature) : PetAI(creature)
    {
        Initialize();
    }

    void Initialize()
    {
        me->InitCharmInfo();
        Unit* owner = me->GetOwner();
        if (!owner)
            return;

        Position flyPos(*owner);
        landPos.Relocate(*owner);
        flyPos.m_positionZ += 15.0f;

        if (!owner->IsWithinLOS(flyPos.m_positionX, flyPos.m_positionY, flyPos.m_positionZ))
        {
            // directly up is not within LoS, try to find somewhat near position that is
            bool found = false;
            float px, py;
            for (uint8 i = 0; i < 5; ++i)
            {
                addOffset(i, flyPos.m_positionX, px);
                for (uint8 j = 0; j < 5; ++j)
                {
                    addOffset(j, flyPos.m_positionY, py);
                    if (owner->IsWithinLOS(px, py, flyPos.m_positionZ))
                    {
						flyPos.Relocate(px, py);
                        found = true;
                        break;
                    }
                }
            }

            if (found)
                me->Relocate(flyPos);
            else
                // if we didn't find any suitable position, just teleport onto owner
                me->Relocate(*owner);
        }
		else
			me->Relocate(flyPos);

        me->SetCanFly(true);
        me->GetMotionMaster()->MovePoint(POINT_INIT, landPos, false);
        me->SetReactState(REACT_PASSIVE);
        init = false;
    }

    void MovementInform(uint32 type, uint32 id) override
    {
        // start fight instantly on land
        if (type == POINT_MOTION_TYPE && id == POINT_INIT)
            StartFight();
        else
            PetAI::MovementInform(type, id);
    }

    void AttackStart(Unit* victim) override
    {
        AttackStartCaster(victim, 40.0f);
    }

    void JustDied(Unit* /*killer*/) override
    {
        // Stop Feeding Gargoyle when it dies
        if (Unit* owner = me->GetOwner())
            owner->RemoveAurasDueToSpell(SPELL_DK_SUMMON_GARGOYLE_2);
    }

    // Fly away when dismissed
    void SpellHit(Unit* caster, SpellInfo const* spell) override
    {
        PetAI::SpellHit(caster, spell);
        if (spell->Id != SPELL_DK_DISMISS_GARGOYLE || !me->IsAlive())
            return;

        Unit* owner = me->GetOwner();
        if (!owner || owner != caster)
            return;

        me->DeleteCharmInfo();

        // Stop Fighting
        me->ApplyModFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE, true);

        // Sanctuary
        me->CastSpell(me, SPELL_DK_SANCTUARY, true);
        me->SetReactState(REACT_PASSIVE);

        //! HACK: Creature's can't have MOVEMENTFLAG_FLYING
        // Fly Away
        me->SetCanFly(true);
        me->AddUnitState(UNIT_STATE_IGNORE_PATHFINDING);
        me->SetSpeedRate(MOVE_FLIGHT, 0.75f);
        me->SetSpeedRate(MOVE_RUN, 0.75f);
        float x = me->GetPositionX() + 20 * std::cos(me->GetOrientation());
        float y = me->GetPositionY() + 20 * std::sin(me->GetOrientation());
        float z = me->GetPositionZ() + 40;
        me->DespawnOrUnsummon(Seconds(4));
        me->RemoveAllAuras();
        me->GetMotionMaster()->MovePoint(0, x, y, z, false);
        // disable default AI
        me->IsAIEnabled = false;
    }

    void UpdateAI(uint32 diff) override
    {
        if (init)
        {
            if (!me->GetVictim())
            {
                if (!me->isMoving())
                    HandleReturnMovement();

                return;
            }

            events.Update(diff);

            if (me->EnsureVictim()->HasBreakableByDamageCrowdControlAura(me))
            {
                me->InterruptNonMeleeSpells(false);
                return;
            }

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            if (uint32 spellId = events.ExecuteEvent())
            {
                DoCast(spellId);
                uint32 casttime = me->GetCurrentSpellCastTime(spellId);
                events.ScheduleEvent(spellId, casttime);
            }
        }
        else if (!me->isMoving())
            me->GetMotionMaster()->MovePoint(POINT_INIT, landPos, false);
    }

private:
    bool init;
    Position landPos;
    EventMap events;

    inline void addOffset(uint8 n, float base, float& where)
    {
        switch (n)
        {
            case 0: where = base; break;
            case 1: where = base - 5.0f; break;
            case 2: where = base + 5.0f; break;
            case 3: where = base - 10.0f; break;
            case 4: where = base + 10.0f; break;
        }
    }

    inline void StartFight()
    {
        me->SetCanFly(false);
        me->SetReactState(REACT_AGGRESSIVE);
        ObjectGuid ownerGuid = me->GetOwnerGUID();
        if (!ownerGuid)
            return;

        // Find victim of Summon Gargoyle spell
        std::list<Unit*> targets;
        Trinity::AnyUnfriendlyUnitInObjectRangeCheck u_check(me, me, 30.0f);
        Trinity::UnitListSearcher<Trinity::AnyUnfriendlyUnitInObjectRangeCheck> searcher(me, targets, u_check);
        Cell::VisitAllObjects(me, searcher, 30.0f);
        for (std::list<Unit*>::const_iterator iter = targets.begin(); iter != targets.end(); ++iter)
        {
            if ((*iter)->HasAura(SPELL_DK_SUMMON_GARGOYLE_1, ownerGuid))
            {
                AttackStart(*iter);
                break;
            }
        }

        events.ScheduleEvent(SPELL_GARGOYLE_STRIKE, 1);
        init = true;
    }
};

struct npc_pet_dk_guardian : public AggressorAI
{
    npc_pet_dk_guardian(Creature* creature) : AggressorAI(creature) { }

    bool CanAIAttack(Unit const* target) const override
    {
        if (!target)
            return false;

        Unit* owner = me->GetOwner();
        if (owner && !target->IsInCombatWith(owner))
            return false;

        return AggressorAI::CanAIAttack(target);
    }
};

void AddSC_deathknight_pet_scripts()
{
    RegisterCreatureAI(npc_pet_dk_ebon_gargoyle);
    RegisterCreatureAI(npc_pet_dk_guardian);
}
