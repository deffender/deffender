/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBSIDIAN_SANCTUM_H_
#define OBSIDIAN_SANCTUM_H_

#include "CreatureAIImpl.h"
#include "InstanceScript.h"

#define OSScriptName "instance_obsidian_sanctum"
#define DataHeader "OS"

class instance_obsidian_sanctum;

uint32 const EncounterCount = 4;

enum OBS_ID
{
    SPELL_FLAME_ORB_PERIODIC                = 57750,
    SPELL_SUMMON_FLAME_ORB                  = 57752,
    SPELL_BALL_OF_FLAMES_VISUAL             = 71706, // from ICC's blood prince's orb, couldn't find proper one

    //Mini bosses common spells
    SPELL_TWILIGHT_RESIDUE                  = 61885,    // makes immune to shadow damage, applied when leave phase

                                       //Miniboses (Vesperon, Shadron, Tenebron)
    SPELL_SHADOW_BREATH                     = 57570,    // Inflicts 6938 to 8062 Fire damage to enemies in a cone in front of the caster.
    SPELL_SHADOW_FISSURE                    = 57579,    // Deals 6188 to 8812 Shadow damage to any enemy within the Shadow fissure after 5 sec.





    SPELL_TWILIGHT_SHIFT_AURA               = 57620,    // Twilight Shift Aura -> SHIFT DMG
    SPELL_TWILIGHT_SHIFT_DMG                = 57874,
    SPELL_TWILIGHT_SHIFT_REMOVAL            = 61187,    // leave phase
    SPELL_TWILIGHT_SHIFT_REMOVAL_ALL        = 61190,    // leave phase (probably version to make all leave)

    //Vesperon
    //In portal is a disciple, when disciple killed remove Power_of_vesperon, portal open multiple times
    NPC_ACOLYTE_OF_VESPERON                 = 31219,    // Acolyte of Vesperon
    SPELL_POWER_OF_VESPERON                 = 61251,    // Vesperon's presence decreases the maximum health of all enemies by 25%.
    SPELL_TWILIGHT_TORMENT                  = 58835,

    //SPELL_TWILIGHT_TORMENT_VESP             = 57948,    // (Shadow only) trigger 57935 then 57988
    //SPELL_TWILIGHT_TORMENT_VESP_ACO         = 58853,    // (Fire and Shadow) trigger 58835 then 57988
    //SPELL_TWILIGHT_TORMENT_VESP_AURA        = 57935,

    //Shadron
    //In portal is a disciple, when disciple killed remove Power_of_vesperon, portal open multiple times
    NPC_ACOLYTE_OF_SHADRON                  = 31218,    // Acolyte of Shadron
    SPELL_POWER_OF_SHADRON                  = 58105,    // Shadron's presence increases Fire damage taken by all enemies by 100%.
    SPELL_GIFT_OF_TWILIGTH_SHA              = 57835,    // TARGET_SCRIPT shadron
    SPELL_GIFT_OF_TWILIGTH_SAR              = 58766,    // TARGET_SCRIPT sartharion
    SPELL_VOID_BLAST                        = 57581,    // Twilight Fissure

    //Tenebron
    //in the portal spawns 6 eggs, if not killed in time (approx. 20s)  they will hatch,  whelps can cast 60708
    SPELL_POWER_OF_TENEBRON                 = 61248,    // Tenebron's presence increases Shadow damage taken by all enemies by 100%.
                                            //Tenebron, dummy spell
    SPELL_SUMMON_TWILIGHT_WHELP             = 58035,    // doesn't work, will spawn NPC_TWILIGHT_WHELP
    SPELL_SUMMON_SARTHARION_TWILIGHT_WHELP  = 58826,    // doesn't work, will spawn NPC_SHARTHARION_TWILIGHT_WHELP
    SPELL_TWILIGHT_REVENGE                  = 60639,
    SPELL_HATCH_EGGS_H                      = 59189,
    SPELL_HATCH_EGGS                        = 58542,
    SPELL_HATCH_EGGS_EFFECT_H               = 59190,
    SPELL_HATCH_EGGS_EFFECT                 = 58685,
    NPC_TWILIHT_WHELP                       = 31214,
    NPC_TWILIGHT_EGG                        = 30882,
    NPC_SARTHARION_TWILIGHT_EGG             = 31204,

    //Whelps
    NPC_TWILIGHT_WHELP                      = 30890,
    NPC_SARTHARION_TWILIGHT_WHELP           = 31214,
    SPELL_FADE_ARMOR                        = 60708,    // Reduces the armor of an enemy by 1500 for 15s

    //flame tsunami
    SPELL_FLAME_TSUNAMI                     = 57494,    // the visual dummy
    SPELL_FLAME_TSUNAMI_LEAP                = 60241,    // SPELL_EFFECT_138 some leap effect, causing caster to move in direction
    VOID_ZONE_PREVISUAL                     = 46265,    // ?? Wrong, can't find proper visual
    VOID_ZONE_VISUAL                        = 69422,

    SPELL_FLAME_TSUNAMI_DMG_AURA            = 57491,    // periodic damage, npc has this aura
    SPELL_FLAME_TSUNAMI_BUFF                = 60430,

    //using these custom points for dragons start and end
    POINT_ID_INIT                           = 100,
    POINT_ID_LAND                           = 200,

    // teleports
    TELEPORT_IN                             = 57620,
    TELEPORT_OUT                            = 61187
};

enum OSAchievementCriteria
{
    CRITERIA_OBSIDIAN_SLAYER = 5224
};

enum OSDataTypes
{
    BOSS_SARTHARION = 0,
    BOSS_TENEBRON = 1,
    BOSS_SHADRON = 2,
    BOSS_VESPERON = 3,
    TWILIGHT_ACHIEVEMENTS = 5,

    DATA_VESP_LOOT      = 7,
    DATA_SHAD_LOOT      = 8,
    DATA_TENE_LOOT      = 9,
    DATA_VESP_ACO       = 10,
    DATA_SHAD_ACO       = 11,
    DATA_TENE_EGG       = 12,
    DATA_PORTAL_OPEN    = 13,
    DATA_ALL            = 14,
    DATA_ACHIEV_ELIGIBLE= 15,

    FNC_DEL_SUMMONS     = 30,

    PROCEED             = 1,

    SPAWNED             = 1,
    NOT_SPAWNED         = 0,

    HAS_LOOT            = 1,
    NO_LOOT             = 0
};

enum OSCreaturesIds
{
    NPC_SARTHARION              = 28860,
    NPC_TENEBRON                = 30452,
    NPC_SHADRON                 = 30451,
    NPC_VESPERON                = 30449,
    NPC_LAVA_BLAZE              = 30643
};

enum OSGameObjectIds
{
    PORTAL_IN = 193988,
    PORTAL_OUT = 193989
};

template <class AI, class T>
inline AI* GetObsidianSanctumAI(T* obj)
{
    return GetInstanceAI<AI>(obj, OSScriptName);
}
#define RegisterObSCreatureAI(ai_name) RegisterCreatureAIWithFactory(ai_name, GetObsidianSanctumAI)

#endif // OBSIDIAN_SANCTUM_H_
