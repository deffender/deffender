/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "naxxramas.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"

enum Spells
{
    SPELL_BOMBARD_SLIME         = 28280,
    SPELL_SLIME_SPRAY           = 28157,
	SPELL_SLIME_SPRAY_HEROIC    = 54364,
    SPELL_MUTATING_INJECTION    = 28169,
    SPELL_MUTATING_EXPLOSION    = 28206,
    SPELL_POISON_CLOUD          = 28240,
    SPELL_POISON_CLOUD_PASSIVE  = 28158,
    SPELL_BERSERK               = 26662
};

enum CreatureId
{
    NPC_FALLOUT_SLIME           = 16290
};

struct boss_grobbulus : public BossAI
{
    boss_grobbulus(Creature* creature) : BossAI(creature, BOSS_GROBBULUS), _eventPause(false) { }

    void Reset() override
    {
        BossAI::Reset();
        _eventPause = false;
    }

    void ScheduleTasks() override
    {
        scheduler.Schedule(Minutes(12), [this](TaskContext /*berserk*/)
        {
            DoCastAOE(SPELL_BERSERK, true);
        })
        .Schedule(Seconds(15), [this](TaskContext cloud)
        {
            DoCastAOE(SPELL_POISON_CLOUD);
            cloud.Repeat(Seconds(15));
        })
        .Schedule(Seconds(15), Seconds(30), [this](TaskContext spray)
        {
            if (_eventPause)
            {
                spray.Repeat(Seconds(1));
                return;
            }

            _eventPause = true;
            spray.Schedule(Seconds(1), [this](TaskContext /**/)
            {
                _eventPause = false;
            });

            DoCastAOE(SPELL_SLIME_SPRAY);
            spray.Repeat(Seconds(15), Seconds(30));
        })
        .Schedule(Seconds(20), [this](TaskContext inject)
        {
            if (_eventPause)
            {
                inject.Repeat(Seconds(1));
                return;
            }

            _eventPause = true;
            inject.Schedule(Seconds(1), [this](TaskContext /**/)
            {
                _eventPause = false;
            });

            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 1, 0.0f, true, true, -SPELL_MUTATING_INJECTION))
                DoCast(target, SPELL_MUTATING_INJECTION);

            inject.Repeat(Seconds(8) + Milliseconds(uint32(std::round(120 * me->GetHealthPct()))));
        });
    }

    void SpellHitTarget(Unit* target, SpellInfo const* spell) override
    {
        if (spell->Id == SPELL_SLIME_SPRAY || spell->Id == SPELL_SLIME_SPRAY_HEROIC)
            me->SummonCreature(NPC_FALLOUT_SLIME, *target, TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        scheduler.Update(diff, [this] {
            DoMeleeAttackIfReady();
        });
    }

private:
    bool _eventPause;
};

struct npc_grobbulus_poison_cloud : public ScriptedAI
{
    npc_grobbulus_poison_cloud(Creature* creature) : ScriptedAI(creature)
    {
        SetCombatMovement(false);
        creature->SetReactState(REACT_PASSIVE);
    }

    void IsSummonedBy(Unit* /*summoner*/) override
    {
        // no visual when casting in ctor or Reset()
        DoCast(me, SPELL_POISON_CLOUD_PASSIVE, true);
    }

    void UpdateAI(uint32 /*diff*/) override { }
};

// 28169 - Mutating Injection
class spell_grobbulus_mutating_injection : public AuraScript
{
    PrepareAuraScript(spell_grobbulus_mutating_injection);

    bool Validate(SpellInfo const* /*spellInfo*/) override
    {
        return ValidateSpellInfo({ SPELL_MUTATING_EXPLOSION, SPELL_POISON_CLOUD });
    }

    void HandleRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        AuraRemoveMode removeMode = GetTargetApplication()->GetRemoveMode();
        if (removeMode != AURA_REMOVE_BY_ENEMY_SPELL && removeMode != AURA_REMOVE_BY_EXPIRE)
            return;

        if (Unit* caster = GetCaster())
        {
            caster->CastSpell(GetTarget(), SPELL_MUTATING_EXPLOSION, true);
            GetTarget()->CastSpell(GetTarget(), SPELL_POISON_CLOUD, true, nullptr, aurEff, GetCasterGUID());
        }
    }

    void Register() override
    {
        AfterEffectRemove += AuraEffectRemoveFn(spell_grobbulus_mutating_injection::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

// 28158, 54362 - Poison (Grobbulus)
class spell_grobbulus_poison_cloud : public AuraScript
{
    PrepareAuraScript(spell_grobbulus_poison_cloud);

    bool Validate(SpellInfo const* spellInfo) override
    {
        return ValidateSpellInfo({ spellInfo->Effects[EFFECT_0].TriggerSpell });
    }

    void PeriodicTick(AuraEffect const* aurEff)
    {
        PreventDefaultAction();

        uint32 triggerSpell = GetSpellInfo()->Effects[aurEff->GetEffIndex()].TriggerSpell;
        int32 mod = int32(((float(aurEff->GetTickNumber()) / aurEff->GetTotalTicks()) * 0.9f + 0.1f) * 10000 * 2 / 3);
        GetTarget()->CastCustomSpell(triggerSpell, SPELLVALUE_RADIUS_MOD, mod, (Unit*)nullptr, TRIGGERED_FULL_MASK, nullptr, aurEff);
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_grobbulus_poison_cloud::PeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
    }
};

void AddSC_boss_grobbulus()
{
    RegisterNaxxramasCreatureAI(boss_grobbulus);
    RegisterNaxxramasCreatureAI(npc_grobbulus_poison_cloud);
    RegisterAuraScript(spell_grobbulus_mutating_injection);
    RegisterAuraScript(spell_grobbulus_poison_cloud);
}
