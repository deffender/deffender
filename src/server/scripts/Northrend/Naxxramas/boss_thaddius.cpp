/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "EventProcessor.h"
#include "GameObject.h"
#include "InstanceScript.h"
#include "Map.h"
#include "naxxramas.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "SpellInfo.h"
#include "SpellScript.h"
#include <iostream>

enum Phases : uint8
{
    PHASE_NOT_ENGAGED,
    PHASE_PETS,
    PHASE_TRANSITION,
    PHASE_THADDIUS
};

enum AIActions
{
    ACTION_PET_REVIVED,
    ACTION_POLARITY_CROSSED // triggers achievement failure, sent from spellscript
};

enum Misc
{
    MAX_POLARITY_10M        = 5,
    MAX_POLARITY_25M        = 13,

    GROUP_PETS              = 1,

    DATA_POLARITY_CROSSED   = 1,

	THADDIUS_GROUP_STALAGG  = 1,
    THADDIUS_GROUP_FEUGEN   = 2,

	// pets
	OVERLOAD_DISTANCE       = 28,
};

enum PetTexts
{
    SAY_PET_AGGRO           = 0,
    SAY_PET_SLAY            = 1,
    SAY_PET_DEATH           = 2,

    EMOTE_FEIGN_DEATH       = 3,
    EMOTE_FEIGN_REVIVE      = 4,

    EMOTE_TESLA_LINK_BREAKS = 0,
    EMOTE_TESLA_OVERLOAD    = 1
};

enum ThaddiusTexts
{
    SAY_GREET               = 0,
    SAY_AGGRO               = 1,
    SAY_SLAY                = 2,
    SAY_ELECT               = 3,
    SAY_DEATH               = 4,
    SAY_SCREAM              = 5,

    EMOTE_POLARITY_SHIFTED  = 6
};

enum Spells
{
    SPELL_THADDIUS_INACTIVE         = 28160, // applied through creature_template_addon
    SPELL_THADDIUS_SPARK_VISUAL     = 28136,
    SPELL_SHOCK_VISUAL              = 28159,

    SPELL_BALL_LIGHTNING            = 28299, // wipe spell when noone is in melee range
    SPELL_CHAIN_LIGHTNING           = 28167,
    SPELL_BERSERK                   = 27680,

    SPELL_FEIGN_DEATH               = 29266,

    // polarity handling
    SPELL_POLARITY_SHIFT            = 28089,

    SPELL_POSITIVE_CHARGE_APPLY     = 28059,
    SPELL_POSITIVE_CHARGE_AMP       = 29659,

    SPELL_NEGATIVE_CHARGE_APPLY     = 28084,
    SPELL_NEGATIVE_CHARGE_AMP       = 29660,

    // pets
    SPELL_STALAGG_POWERSURGE        = 28134,
    SPELL_STALAGG_TESLA_PERIODIC    = 28098,
    SPELL_STALAGG_CHAIN_VISUAL      = 28096,

    SPELL_FEUGEN_STATICFIELD        = 28135,
    SPELL_FEUGEN_TESLA_PERIODIC     = 28110,
    SPELL_FEUGEN_CHAIN_VISUAL       = 28111,

    SPELL_MAGNETIC_PULL             = 54517,
    SPELL_MAGNETIC_PULL_EFFECT      = 28337,

    // @hack feugen/stalagg use this in P1 after gripping tanks to prevent mmaps from pathing them off the platform once they approach the ramp
    // developer from the future, if you read this and mmaps in the room has been fixed, then get rid of the hackfix, please
    SPELL_ROOT_SELF                 = 75215,

    SPELL_TESLA_SHOCK               = 28099 // wipe spell when pets are moved from platform
};

inline void CoilTalk(GameObject* go, uint32 text)
{
    if (go)
        if (Creature* coil = go->FindNearestCreature(NPC_TESLA, 10.0f, true))
            coil->AI()->Talk(text);
}

inline void CoilCast(GameObject* go, uint32 spellId, Unit* target = nullptr)
{
    if (go)
    {
        go->SetGoState(spellId ? GO_STATE_ACTIVE : GO_STATE_READY);
        if (Creature* coil = go->FindNearestCreature(NPC_TESLA, 10.0f, true))
        {
            if (spellId)
                coil->CastSpell(target, spellId, true);
            else
                coil->CastStop();
        }
    }
}

struct boss_thaddius : public BossAI
{
    boss_thaddius(Creature* creature) :
        BossAI(creature, BOSS_THADDIUS), _phase(PHASE_NOT_ENGAGED), _stalaggAlive(false), _feugenAlive(false),
        _ballLightningUnlocked(false), _ballLightningEnabled(false), _shockingEligibility(true)
    {
        SetCombatMovement(false);
        scheduler.SetValidator([this]
        {
            return _phase != PHASE_NOT_ENGAGED && (_phase != PHASE_THADDIUS || UpdateVictim());
        });
    }

    void JustAppeared() override
    {
        Reset();
    }

    void Reset() override
    {
        BossAI::Reset();

        _phase = PHASE_NOT_ENGAGED;
        _ballLightningUnlocked = false;
        _ballLightningEnabled = false;
        _shockingEligibility = true;

        me->SetReactState(REACT_PASSIVE);

        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
        me->SetImmuneToPC(true);

        me->SummonCreatureGroup(THADDIUS_GROUP_STALAGG);
        me->SummonCreatureGroup(THADDIUS_GROUP_FEUGEN);
    }

    void EnterEvadeMode(EvadeReason why) override
    {
        if (_phase == PHASE_THADDIUS && !_ballLightningEnabled && why == EVADE_REASON_NO_HOSTILES)
        {
            _ballLightningEnabled = true;
            return; // try again
        }

        // remove polarity shift debuffs on reset
        instance->DoRemoveAurasDueToSpellOnPlayers(SPELL_POSITIVE_CHARGE_APPLY);
        instance->DoRemoveAurasDueToSpellOnPlayers(SPELL_NEGATIVE_CHARGE_APPLY);

        summons.DespawnAll();
        BossAI::EnterEvadeMode(why);
    }

    bool CanAIAttack(Unit const* who) const override
    {
        if (_ballLightningEnabled || me->IsWithinMeleeRange(who))
            return BossAI::CanAIAttack(who);
        else
            return false;
    }

    void KilledUnit(Unit* victim) override
    {
        if (victim->GetTypeId() == TYPEID_PLAYER)
            Talk(SAY_SLAY);
    }

    void JustDied(Unit* killer) override
    {
        BossAI::JustDied(killer);
        Talk(SAY_DEATH);
    }

    void EnterCombat(Unit* who) override
    {
        BossAI::EnterCombat(who);
        _phase = PHASE_PETS;
        summons.DoZoneInCombat();
    }

    void JustSummoned(Creature* summon) override
    {
        BossAI::JustSummoned(summon);
        if (summon->GetEntry() == NPC_FEUGEN)
            _feugenAlive = true;
        else if (summon->GetEntry() == NPC_STALAGG)
            _stalaggAlive = true;
    }

    void SummonedCreatureDies(Creature* summon, Unit* /*killer*/) override
    {
        if (summon->GetEntry() == NPC_FEUGEN && _stalaggAlive)
            _feugenAlive = false;
        else if (summon->GetEntry() == NPC_STALAGG && _feugenAlive)
            _stalaggAlive = false;
        else
            Transition();
    }

    void DoAction(int32 action) override
    {
        if (action == ACTION_POLARITY_CROSSED)
            _shockingEligibility = false;
        else if (action == ACTION_PET_REVIVED)
            _feugenAlive = _stalaggAlive = true;
    }

    uint32 GetData(uint32 id) const override
    {
        return uint32(id == DATA_POLARITY_CROSSED && _shockingEligibility);
    }

    void UpdateAI(uint32 diff) override
    {
        scheduler.Update(diff, [this]
        {
            if (_phase == PHASE_THADDIUS && !me->HasUnitState(UNIT_STATE_CASTING))
            {
                if (me->IsWithinMeleeRange(me->GetVictim()))
                {
                    _ballLightningEnabled = false;
                    DoMeleeAttackIfReady();
                }
                else if (_ballLightningUnlocked)
                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM))
                        DoCast(target, SPELL_BALL_LIGHTNING);
            }
        });
    }

private:
    uint8 _phase;
    bool _stalaggAlive;
    bool _feugenAlive;
    bool _ballLightningUnlocked; // whether the initial ball lightning grace period has expired and we should proceed to exterminate with extreme prejudice
    bool _ballLightningEnabled; // switch that is flipped to true if we try to evade due to no eligible targets in melee range
    bool _shockingEligibility;

    // initiate transition between pet phase and thaddius phase
    void Transition()
    {
        _phase = PHASE_TRANSITION;
        scheduler.CancelGroup(GROUP_PETS);
        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);

        for (auto guid : summons)
            if (Creature* pet = instance->instance->GetCreature(guid))
                pet->KillSelf();

        scheduler.Schedule(Seconds(10), [this](TaskContext /*coil_overload*/)
        {
            CoilTalk(instance->GetGameObject(DATA_NAXX_TESLA_FEUGEN), EMOTE_TESLA_OVERLOAD);
            CoilTalk(instance->GetGameObject(DATA_NAXX_TESLA_STALAGG), EMOTE_TESLA_OVERLOAD);
        })
        .Schedule(Seconds(12), [this](TaskContext /*coil_shock*/)
        {
            me->CastSpell(me, SPELL_THADDIUS_SPARK_VISUAL, true);
            CoilCast(instance->GetGameObject(DATA_NAXX_TESLA_FEUGEN), SPELL_SHOCK_VISUAL, me);
            CoilCast(instance->GetGameObject(DATA_NAXX_TESLA_STALAGG), SPELL_SHOCK_VISUAL, me);
        })
        .Schedule(Seconds(14), [this](TaskContext start_fight)
        {
            me->RemoveAurasDueToSpell(SPELL_THADDIUS_INACTIVE);
            me->SetImmuneToPC(false);
            me->SetReactState(REACT_AGGRESSIVE);
            DoCastSelf(SPELL_THADDIUS_SPARK_VISUAL);
            _ballLightningUnlocked = false;
            _phase = PHASE_THADDIUS;

            summons.DespawnAll();

            if (Unit* closest = SelectTarget(SELECT_TARGET_MINDISTANCE, 0, 1000.0f))
                AttackStart(closest);
            else // if there is no nearest target, then there is no target, meaning we should reset
            {
                _ballLightningEnabled = true;
                EnterEvadeMode(EVADE_REASON_NO_HOSTILES);
                return;
            }

            CoilCast(instance->GetGameObject(DATA_NAXX_TESLA_FEUGEN), 0);
            CoilCast(instance->GetGameObject(DATA_NAXX_TESLA_STALAGG), 0);

            Talk(SAY_AGGRO);

            start_fight.Schedule(Seconds(5), [this](TaskContext /*enable_ball_lightning*/)
            {
                _ballLightningUnlocked = true;
            })
            .Schedule(Seconds(10), [this](TaskContext polarity_shift)
            {
                me->CastStop(); // shift overrides all other spells
                DoCastAOE(SPELL_POLARITY_SHIFT);

                polarity_shift.Schedule(Seconds(3), [this](TaskContext /*polarity_shift_talk*/)
                {
                    Talk(SAY_ELECT);
                    Talk(EMOTE_POLARITY_SHIFTED);
                });

                polarity_shift.Repeat(Seconds(30));
            })
            .Schedule(Seconds(10), Seconds(20), [this](TaskContext chain_lightning)
            {
                if (me->HasUnitState(UNIT_STATE_CASTING))
                    chain_lightning.Repeat(Seconds(1));
                else
                {
                    DoCastVictim(SPELL_CHAIN_LIGHTNING);
                    chain_lightning.Repeat(Seconds(10), Seconds(20));
                }
            })
            .Schedule(Minutes(6), [this](TaskContext /*berserk*/)
            {
                me->CastStop();
                DoCastSelf(SPELL_BERSERK);
            });
        });
    }
};

struct npc_thaddius_petAI : public ScriptedAI
{
    npc_thaddius_petAI(Creature* creature) : ScriptedAI(creature), instance(creature->GetInstanceScript()), _isOverloading(false)
    {
        SetBoundary(instance->GetBossBoundary(BOSS_THADDIUS));
    }

    void EnterEvadeMode(EvadeReason why) override
    {
        if (Creature* thaddius = instance->GetCreature(DATA_THADDIUS))
            thaddius->AI()->EnterEvadeMode(why);

        me->DespawnOrUnsummon();
    }

    void KilledUnit(Unit* victim) override
    {
        if (victim->GetTypeId() == TYPEID_PLAYER)
            Talk(SAY_PET_SLAY);
    }

    void DamageTaken(Unit* done_by, uint32& damage) override
    {
        if (me->IsImmuneToPC())
            damage = 0;
        else if (damage >= me->GetHealth())
        {
            damage = 0;
            me->RemoveAllAuras();
            me->AttackStop();
            me->SetImmuneToPC(true);
            scheduler.CancelAll();

            DoCastSelf(SPELL_FEIGN_DEATH);
            Talk(EMOTE_FEIGN_DEATH);

            scheduler.Schedule(Seconds(5), [this](TaskContext /*respawn*/)
            {
                me->SetFullHealth();
                me->RemoveAurasDueToSpell(SPELL_FEIGN_DEATH);
                me->SetImmuneToPC(false);
                me->SetReactState(REACT_AGGRESSIVE);
                ScheduleTasks();
            });

            if (Creature* thaddius = instance->GetCreature(DATA_THADDIUS))
                thaddius->AI()->SummonedCreatureDies(me, done_by);
        }
    }

    void EnterCombat(Unit* who) override
    {
        Talk(SAY_PET_AGGRO);
        ScheduleTasks();

        if (Creature* thaddius = instance->GetCreature(DATA_THADDIUS))
            thaddius->EngageWithTarget(who);
    }

    void UpdateAI(uint32 diff) override
    {
        // immune = feign death
        if (me->IsImmuneToPC() || UpdateVictim())
        {
            scheduler.Update(diff, [this] {
                DoMeleeAttackIfReady();
            });
        }
    }

protected:
    void _SpellHitPet(Unit* caster, uint32 chainVisualId)
    {
        if (me->IsEngaged() && !me->GetHomePosition().IsInDist(me, OVERLOAD_DISTANCE))
        {
            if (!_isOverloading)
            {
                _isOverloading = true;
                caster->SetImmuneToPC(false);
                if (Creature* creatureCaster = caster->ToCreature())
                    creatureCaster->AI()->Talk(EMOTE_TESLA_LINK_BREAKS);

                me->RemoveAurasDueToSpell(chainVisualId);
            }

            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM))
            {
                caster->CastStop(SPELL_TESLA_SHOCK);
                caster->CastSpell(target, SPELL_TESLA_SHOCK,true);
            }
        }
        else if (_isOverloading)
        {
            _isOverloading = false;
            caster->CastStop();
            caster->CastSpell(me, chainVisualId, true);
            caster->SetImmuneToPC(true);
        }
        else if (!me->HasAura(chainVisualId))
            JustAppeared(); // reapply coil aura
    }

    virtual void ScheduleTasks() = 0;

    TaskScheduler scheduler;
	InstanceScript* instance;

private:
    bool _isOverloading;
};

struct npc_stalagg : public npc_thaddius_petAI
{
    npc_stalagg(Creature* creature) : npc_thaddius_petAI(creature) { }

    void JustAppeared() override
    {
        CoilCast(instance->GetGameObject(DATA_NAXX_TESLA_STALAGG), SPELL_STALAGG_CHAIN_VISUAL, me);
    }

    void ScheduleTasks() override
    {
        scheduler.Schedule(Seconds(10), [this](TaskContext power_surge)
        {
            DoCastSelf(SPELL_STALAGG_POWERSURGE);
            power_surge.Repeat(Seconds(25), Seconds(30));
        });
    }

    void SpellHit(Unit* caster, SpellInfo const* spell) override
    {
        if (!caster || spell->Id != SPELL_STALAGG_TESLA_PERIODIC)
            return;

        _SpellHitPet(caster, SPELL_STALAGG_CHAIN_VISUAL);
    }
};

struct npc_feugen : public npc_thaddius_petAI
{
    npc_feugen(Creature* creature) : npc_thaddius_petAI(creature) { }

    void JustAppeared() override
    {
        CoilCast(instance->GetGameObject(DATA_NAXX_TESLA_FEUGEN), SPELL_FEUGEN_CHAIN_VISUAL, me);
    }

    void ScheduleTasks() override
    {
        scheduler.Schedule(Seconds(6), [this](TaskContext static_field)
        {
            DoCastSelf(SPELL_FEUGEN_STATICFIELD);
            static_field.Repeat();
        })
        .Schedule(Seconds(20), [this](TaskContext magnetic_pull)
        {
            DoCastSelf(SPELL_MAGNETIC_PULL);
            magnetic_pull.Repeat();
        });
    }

    void SpellHit(Unit* caster, SpellInfo const* spell) override
    {
        if (!caster || spell->Id != SPELL_FEUGEN_TESLA_PERIODIC)
            return;

        _SpellHitPet(caster, SPELL_FEUGEN_CHAIN_VISUAL);
    }
};

struct npc_tesla : public ScriptedAI
{
    npc_tesla(Creature* creature) : ScriptedAI(creature)
    {
        me->SetReactState(REACT_PASSIVE);
    }

    void EnterEvadeMode(EvadeReason /*why*/) override { } // never stop casting due to evade
    void UpdateAI(uint32 /*diff*/) override { } // never do anything unless told
    void EnterCombat(Unit* /*who*/) override { }
    void DamageTaken(Unit* /*who*/, uint32& damage) override { damage = 0; } // no, you can't kill it
};

class spell_thaddius_polarity_charge : public SpellScript
{
    PrepareSpellScript(spell_thaddius_polarity_charge);

    bool Validate(SpellInfo const* /*spell*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_POSITIVE_CHARGE_APPLY,
            SPELL_POSITIVE_CHARGE_AMP,
            SPELL_NEGATIVE_CHARGE_APPLY,
            SPELL_NEGATIVE_CHARGE_AMP
        });
    }

    void HandleTargets(std::list<WorldObject*>& targets)
    {
        if (!GetTriggeringSpell())
            return;

        InstanceScript* instance = GetCaster()->GetInstanceScript();
        if (!instance)
            return;

        uint32 triggeringId = GetTriggeringSpell()->Id;
        uint32 ampId = triggeringId == SPELL_POSITIVE_CHARGE_APPLY ? SPELL_POSITIVE_CHARGE_AMP : SPELL_NEGATIVE_CHARGE_AMP;
        uint32 maxStacks = instance->instance->GetDifficulty() == RAID_DIFFICULTY_10MAN_NORMAL ? MAX_POLARITY_10M : MAX_POLARITY_25M;
        uint32 stacksCount = 1;

        targets.remove_if([&stacksCount, triggeringId](WorldObject* obj)
        {
            if (obj->GetTypeId() != TYPEID_PLAYER)
                return true;
            else if (obj->ToPlayer()->HasAura(triggeringId))
            {
                ++stacksCount;
                return true;
            }
            else
                return false;
        });

        if (!targets.empty())
            if (Creature* thaddius = instance->GetCreature(DATA_THADDIUS))
                thaddius->AI()->DoAction(ACTION_POLARITY_CROSSED);

        GetCaster()->SetAuraStack(ampId, GetCaster(), std::min(stacksCount, maxStacks));
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_thaddius_polarity_charge::HandleTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
    }
};

class spell_thaddius_polarity_shift : public SpellScript
{
    PrepareSpellScript(spell_thaddius_polarity_shift);

    bool Validate(SpellInfo const* /*spell*/) override
    {
        return ValidateSpellInfo(
        {
            SPELL_POSITIVE_CHARGE_APPLY,
            SPELL_POSITIVE_CHARGE_AMP,
            SPELL_NEGATIVE_CHARGE_APPLY,
            SPELL_NEGATIVE_CHARGE_AMP
        });
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        Unit* target = GetHitUnit();
        if (target->GetTypeId() != TYPEID_PLAYER)
            return;

        target->RemoveAurasDueToSpell(SPELL_POSITIVE_CHARGE_APPLY);
        target->RemoveAurasDueToSpell(SPELL_NEGATIVE_CHARGE_APPLY);
        target->RemoveAurasDueToSpell(SPELL_POSITIVE_CHARGE_AMP);
        target->RemoveAurasDueToSpell(SPELL_NEGATIVE_CHARGE_AMP);
        target->CastSpell(target, roll_chance_i(50) ? SPELL_POSITIVE_CHARGE_APPLY : SPELL_NEGATIVE_CHARGE_APPLY, true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_thaddius_polarity_shift::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

class spell_thaddius_magnetic_pull : public SpellScript
{
    PrepareSpellScript(spell_thaddius_magnetic_pull);

    bool Validate(SpellInfo const* /*spell*/) override
    {
        return ValidateSpellInfo({ SPELL_MAGNETIC_PULL_EFFECT });
    }

    void HandleCast() // only feugen ever casts this according to wowhead data
    {
        Unit* feugen = GetCaster();
        if (!feugen || feugen->GetEntry() != NPC_FEUGEN)
            return;

        Unit* stalagg = feugen->FindNearestCreature(NPC_STALAGG, 500.0f, true);
        if (!stalagg)
            return;

        ThreatManager& feugenThreat = feugen->GetThreatManager();
        ThreatManager& stalaggThreat = stalagg->GetThreatManager();

        Unit* feugenTank = feugenThreat.GetCurrentVictim();
        Unit* stalaggTank = stalaggThreat.GetCurrentVictim();

        if (!feugenTank || !stalaggTank)
            return;

        if (feugenTank == stalaggTank) // special behavior if the tanks are the same (taken from retail)
        {
            float feugenTankThreat = feugenThreat.GetThreat(feugenTank);
            float stalaggTankThreat = stalaggThreat.GetThreat(stalaggTank);

            feugen->GetThreatManager().AddThreat(feugenTank, stalaggTankThreat - feugenTankThreat, nullptr, true, true);
            stalagg->GetThreatManager().AddThreat(stalaggTank, feugenTankThreat - stalaggTankThreat, nullptr, true, true);

            feugen->CastSpell(stalaggTank, SPELL_MAGNETIC_PULL_EFFECT, true);
        }
        else // normal case, two tanks
        {
            float feugenTankThreat = feugenThreat.GetThreat(feugenTank);
            float feugenOtherThreat = feugenThreat.GetThreat(stalaggTank);
            float stalaggTankThreat = stalaggThreat.GetThreat(stalaggTank);
            float stalaggOtherThreat = stalaggThreat.GetThreat(feugenTank);

            // set the two entries in feugen's threat table to be equal to the ones in stalagg's
            feugen->GetThreatManager().AddThreat(stalaggTank, stalaggTankThreat - feugenOtherThreat, nullptr, true, true);
            feugen->GetThreatManager().AddThreat(feugenTank, stalaggOtherThreat - feugenTankThreat, nullptr, true, true);

            // set the two entries in stalagg's threat table to be equal to the ones in feugen's
            stalagg->GetThreatManager().AddThreat(feugenTank, feugenTankThreat - stalaggOtherThreat, nullptr, true, true);
            stalagg->GetThreatManager().AddThreat(stalaggTank, feugenOtherThreat - stalaggTankThreat, nullptr, true, true);

            // pull the two tanks across
            feugenTank->CastSpell(stalaggTank, SPELL_MAGNETIC_PULL_EFFECT, true);
            stalaggTank->CastSpell(feugenTank, SPELL_MAGNETIC_PULL_EFFECT, true);

            // @hack prevent mmaps clusterfucks from breaking tesla while tanks are midair
            feugen->AddAura(SPELL_ROOT_SELF, feugen);
            stalagg->AddAura(SPELL_ROOT_SELF, stalagg);

            // and make both attack their respective new tanks
            if (feugen->GetAI())
                feugen->GetAI()->AttackStart(stalaggTank);
            if (stalagg->GetAI())
                stalagg->GetAI()->AttackStart(feugenTank);
        }
    }

    void Register() override
    {
        OnCast += SpellCastFn(spell_thaddius_magnetic_pull::HandleCast);
    }
};

struct at_thaddius_entrance : public OnlyOnceAreaTriggerScript
{
    at_thaddius_entrance() : OnlyOnceAreaTriggerScript("at_thaddius_entrance") { }

    bool _OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/) override
    {
        if (player->IsGameMaster())
            return true;

        InstanceScript* instance = player->GetInstanceScript();
        if (instance && instance->GetBossState(BOSS_THADDIUS) != DONE)
        {
            if (Creature* thaddius = instance->GetCreature(DATA_THADDIUS))
                thaddius->AI()->Talk(SAY_GREET);
        }

        return true;
    }
};

struct achievement_thaddius_shocking : public AchievementCriteriaScript
{
    achievement_thaddius_shocking() : AchievementCriteriaScript("achievement_thaddius_shocking") { }

    bool OnCheck(Player* /*source*/, Unit* target) override
    {
        return target && target->GetAI() && target->GetAI()->GetData(DATA_POLARITY_CROSSED);
    }
};

void AddSC_boss_thaddius()
{
    RegisterNaxxramasCreatureAI(boss_thaddius);
    RegisterNaxxramasCreatureAI(npc_stalagg);
    RegisterNaxxramasCreatureAI(npc_feugen);
    RegisterNaxxramasCreatureAI(npc_tesla);

    RegisterSpellScript(spell_thaddius_polarity_charge);
    RegisterSpellScript(spell_thaddius_polarity_shift);
    RegisterSpellScript(spell_thaddius_magnetic_pull);

    new at_thaddius_entrance();
    new achievement_thaddius_shocking();
}
