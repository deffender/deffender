#include <chrono>
#include "MotionMaster.h"
#include "ScriptedCreature.h"
#include "Spell.h"
#include "ulduar.h"

enum Spells
{
    SPELL_FORKED_LIGHTNING = 63541,
    SPELL_SEPARATION_ANXIETY = 63539,
    SPELL_SUMMON_CHARGED_SPHERE = 63527,
    SPELL_VENGEFUL_SURGE = 63630,
    SPELL_SUPERCHARGED = 63528,
};

enum Misc
{
    NPC_STORM_TEMPERED_KEEPER1 = 33699,
    NPC_STORM_TEMPERED_KEEPER2 = 33722,
};

#define IS_FIRST(who) (who->GetEntry() == NPC_STORM_TEMPERED_KEEPER1)
#define OTHER_KEEPER(who) (IS_FIRST(who) ? NPC_STORM_TEMPERED_KEEPER2 : NPC_STORM_TEMPERED_KEEPER1)

struct npc_ulduar_storm_keeper : public ScriptedAI
{
    npc_ulduar_storm_keeper(Creature* creature) : ScriptedAI(creature), instance(creature->GetInstanceScript())
    {
        scheduler.SetValidator([this]
        {
            return !me->HasUnitState(UNIT_STATE_CASTING);
        });
    }

    void Reset() override
    {
        scheduler.CancelAll();
    }

    void JustDied(Unit* /*killer*/) override
    {
        scheduler.CancelAll();
    }

    void EnterCombat(Unit* who) override
    {
        _linkedKeeper.Clear();
        auto sphereTimer = 10s;

        if (Creature* creature = me->FindNearestCreature(OTHER_KEEPER(me), 50.0f, true))
        {
            _linkedKeeper = creature->GetGUID();

            if (creature->IsAlive())
            {
                DoZoneInCombat(creature);

                // we want different timers
                if (!IS_FIRST(me))
                    sphereTimer = 20s;
            }
        }
        else
            DoCastSelf(SPELL_VENGEFUL_SURGE);

        scheduler.Schedule(500ms, [this](TaskContext check_linked)
        {
            if (Creature* creature = instance->instance->GetCreature(_linkedKeeper))
            {
                if (!creature->IsAlive() && !me->HasAura(SPELL_VENGEFUL_SURGE))
                    DoCastSelf(SPELL_VENGEFUL_SURGE);

                if (me->GetExactDist(creature) > 50.0f && !_hasAnxiety)
                {
                    scheduler.Schedule(1ms, [this](TaskContext cast_anxiety)
                    {
                        // avoid spamming this spell
                        if (_hasAnxiety)
                        {
                            DoCastSelf(SPELL_SEPARATION_ANXIETY);
                            cast_anxiety.Repeat(5s);
                        }
                    });

                    _hasAnxiety = true;
                }
                else if (_hasAnxiety)
                {
                    me->RemoveAurasDueToSpell(SPELL_SEPARATION_ANXIETY);
                    _hasAnxiety = false;
                }
            }

            check_linked.Repeat();
        })
        .Schedule(8s, 12s, [this](TaskContext forked_lightning)
        {
            DoCastAOE(SPELL_FORKED_LIGHTNING);
            forked_lightning.Repeat();
        })
        .Schedule(sphereTimer, [this](TaskContext sphere)
        {
            DoCastAOE(SPELL_SUMMON_CHARGED_SPHERE);
            sphere.Repeat(20s, 25s);
        });
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        scheduler.Update(diff, [this]
        {
            DoMeleeAttackIfReady();
        });
    }

private:
    InstanceScript* instance;
    ObjectGuid _linkedKeeper;
    TaskScheduler scheduler;
    bool _hasAnxiety;
};

struct npc_ulduar_charged_sphere : public ScriptedAI
{
    npc_ulduar_charged_sphere(Creature* creature) : ScriptedAI(creature)
    {
        me->SetReactState(REACT_PASSIVE);
    }

    void IsSummonedBy(Unit* summoner) override
    {
        if (Creature* keeper = me->FindNearestCreature(OTHER_KEEPER(summoner), 100.0f))
            me->GetMotionMaster()->MoveFollow(keeper, 0.0f, 0.0f, true);
    }

    void MovementInform(uint32 type, uint32 /*id*/) override
    {
        if (type == FOLLOW_MOTION_TYPE)
        {
            DoCastAOE(SPELL_SUPERCHARGED);
            me->GetMotionMaster()->MoveIdle();
            me->DespawnOrUnsummon(200ms);
        }
    }
};

class spell_ulduar_teleporter : public SpellScript
{
    PrepareSpellScript(spell_ulduar_teleporter);

    SpellCastResult CheckRequirement()
    {
        if (GetExplTargetUnit()->GetTypeId() != TYPEID_PLAYER)
            return SPELL_FAILED_DONT_REPORT;

        if (GetExplTargetUnit()->IsInCombat())
        {
            Spell::SendCastResult(GetExplTargetUnit()->ToPlayer(), GetSpellInfo(), 0, SPELL_FAILED_AFFECTING_COMBAT);
            return SPELL_FAILED_AFFECTING_COMBAT;
        }

        return SPELL_CAST_OK;
    }

    void Register() override
    {
        OnCheckCast += SpellCheckCastFn(spell_ulduar_teleporter::CheckRequirement);
    }
};

struct UlduarPlayerScript : public PlayerScript
{
    enum Spells
    {
        AURA_UD_NERF = 110002
    };

    UlduarPlayerScript() : PlayerScript("UlduarPlayerScript") {}

    void OnUpdateZone(Player* player, uint32 newZone, uint32 newArea)
    {
        if (newZone == 4273)
            player->CastSpell(player, AURA_UD_NERF, true);
        else
            player->RemoveAurasDueToSpell(AURA_UD_NERF);

        // prevent teleporting/leaving with vehicles
        if (player->GetVehicleBase() && newArea != 4650 && newArea != 4665 && newArea != 4652)
            if (Creature* vehicle = player->GetVehicleBase()->ToCreature())
                if (vehicle->GetEntry() == NPC_SALVAGED_DEMOLISHER
                    || vehicle->GetEntry() == NPC_SALVAGED_SIEGE_ENGINE
                    || vehicle->GetEntry() == NPC_SALVAGED_CHOPPER)
                    vehicle->DespawnOrUnsummon();
    }
};

void AddSC_ulduar()
{
    RegisterUlduarCreatureAI(npc_ulduar_storm_keeper);
    RegisterUlduarCreatureAI(npc_ulduar_charged_sphere);
    RegisterSpellScript(spell_ulduar_teleporter);
    new UlduarPlayerScript();
}
