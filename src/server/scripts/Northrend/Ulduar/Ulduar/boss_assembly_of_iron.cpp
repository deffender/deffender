/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
SDName: Assembly of Iron encounter
SD%Complete: 60%
SDComment: chain lightning won't cast, supercharge don't work (auras don't stack from different casters)
SDCategory: Ulduar - Ulduar
EndScriptData */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "ScriptedCreature.h"
#include "SpellAuras.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TemporarySummon.h"
#include "ulduar.h"

enum AssemblySpells
{
    // General
    SPELL_SUPERCHARGE                            = 61920,
    SPELL_BERSERK                                = 47008, // Hard enrage, don't know the correct ID.
    SPELL_KILL_CREDIT                            = 65195, // spell_dbc

    // Steelbreaker
    SPELL_HIGH_VOLTAGE                           = 61890,
    SPELL_FUSION_PUNCH                           = 61903,
    SPELL_STATIC_DISRUPTION                      = 44008,
    SPELL_OVERWHELMING_POWER                     = 64637,
    SPELL_ELECTRICAL_CHARGE                      = 61902,

    // Runemaster Molgeim
    SPELL_SHIELD_OF_RUNES                        = 62274,
    SPELL_SHIELD_OF_RUNES_BUFF                   = 62277,
    SPELL_SUMMON_RUNE_OF_POWER                   = 63513,
    SPELL_RUNE_OF_DEATH                          = 62269,
    SPELL_RUNE_OF_SUMMONING                      = 62273, // This is the spell that summons the rune
    SPELL_RUNE_OF_SUMMONING_SUMMON               = 62020, // Spell that summons

    // Stormcaller Brundir
    SPELL_CHAIN_LIGHTNING                        = 61879,
    SPELL_OVERLOAD                               = 61869,
    SPELL_LIGHTNING_WHIRL                        = 61915,
    SPELL_LIGHTNING_TENDRILS                     = 61887,
    SPELL_LIGHTNING_TENDRILS_VISUAL              = 61883,
    SPELL_STORMSHIELD                            = 64187
};

enum AssemblyActions
{
    ACTION_SUPERCHARGE                           = 1,
    ACTION_ADD_CHARGE                            = 2
};

enum AssemblyYells
{
    SAY_ASSEMBLY_AGGRO                          = 0,
    SAY_ASSEMBLY_SLAY                           = 1,
    SAY_ASSEMBLY_DEATH                          = 2,
    SAY_ASSEMBLY_ENCOUNTER_DEFEATED             = 3,
    SAY_ASSEMBLY_BERSERK                        = 4,

    SAY_STEELBREAKER_POWER                      = 5,

    SAY_MOLGEIM_RUNE_DEATH                      = 5,
    SAY_MOLGEIM_SUMMON                          = 6,

    SAY_BRUNDIR_SPECIAL                         = 5,
    SAY_BRUNDIR_FLIGHT                          = 6,
    EMOTE_BRUNDIR_OVERLOAD                      = 7
};

enum Misc
{
    NPC_WORLD_TRIGGER                            = 22515,

    DATA_PHASE_3                                 = 1
};

#define FLOOR_Z                                  427.28f
#define FINAL_FLIGHT_Z                           435.0f

struct boss_assemblyAI : public BossAI
{
    boss_assemblyAI(Creature* creature) : BossAI(creature, BOSS_ASSEMBLY_OF_IRON), _phase(0)
    {}

    virtual void Reset() override
    {
        _Reset();
        _phase = 0;
    }

    virtual void EnterCombat(Unit* /*who*/) override
    {
        _EnterCombat();
        Talk(SAY_ASSEMBLY_AGGRO);
        ++_phase;
    }

    uint32 GetData(uint32 type) const override
    {
        if (type == DATA_PHASE_3)
            return (_phase >= 3) ? 1 : 0;

        return 0;
    }

    void DamageTaken(Unit* /*attacker*/, uint32& damage) override
    {
        if (damage >= me->GetHealth())
            // minion magic in progress...
            if (!CheckEncounterFinish())
                me->SetLootDisabled(true);
    }

    void JustDied(Unit* /*killer*/) override
    {
        _JustDied();

        if (instance->GetBossState(BOSS_ASSEMBLY_OF_IRON) == DONE)
        {
            DoCastAOE(SPELL_KILL_CREDIT, true);
            Talk(SAY_ASSEMBLY_ENCOUNTER_DEFEATED);
        }
        else
        {
            DoCastAOE(SPELL_SUPERCHARGE, true);
            Talk(SAY_ASSEMBLY_DEATH);
        }
    }

    virtual void KilledUnit(Unit* who) override
    {
        if (who->GetTypeId() == TYPEID_PLAYER)
            Talk(SAY_ASSEMBLY_SLAY);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        scheduler.Update(diff, [this]
        {
            if (me->GetEntry() != NPC_BRUNDIR)
                DoMeleeAttackIfReady();
        });
    }

protected:
    uint32 _phase;

private:
    bool CheckEncounterFinish()
    {
        std::array<uint32, 2> check;
        switch (me->GetEntry())
        {
            case NPC_STEELBREAKER: check = {{DATA_MOLGEIM, DATA_BRUNDIR}}; break;
            case NPC_MOLGEIM: check = {{DATA_STEELBREAKER, DATA_BRUNDIR}}; break;
            case NPC_BRUNDIR: check = {{DATA_STEELBREAKER, DATA_MOLGEIM}}; break;
            default: return false;
        }

        for (uint32 id : check)
            if (Creature* creature = instance->instance->GetCreature(instance->GetGuidData(id)))
                if (creature->IsAlive())
                    return false;

        return true;
    }
};

struct boss_steelbreaker : public boss_assemblyAI
{
    enum Groups
    {
        GROUP_PUNCH = 1
    };

    boss_steelbreaker(Creature* creature) : boss_assemblyAI(creature) { }

    void EnterCombat(Unit* who) override
    {
        boss_assemblyAI::EnterCombat(who);
        DoCastSelf(SPELL_HIGH_VOLTAGE);
    }

    void ScheduleTasks() override
    {
        scheduler.Schedule(15min, [this](TaskContext /*berserk*/)
        {
            Talk(SAY_ASSEMBLY_BERSERK);
            DoCastSelf(SPELL_BERSERK);
        })
        .Schedule(15s, GROUP_PUNCH, [this](TaskContext fusion_punch)
        {
            if (me->IsWithinMeleeRange(me->GetVictim()))
                DoCastVictim(SPELL_FUSION_PUNCH);

            fusion_punch.Repeat(13s, 22s);
        });
    }

    void SpellHit(Unit* /*caster*/, SpellInfo const* spell) override
    {
        if (spell->Id == SPELL_SUPERCHARGE)
        {
            ++_phase;
            me->SetFullHealth();
            scheduler.RescheduleGroup(GROUP_PUNCH, 15s);

            if (_phase == 2)
            {
                scheduler.Schedule(30s, [this](TaskContext static_disruption)
                {
                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                        DoCast(target, SPELL_STATIC_DISRUPTION);

                    static_disruption.Repeat(20s, 40s);
                });
            }

            if (_phase == 3)
            {
                scheduler.Schedule(2s, 5s, [this](TaskContext overwhelming_power)
                {
                    Talk(SAY_STEELBREAKER_POWER);
                    DoCastVictim(SPELL_OVERWHELMING_POWER);
                    overwhelming_power.Repeat(RAID_MODE(61s, 36s));
                });
            }
        }
    }

    void DoAction(int32 action) override
    {
        switch (action)
        {
            case ACTION_ADD_CHARGE:
                DoCast(me, SPELL_ELECTRICAL_CHARGE, true);
                break;
        }
    }

    void KilledUnit(Unit* who) override
    {
        boss_assemblyAI::KilledUnit(who);
        if (_phase == 3)
            DoCast(me, SPELL_ELECTRICAL_CHARGE);
    }
};

struct boss_runemaster_molgeim : public boss_assemblyAI
{
    enum Groups
    {
        GROUP_SHIELD,
        GROUP_RUNE,
    };

    boss_runemaster_molgeim(Creature* creature) : boss_assemblyAI(creature) { }

    void ScheduleTasks() override
    {
        scheduler.Schedule(15min, [this](TaskContext /*berserk*/)
        {
            Talk(SAY_ASSEMBLY_BERSERK);
            DoCast(SPELL_BERSERK);
        })
        .Schedule(20s, GROUP_RUNE, [this](TaskContext rune_of_power)
        {
            Unit* target = me;
            if (uint32 data = urand(0, 2))
                if (Creature* boss = ObjectAccessor::GetCreature(*me, instance->GetGuidData(data == 1 ? DATA_STEELBREAKER : DATA_BRUNDIR)))
                    if (boss->IsAlive())
                        target = boss;

            DoCast(target, SPELL_SUMMON_RUNE_OF_POWER);
            rune_of_power.Repeat(60s);
        })
        .Schedule(30s, GROUP_SHIELD, [this](TaskContext shield_of_runes)
        {
            DoCast(me, SPELL_SHIELD_OF_RUNES);
            shield_of_runes.Repeat(27s, 34s);
        });
    }

    void SpellHit(Unit* /*caster*/, SpellInfo const* spell) override
    {
        if (spell->Id == SPELL_SUPERCHARGE)
        {
            ++_phase;
            me->SetFullHealth();
            scheduler.RescheduleGroup(GROUP_SHIELD, 27s);
            scheduler.RescheduleGroup(GROUP_RUNE, 25s);

            if (_phase == 2)
            {
                scheduler.Schedule(30s, [this](TaskContext rune_of_death)
                {
                    Talk(SAY_MOLGEIM_RUNE_DEATH);
                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                        DoCast(target, SPELL_RUNE_OF_DEATH);

                    rune_of_death.Repeat(30s, 40s);
                });
            }

            if (_phase == 3)
            {
                scheduler.Schedule(20s, 30s, [this](TaskContext rune_of_summoning)
                {
                    Talk(SAY_MOLGEIM_SUMMON);
                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                        DoCast(target, SPELL_RUNE_OF_SUMMONING);
                    rune_of_summoning.Repeat();
                });
            }
        }
    }
};

struct boss_stormcaller_brundir : public boss_assemblyAI
{
    enum Groups
    {
        GROUP_CHAIN_LIGHTNING,
        GROUP_OVERLOAD,
        GROUP_FLIGHT,
    };

    boss_stormcaller_brundir(Creature* creature) : boss_assemblyAI(creature) { }

    void Reset() override
    {
        boss_assemblyAI::Reset();
        me->SetDisableGravity(false);
        me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_INTERRUPT, false);  // Should be interruptable unless overridden by spell (Overload)
        me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_STUN, false);       // Reset immumity, Brundir should be stunnable by default
    }

    void ScheduleTasks() override
    {
        scheduler.Schedule(15min, [this](TaskContext /*berserk*/)
        {
            Talk(SAY_ASSEMBLY_BERSERK);
            DoCast(SPELL_BERSERK);
        })
        .Schedule(4s, GROUP_CHAIN_LIGHTNING, [this](TaskContext chain_lightning)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                DoCast(target, SPELL_CHAIN_LIGHTNING);

            chain_lightning.Repeat(7s, 10s);
        })
        .Schedule(1min, 2min, GROUP_OVERLOAD, [this](TaskContext overload)
        {
            Talk(EMOTE_BRUNDIR_OVERLOAD);
            Talk(SAY_BRUNDIR_SPECIAL);
            DoCast(SPELL_OVERLOAD);
            overload.Repeat();
        })
        .Schedule(1s, [this](TaskContext move_position)
        {
            if (me->IsWithinMeleeRange(me->GetVictim()))
            {
                float x = float(irand(-25, 25));
                float y = float(irand(-25, 25));
                me->GetMotionMaster()->MovePoint(0, me->GetPositionX() + x, me->GetPositionY() + y, FLOOR_Z);

                // Prevention to go outside the room or into the walls
                if (Creature* trigger = me->FindNearestCreature(NPC_WORLD_TRIGGER, 100.0f, true))
                    if (me->GetDistance(trigger) >= 50.0f)
                        me->GetMotionMaster()->MovePoint(0, trigger->GetPositionX(), trigger->GetPositionY(), FLOOR_Z);
            }

            move_position.Repeat(7500ms, 10s);
        });
    }

    void SpellHit(Unit* /*caster*/, SpellInfo const* spell) override
    {
        if (spell->Id == SPELL_SUPERCHARGE)
        {
            ++_phase;
            me->SetFullHealth();
            scheduler.RescheduleGroup(GROUP_CHAIN_LIGHTNING, 7s, 12s);
            scheduler.RescheduleGroup(GROUP_OVERLOAD, 40s, 50s);

            if (_phase == 2)
            {
                scheduler.Schedule(15s, 25s, [this](TaskContext lightning_whirl)
                {
                    DoCast(SPELL_LIGHTNING_WHIRL);
                    lightning_whirl.Repeat(15s, 20s);
                });
            }

            if (_phase == 3)
            {
                DoCast(me, SPELL_STORMSHIELD);
                scheduler.Schedule(50s, 60s, 0, std::bind(&boss_stormcaller_brundir::LightningTendrils, this, std::placeholders::_1));
                me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_STUN, true); // Apply immumity to stuns
            }
        }
    }

private:
    void LightningTendrils(TaskContext context)
    {
        Talk(SAY_BRUNDIR_FLIGHT);
        DoCast(me, SPELL_LIGHTNING_TENDRILS);
        DoCast(me, SPELL_LIGHTNING_TENDRILS_VISUAL);

        me->AttackStop();
        me->GetMotionMaster()->Initialize();
        me->GetMotionMaster()->MovePoint(0, me->GetPositionX(), me->GetPositionY(), FINAL_FLIGHT_Z);

        context.DelayAll(35s);

        context.Schedule(2500ms, GROUP_FLIGHT, [this](TaskContext flight)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                me->GetMotionMaster()->MovePoint(0, target->GetPositionX(), target->GetPositionY(), FINAL_FLIGHT_Z);

            flight.Repeat(6s);
        })
        .Schedule(32500ms, [this](TaskContext end_flight)
        {
            me->GetMotionMaster()->Initialize();
            me->GetMotionMaster()->MovePoint(0, 1586.920166f, 119.848984f, FINAL_FLIGHT_Z);

            end_flight.CancelGroup(GROUP_FLIGHT);
            end_flight.Schedule(4s, [this](TaskContext land)
            {
                me->GetMotionMaster()->Initialize();
                me->GetMotionMaster()->MovePoint(0, me->GetPositionX(), me->GetPositionY(), FLOOR_Z);

                land.Schedule(2500ms, [this](TaskContext /*ground*/)
                {
                    DoRemoveAura(SPELL_LIGHTNING_TENDRILS);
                    me->RemoveAurasDueToSpell(SPELL_LIGHTNING_TENDRILS_VISUAL);
                    me->GetMotionMaster()->MoveChase(me->GetVictim(), 0.0f, 0.0f);
                    ResetThreatList();
                });
            });
        });

        context.Repeat(90s);
    }
};

class spell_shield_of_runes : public AuraScript
{
    PrepareAuraScript(spell_shield_of_runes);

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Unit* caster = GetCaster())
            if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE)
                caster->CastSpell(caster, SPELL_SHIELD_OF_RUNES_BUFF, false);
    }

    void Register() override
    {
         AfterEffectRemove += AuraEffectRemoveFn(spell_shield_of_runes::OnRemove, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
    }
};

class spell_assembly_meltdown : public SpellScript
{
    PrepareSpellScript(spell_assembly_meltdown);

    void HandleInstaKill(SpellEffIndex /*effIndex*/)
    {
        if (InstanceScript* instance = GetCaster()->GetInstanceScript())
            if (Creature* Steelbreaker = ObjectAccessor::GetCreature(*GetCaster(), instance->GetGuidData(DATA_STEELBREAKER)))
                Steelbreaker->AI()->DoAction(ACTION_ADD_CHARGE);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_assembly_meltdown::HandleInstaKill, EFFECT_1, SPELL_EFFECT_INSTAKILL);
    }
};

class spell_assembly_rune_of_summoning : public AuraScript
{
    PrepareAuraScript(spell_assembly_rune_of_summoning);

    bool Validate(SpellInfo const* /*spell*/) override
    {
        return ValidateSpellInfo({ SPELL_RUNE_OF_SUMMONING_SUMMON });
    }

    void HandlePeriodic(AuraEffect const* aurEff)
    {
        PreventDefaultAction();
        GetTarget()->CastSpell(GetTarget(), SPELL_RUNE_OF_SUMMONING_SUMMON, true, nullptr, aurEff, GetTarget()->IsSummon() ? GetTarget()->ToTempSummon()->GetSummonerGUID() : ObjectGuid::Empty);
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (TempSummon* summ = GetTarget()->ToTempSummon())
            summ->DespawnOrUnsummon(1);
    }

    void Register() override
    {
        OnEffectPeriodic += AuraEffectPeriodicFn(spell_assembly_rune_of_summoning::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
        OnEffectRemove += AuraEffectRemoveFn(spell_assembly_rune_of_summoning::OnRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

class achievement_assembly_i_choose_you : public AchievementCriteriaScript
{
    public:
        achievement_assembly_i_choose_you() : AchievementCriteriaScript("achievement_assembly_i_choose_you") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(DATA_PHASE_3);
        }
};

void AddSC_boss_assembly_of_iron()
{
    RegisterUlduarCreatureAI(boss_steelbreaker);
    RegisterUlduarCreatureAI(boss_runemaster_molgeim);
    RegisterUlduarCreatureAI(boss_stormcaller_brundir);
    RegisterAuraScript(spell_shield_of_runes);
    RegisterSpellScript(spell_assembly_meltdown);
    RegisterAuraScript(spell_assembly_rune_of_summoning);
    new achievement_assembly_i_choose_you();
}
