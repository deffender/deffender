/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "CellImpl.h"
#include "GridNotifiersImpl.h"
#include "GridNotifiers.h"
#include "InstanceScript.h"
#include "Map.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "SpellScript.h"
#include "ulduar.h"
#include "Vehicle.h"

/* ScriptData
SDName: boss_kologarn
SD%Complete: 90
SDComment: @todo Achievements
SDCategory: Ulduar
EndScriptData */

enum Spells
{
    SPELL_ARM_DEAD_DAMAGE               = 63629,
    SPELL_TWO_ARM_SMASH                 = 63356,
    SPELL_ONE_ARM_SMASH                 = 63573,
    SPELL_ARM_SWEEP                     = 63766,
    SPELL_STONE_SHOUT                   = 63716,
    SPELL_PETRIFY_BREATH                = 62030,
    SPELL_STONE_GRIP                    = 62166,
    SPELL_STONE_GRIP_CANCEL             = 65594,
    SPELL_SUMMON_RUBBLE                 = 63633,
    SPELL_FALLING_RUBBLE                = 63821,
    SPELL_ARM_ENTER_VEHICLE             = 65343,
    SPELL_ARM_ENTER_VISUAL              = 64753,

    SPELL_SUMMON_FOCUSED_EYEBEAM        = 63342,
    SPELL_FOCUSED_EYEBEAM_PERIODIC_LEFT = 63347,
    SPELL_FOCUSED_EYEBEAM_PERIODIC_RIGHT= 63977,
    SPELL_FOCUSED_EYEBEAM_VISUAL_LEFT   = 63676,
    SPELL_FOCUSED_EYEBEAM_VISUAL_RIGHT  = 63702,
    SPELL_EYEBEAM_PERIODIC_DAMAGE_10    = 63346,
    SPELL_EYEBEAM_PERIODIC_DAMAGE_25    = 63976,

    // Passive
    SPELL_KOLOGARN_REDUCE_PARRY         = 64651,
    SPELL_KOLOGARN_PACIFY               = 63726,
    SPELL_KOLOGARN_UNK_0                = 65219, // Not found in DBC

    SPELL_BERSERK                       = 47008  // guess
};

enum NPCs
{
    NPC_RUBBLE_STALKER                  = 33809,
    NPC_ARM_SWEEP_STALKER               = 33661
};

enum Events
{
    EVENT_NONE = 0,
    EVENT_INSTALL_ACCESSORIES,
    EVENT_MELEE_CHECK,
    EVENT_SMASH,
    EVENT_SWEEP,
    EVENT_STONE_SHOUT,
    EVENT_STONE_GRIP,
    EVENT_FOCUSED_EYEBEAM,
    EVENT_RESPAWN_LEFT_ARM,
    EVENT_RESPAWN_RIGHT_ARM,
    EVENT_ENRAGE,
};

enum Data
{
    DATA_IF_LOOKS_COULD_KILL,
    DATA_RUBBLE_AND_ROLL,
    DATA_WITH_OPEN_ARMS,
};

// disarmed, if looks could kill, rubble and roll, with open arms

enum Yells
{
    SAY_AGGRO                               = 0,
    SAY_SLAY                                = 1,
    SAY_LEFT_ARM_GONE                       = 2,
    SAY_RIGHT_ARM_GONE                      = 3,
    SAY_SHOCKWAVE                           = 4,
    SAY_GRAB_PLAYER                         = 5,
    SAY_DEATH                               = 6,
    SAY_BERSERK                             = 7,
    EMOTE_STONE_GRIP                        = 8
};

struct NotMeleePred
{
    NotMeleePred(Unit* owner) : _owner(owner) {}

    bool operator()(Unit* target) const
    {
        return _owner->GetTypeId() == TYPEID_PLAYER && !_owner->IsWithinMeleeRange(target);;
    }

private:
    Unit* _owner;
};

struct boss_kologarn : public BossAI
{
    boss_kologarn(Creature* creature) :
        BossAI(creature, BOSS_KOLOGARN), _left(false), _right(false)
    {
        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
        me->SetControlled(true, UNIT_STATE_ROOT);

        DoCastSelf(SPELL_KOLOGARN_REDUCE_PARRY);
        SetCombatMovement(false);
        Reset();
    }

    void EnterCombat(Unit* /*who*/) override
    {
        Talk(SAY_AGGRO);

        events.ScheduleEvent(EVENT_MELEE_CHECK, 6s);
        events.ScheduleEvent(EVENT_SMASH, 5s);
        events.ScheduleEvent(EVENT_SWEEP, 19s);
        events.ScheduleEvent(EVENT_STONE_GRIP, 25s);
        events.ScheduleEvent(EVENT_FOCUSED_EYEBEAM, 21s);
        events.ScheduleEvent(EVENT_ENRAGE, 10min);

        _EnterCombat();
    }

    void Reset() override
    {
        _Reset();
        _eyebeamHit = false;
        _rubblesCount = 0;
        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
    }

    void JustDied(Unit* /*killer*/) override
    {
        Talk(SAY_DEATH);
        DoCast(SPELL_KOLOGARN_PACIFY);
        me->GetMotionMaster()->MoveTargetedHome();
        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
        me->SetCorpseDelay(WEEK); // Prevent corpse from despawning.
        _JustDied();
    }

    void KilledUnit(Unit* who) override
    {
        if (who->GetTypeId() == TYPEID_PLAYER)
            Talk(SAY_SLAY);
    }

    bool CanAIAttack(Unit const* target) const override
    {
        return !target->HasAura(RAID_MODE(64290, 64292));
    }

    void JustSummoned(Creature* summon) override
    {
        BossAI::JustSummoned(summon);

        if (summon->GetEntry() == NPC_RUBBLE)
            ++_rubblesCount;
        else if (summon->GetEntry() == NPC_LEFT_ARM || summon->GetEntry() == NPC_RIGHT_ARM)
        {
            if (summon->GetEntry() == NPC_LEFT_ARM)
                _left = true;
            else
                _right = true;

            events.CancelEvent(EVENT_STONE_SHOUT);
        }
    }

    void SummonedCreatureDies(Creature* summon, Unit* /*killer*/) override
    {
        if (summon->GetEntry() == NPC_LEFT_ARM)
        {
            _left = false;

            if (instance->GetBossState(BOSS_KOLOGARN) == IN_PROGRESS)
            {
                Talk(SAY_LEFT_ARM_GONE);
                events.ScheduleEvent(EVENT_RESPAWN_LEFT_ARM, 40s);
            }
        }
        else if (summon->GetEntry() == NPC_RIGHT_ARM)
        {
            _right = false;

            if (instance->GetBossState(BOSS_KOLOGARN) == IN_PROGRESS)
            {
                Talk(SAY_RIGHT_ARM_GONE);
                events.ScheduleEvent(EVENT_RESPAWN_RIGHT_ARM, 40s);
            }
        }
        else
            return;

        if (!_left && !_right)
            events.ScheduleEvent(EVENT_STONE_SHOUT, 5s);

        instance->DoStartTimedAchievement(ACHIEVEMENT_TIMED_TYPE_EVENT, CRITERIA_DISARMED);
        _armDestroyed = true;
    }

    uint32 GetData(uint32 id) const override
    {
        switch (id)
        {
            case DATA_IF_LOOKS_COULD_KILL:
                return !_eyebeamHit;
            case DATA_RUBBLE_AND_ROLL:
                return _rubblesCount >= 25;
            case DATA_WITH_OPEN_ARMS:
                return !_armDestroyed;
            default:
                return 0;
        }
    }

    void SetData(uint32 id, uint32 /*value*/) override
    {
        if (id == DATA_IF_LOOKS_COULD_KILL)
            _eyebeamHit = true;
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        events.Update(diff);

        if (me->HasUnitState(UNIT_STATE_CASTING))
            return;

        while (uint32 eventId = events.ExecuteEvent())
        {
            switch (eventId)
            {
                case EVENT_MELEE_CHECK:
                    if (!me->IsWithinMeleeRange(me->GetVictim()))
                        DoCastAOE(SPELL_PETRIFY_BREATH);

                    events.ScheduleEvent(EVENT_MELEE_CHECK, 1s);
                    break;

                case EVENT_SWEEP:
                    if (_left)
                        DoCast(me->FindNearestCreature(NPC_ARM_SWEEP_STALKER, 50.0f, true), SPELL_ARM_SWEEP, true);

                    events.ScheduleEvent(EVENT_SWEEP, 25s);
                    break;

                case EVENT_SMASH:
                    if (_left && _right)
                        DoCastVictim(SPELL_TWO_ARM_SMASH);
                    else if (_left || _right)
                        DoCastVictim(SPELL_ONE_ARM_SMASH);

                    events.ScheduleEvent(EVENT_SMASH, 15s);
                    break;

                case EVENT_STONE_SHOUT:
                    DoCastAOE(SPELL_STONE_SHOUT);
                    events.ScheduleEvent(EVENT_STONE_SHOUT, 2s);
                    break;

                case EVENT_ENRAGE:
                    DoCastSelf(SPELL_BERSERK);
                    Talk(SAY_BERSERK);
                    break;

                case EVENT_RESPAWN_LEFT_ARM:
                case EVENT_RESPAWN_RIGHT_ARM:
                {
                    if (Vehicle* vehicle = me->GetVehicleKit())
                    {
                        int8 seat = eventId == EVENT_RESPAWN_LEFT_ARM ? 0 : 1;
                        uint32 entry = eventId == EVENT_RESPAWN_LEFT_ARM ? NPC_LEFT_ARM : NPC_RIGHT_ARM;
                        vehicle->InstallAccessory(entry, seat, true, TEMPSUMMON_MANUAL_DESPAWN, 0);
                    }
                    break;
                }

                case EVENT_STONE_GRIP:
                {
                    if (_right)
                    {
                        DoCastAOE(SPELL_STONE_GRIP);
                        Talk(SAY_GRAB_PLAYER);
                        Talk(EMOTE_STONE_GRIP);
                    }

                    events.ScheduleEvent(EVENT_STONE_GRIP, 25s);
                    break;
                }

                case EVENT_FOCUSED_EYEBEAM:
                    if (Unit* eyebeamTargetUnit = SelectTarget(SELECT_TARGET_RANDOM, 0, NotMeleePred(me)))
                        DoCast(eyebeamTargetUnit, SPELL_SUMMON_FOCUSED_EYEBEAM);
                    else if (Unit* eyebeamTargetUnit = SelectTarget(SELECT_TARGET_RANDOM, 0))
                        DoCast(eyebeamTargetUnit, SPELL_SUMMON_FOCUSED_EYEBEAM);

                    events.ScheduleEvent(EVENT_FOCUSED_EYEBEAM, urand(15, 35) * IN_MILLISECONDS);
                    break;
            }

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;
        }

        DoMeleeAttackIfReady();
    }

private:
    bool _left;
    bool _right;

    // achievements
    bool _armDestroyed;
    bool _eyebeamHit;
    uint8 _rubblesCount;
};

struct npc_kologarn_arm : public ScriptedAI
{
    // readability
    enum Side : bool
    {
        SIDE_LEFT = true,
        SIDE_RIGHT = false,
    };

    npc_kologarn_arm(Creature* creature) : ScriptedAI(creature) {}

    class RubbleStalkerFinder
    {
        bool _side;

    public:
        RubbleStalkerFinder(bool side) : _side(side) { }

        bool operator()(Creature* u)
        {
            if (u->GetEntry() == NPC_RUBBLE_STALKER)
            {
                if (u->GetPositionY() < -24.0f && _side == SIDE_LEFT)
                    return true;
                else if (u->GetPositionY() > -24.0f && _side == SIDE_RIGHT)
                    return true;
            }

            return false;
        }
    };

    void JustDied(Unit* /*killer*/) override
    {
        DoCastAOE(SPELL_ARM_DEAD_DAMAGE, true);

        Creature* stalker = nullptr;
        RubbleStalkerFinder checker(me->GetEntry() == NPC_LEFT_ARM ? SIDE_LEFT : SIDE_RIGHT);
        Trinity::CreatureSearcher<RubbleStalkerFinder> searcher(me, stalker, checker);
        Cell::VisitAllObjects(me, searcher, 20.0f);

        if (stalker)
        {
            stalker->CastSpell(stalker, SPELL_FALLING_RUBBLE, true);
            stalker->CastSpell(stalker, SPELL_SUMMON_RUBBLE, true);
        }

        me->DespawnOrUnsummon();
    }
};

// needs script for achievement and unsummon on death
struct npc_kologarn_rubble : public ScriptedAI
{
    npc_kologarn_rubble(Creature* creature) : ScriptedAI(creature) {}

    void IsSummonedBy(Unit* /*summoner*/) override
    {
        if (InstanceScript* inst = me->GetInstanceScript())
            if (Creature* kologarn = inst->GetCreature(BOSS_KOLOGARN))
                kologarn->AI()->JustSummoned(me);
    }
};

struct npc_kologarn_eyebeam : public ScriptedAI
{
    npc_kologarn_eyebeam(Creature* creature) : ScriptedAI(creature)
    {
        me->SetReactState(REACT_PASSIVE);
    }

    void IsSummonedBy(Unit* summoner) override
    {
        _summoner = summoner->GetGUID();
        _followTimer = 1000;

        if (InstanceScript* inst = me->GetInstanceScript())
            if (Creature* kologarn = inst->GetCreature(BOSS_KOLOGARN))
                kologarn->AI()->JustSummoned(me);

        if (me->GetEntry() == NPC_FOCUSED_EYEBEAM)
        {
            DoCastSelf(SPELL_FOCUSED_EYEBEAM_PERIODIC_LEFT);
            DoCastSelf(SPELL_FOCUSED_EYEBEAM_VISUAL_LEFT);
        }
        else
        {
            DoCastSelf(SPELL_FOCUSED_EYEBEAM_PERIODIC_RIGHT);
            DoCastSelf(SPELL_FOCUSED_EYEBEAM_VISUAL_RIGHT);
        }
    }

    void SpellHitTarget(Unit* /*target*/, SpellInfo const* spell) override
    {
        if (spell->Id == SPELL_EYEBEAM_PERIODIC_DAMAGE_10 || spell->Id == SPELL_EYEBEAM_PERIODIC_DAMAGE_25)
            if (InstanceScript* inst = me->GetInstanceScript())
                if (Creature* kologarn = inst->GetCreature(BOSS_KOLOGARN))
                    kologarn->AI()->SetData(DATA_IF_LOOKS_COULD_KILL, 1);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!_summoner)
            return;

        if (_followTimer <= diff)
        {
            if (Unit* target = ObjectAccessor::GetUnit(*me, _summoner))
                me->GetMotionMaster()->MoveFollow(target, 0.0f, 0.0f, true);

            _summoner = ObjectGuid::Empty;
        }
        else
            _followTimer -= diff;
    }

private:
    ObjectGuid _summoner;
    uint32 _followTimer;
};

class spell_ulduar_rubble_summon : public SpellScript
{
    PrepareSpellScript(spell_ulduar_rubble_summon);

    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        Unit* caster = GetCaster();
        if (!caster)
            return;

        ObjectGuid originalCaster = caster->GetInstanceScript() ? caster->GetInstanceScript()->GetGuidData(BOSS_KOLOGARN) : ObjectGuid::Empty;
        uint32 spellId = GetEffectValue();
        for (uint8 i = 0; i < 5; ++i)
            caster->CastSpell(caster, spellId, true, nullptr, nullptr, originalCaster);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_ulduar_rubble_summon::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

class spell_ulduar_stone_grip_cast_target : public SpellScript
{
    PrepareSpellScript(spell_ulduar_stone_grip_cast_target);

    bool Load() override
    {
        return GetCaster()->GetTypeId() == TYPEID_UNIT;
    }

    void FilterTargetsInitial(std::list<WorldObject*>& unitList)
    {
        Trinity::Containers::RandomResize(unitList, [this](WorldObject* target)
        {
            if (GetCaster()->ToCreature()->GetThreatManager().GetThreatListSize() > 1 && target == GetCaster()->GetVictim())
                return false;

            return true;
        }, GetCaster()->GetMap()->Is25ManRaid() ? 3 : 1);

        // For subsequent effects
        _unitList = unitList;
    }

    void FillTargetsSubsequential(std::list<WorldObject*>& unitList)
    {
        unitList = _unitList;
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_ulduar_stone_grip_cast_target::FilterTargetsInitial, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_ulduar_stone_grip_cast_target::FillTargetsSubsequential, EFFECT_1, TARGET_UNIT_SRC_AREA_ENEMY);
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_ulduar_stone_grip_cast_target::FillTargetsSubsequential, EFFECT_2, TARGET_UNIT_SRC_AREA_ENEMY);
    }

private:
    // Shared between effects
    std::list<WorldObject*> _unitList;
};

class spell_ulduar_stone_grip : public AuraScript
{
    PrepareAuraScript(spell_ulduar_stone_grip);

    void OnRemoveStun(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        if (Player* owner = GetOwner()->ToPlayer())
            owner->RemoveAurasDueToSpell(aurEff->GetAmount());
    }

    void OnRemoveVehicle(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        PreventDefaultAction();
        Unit* unit = GetCaster();
        if (!unit)
            return;

        Position exitPosition;
        exitPosition.m_positionX = 1778.0f + frand(-5.0f, 5.0f);
        exitPosition.m_positionY = -7.5f + frand(-5.0f, 5.0f);
        exitPosition.m_positionZ = 457.9322f;

        // Remove pending passengers before exiting vehicle - might cause an Uninstall
        GetTarget()->GetVehicleKit()->RemovePendingEventsForPassenger(unit);
        unit->_ExitVehicle(&exitPosition);
        unit->RemoveAurasDueToSpell(GetId());

        // Temporarily relocate player to vehicle exit dest serverside to send proper fall movement
        // beats me why blizzard sends these 2 spline packets one after another instantly
        Position oldPos = unit->GetPosition();
        unit->Relocate(exitPosition);
        unit->GetMotionMaster()->MoveFall();
        unit->Relocate(oldPos);
    }

    void Register() override
    {
        OnEffectRemove += AuraEffectRemoveFn(spell_ulduar_stone_grip::OnRemoveVehicle, EFFECT_0, SPELL_AURA_CONTROL_VEHICLE, AURA_EFFECT_HANDLE_REAL);
        AfterEffectRemove += AuraEffectRemoveFn(spell_ulduar_stone_grip::OnRemoveStun, EFFECT_2, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL);
    }
};

class spell_ulduar_stone_grip_absorb : public AuraScript
{
    PrepareAuraScript(spell_ulduar_stone_grip_absorb);

    //! This will be called when Right Arm (vehicle) has sustained a specific amount of damage depending on instance mode
    //! What we do here is remove all harmful aura's related and teleport to safe spot.
    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_ENEMY_SPELL)
            return;

        GetUnitOwner()->CastSpell(GetUnitOwner(), SPELL_STONE_GRIP_CANCEL, true);
    }

    void Register() override
    {
        AfterEffectRemove += AuraEffectRemoveFn(spell_ulduar_stone_grip_absorb::OnRemove, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
    }
};

class spell_ulduar_squeezed_lifeless : public SpellScript
{
    PrepareSpellScript(spell_ulduar_squeezed_lifeless);

    void HandleInstaKill(SpellEffIndex /*effIndex*/)
    {
        if (!GetHitPlayer() || !GetHitPlayer()->GetVehicle())
            return;

        //! Proper exit position does not work currently,
        //! See documentation in void Unit::ExitVehicle(Position const* exitPosition)
        Position pos;
        pos.m_positionX = 1756.25f + irand(-3, 3);
        pos.m_positionY = -8.3f + irand(-3, 3);
        pos.m_positionZ = 448.8f;
        pos.SetOrientation(float(M_PI));
        GetHitPlayer()->DestroyForNearbyPlayers();
        GetHitPlayer()->ExitVehicle(&pos);
        GetHitPlayer()->UpdateObjectVisibility(false);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_ulduar_squeezed_lifeless::HandleInstaKill, EFFECT_1, SPELL_EFFECT_INSTAKILL);
    }
};

class spell_ulduar_cancel_stone_grip : public SpellScript
{
    PrepareSpellScript(spell_ulduar_cancel_stone_grip);

    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        Unit* target = GetHitUnit();
        if (!target || !target->GetVehicleBase())
            return;

        switch (target->GetMap()->GetDifficulty())
        {
            case RAID_DIFFICULTY_10MAN_NORMAL:
                target->GetVehicleBase()->RemoveAura(GetSpellInfo()->Effects[EFFECT_0].CalcValue(), target->GetGUID());
                break;
            case RAID_DIFFICULTY_25MAN_NORMAL:
                // wrong DBC data, no effect 1 :(
                target->GetVehicleBase()->RemoveAura(/*GetSpellInfo()->Effects[EFFECT_1].CalcValue()*/63985, target->GetGUID());
                break;
            default:
                break;
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_ulduar_cancel_stone_grip::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

class spell_kologarn_stone_shout : public SpellScript
{
    PrepareSpellScript(spell_kologarn_stone_shout);

    void FilterTargets(std::list<WorldObject*>& unitList)
    {
        unitList.remove_if(PlayerOrPetCheck());
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_kologarn_stone_shout::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
    }
};

class achievement_with_open_arms : public AchievementCriteriaScript
{
    public:
        achievement_with_open_arms() : AchievementCriteriaScript("achievement_with_open_arms") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(DATA_WITH_OPEN_ARMS);
        }
};

class achievement_if_looks_could_kill : public AchievementCriteriaScript
{
    public:
        achievement_if_looks_could_kill() : AchievementCriteriaScript("achievement_if_looks_could_kill") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(DATA_IF_LOOKS_COULD_KILL);
        }
};

class achievement_rubble_and_roll : public AchievementCriteriaScript
{
    public:
        achievement_rubble_and_roll() : AchievementCriteriaScript("achievement_rubble_and_roll") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(DATA_RUBBLE_AND_ROLL);
        }
};

void AddSC_boss_kologarn()
{
    RegisterUlduarCreatureAI(boss_kologarn);
    RegisterUlduarCreatureAI(npc_kologarn_arm);
    RegisterUlduarCreatureAI(npc_kologarn_rubble);
    RegisterUlduarCreatureAI(npc_kologarn_eyebeam);
    RegisterSpellScript(spell_ulduar_rubble_summon);
    RegisterSpellScript(spell_ulduar_stone_grip_cast_target);
    RegisterAuraScript(spell_ulduar_stone_grip);
    RegisterAuraScript(spell_ulduar_stone_grip_absorb);
    RegisterSpellScript(spell_ulduar_squeezed_lifeless);
    RegisterSpellScript(spell_ulduar_cancel_stone_grip);
    RegisterSpellScript(spell_kologarn_stone_shout);
    new achievement_with_open_arms();
    new achievement_if_looks_could_kill();
    new achievement_rubble_and_roll();
}
